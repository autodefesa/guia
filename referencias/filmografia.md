# Filmografia

Filmografia sobre segurança, privacidade, vigilância e espionagem.

## Não-ficção

* [Citizenfour (2014)](https://www.imdb.com/title/tt4044364/).
* [The Gatekeepers](http://www.cartacapital.com.br/internacional/em-documentario-ex-espioes-israelenses-criticam-a-politica-de-seguranca-do-pais/).
* [The Most Dangerous Man in America: Daniel Ellsberg and the Pentagon Papers](http://www.imdb.com/title/tt1319726/).
* [The Vula Connection](https://www.youtube.com/watch?v=29vrvKsKXPI).
* [The Internet's Own Boy: The Story of Aaron Swartz](https://www.imdb.com/title/tt3268458/).
* [VIPs: Histórias Reais de um Mentiroso (2010)](https://www.imdb.com/title/tt1997594/).
* [Collateral Murder (2010)](https://www.imdb.com/title/tt1820416/).
* [Shoshana Zuboff on surveillance capitalism | VPRO Documentary](https://www.youtube.com/watch?v=hIXhnWUmMvw).
* [XPloit: Internet sob ataque (websérie 1-6)](https://www.youtube.com/watch?v=18ao1F7lz8A).
* [Don't Fuck with Cats (2019)](https://www.imdb.com/title/tt11318602).
* [Caso Pegasus](https://pt.wikipedia.org/wiki/Pegasus_(spyware)):
    * [Global Spyware Scandal: Exposing Pegasus (2023)](https://www.pbs.org/wgbh/frontline/documentary/global-spyware-scandal-exposing-pegasus/).
    * [Surveilled (2024)](https://www.themoviedb.org/movie/1369240-surveilled).

## Ficcionalizados

* [A Batalha de Argel (1966)](https://www.imdb.com/title/tt0058946/).
* [Snowden](http://www.imdb.com/title/tt3774114).
* [Takedown (2000)](https://www.imdb.com/title/tt0159784/).
* [The Imitation Game (2014)](https://www.imdb.com/title/tt2084970/).
* [Argo (2012)](https://www.imdb.com/title/tt1024648/).
* [The Report (2019)](https://www.imdb.com/title/tt8236336/).
* [Official Secrets (2019)](https://www.imdb.com/title/tt5431890/).
* [Wasp Network (2019)](https://www.imdb.com/title/tt6760876/).
* [The Post (2017)](https://www.imdb.com/title/tt6294822/).
* [Cambridge Spies (2003)](https://www.imdb.com/title/tt0346223/).

## Ficção

* [A Most Wanted Man - Wikipedia](https://en.wikipedia.org/wiki/A_Most_Wanted_Man).
* [Sneakers](http://www.imdb.com/title/tt0105435/).
* [The Conversation](https://en.wikipedia.org/wiki/The_Conversation).
* [Catch Me If You Can](https://www.imdb.com/title/tt0264464/).
* [The Lives of Others](https://www.imdb.com/title/tt0405094/).
* [Brazil](https://www.imdb.com/title/tt0088846/).
* [1984](https://www.imdb.com/title/tt0087803).
* [The Girl with the Dragon Tattoo (2009)](https://www.imdb.com/title/tt1132620/).
* [Wargames (1984)](https://www.imdb.com/title/tt0086567/).
* [Hackers (1995)](https://www.imdb.com/title/tt0113243/).
* [Tinker Tailor Soldier Spy (2011)](https://www.imdb.com/title/tt1340800/).
* [The Little Drummer Girl (2018)](https://www.imdb.com/title/tt7598448/).
* [Our Kind of Traitor (2016)](https://www.imdb.com/title/tt1995390/).
* [Gattaca](https://www.imdb.com/title/tt0119177/).
* [J. Edgar (2011)](https://www.imdb.com/title/tt1616195/).
* [The Departed (2006)](https://www.imdb.com/title/tt0407887/).
* [The Night Manager (2016)](https://www.imdb.com/title/tt1399664/).
* [Red Sparrow (2018)](https://www.imdb.com/title/tt2873282/).
* [Atomic Blonde (2017)](https://www.imdb.com/title/tt2406566/).
* [The Constant Gardener (2005)](https://www.imdb.com/title/tt0387131/).
* [Ghost in the shell (2017)](https://www.imdb.com/title/tt1219827/).

## Referências

* [Movies for Hackers](https://github.com/k4m4/movies-for-hackers).
