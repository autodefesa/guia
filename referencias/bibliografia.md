# Bibliografia

## Em português

Criptografia:

* [O Livro dos Códigos, de Simon Singh](http://www.record.com.br/livro_sinopse.asp?id_livro=16390).

Hacking:

* A Arte de Enganar, de Kevin Mitnick.

História e sociedade:

* Vigilância Líquida, de Zygmunt Bauman.
* Ministério do Silêncio.
* [Quem pagou a conta? A CIA na guerra fria da cultura](http://editorarecord.com.br/livro_sinopse.asp?id_livro=276).
* [Atividade de Inteligência e Legislação Correlata](http://www.jb.com.br/cultura/noticias/2009/07/16/livro-de-ex-oficial-da-abin-revela-bastidores-da-inteligencia/).
* Livros sobre o Snowden.
* Wikileaks:
    * Quando o Google encontra o Wikileaks.

Literatura:

* Livros do John le Carré.
* O Candidato da Manchúria.

## Em espanhol

* [Diagnósticos en seguridad digital para organizaciones defensoras de derechos humanos y del territorio: un manual para facilitadores](https://gendersec.tacticaltech.org/wiki/index.php/Diagn%C3%B3sticos_en_seguridad_digital_para_organizaciones_defensoras_de_derechos_humanos_y_del_territorio:_un_manual_para_facilitadores).
* [Zen y el arte de que la tecnología trabaje para ti](https://gendersec.tacticaltech.org/wiki/index.php/Complete_manual/es).

## Em inglês

* [Livros do Bruce Schneier](https://www.schneier.com/books/):
    * [Data and Goliath](http://boingboing.net/2015/03/02/bruce-schneiers-data-and-gol.html).
* [The American Black Chamber](https://en.wikipedia.org/wiki/The_American_Black_Chamber).
* [The Codebreakers](https://en.wikipedia.org/wiki/The_Codebreakers). ([2](http://openlibrary.org/works/OL2543107W/The_codebreakers)).
* Livros do [James Bamford](https://en.wikipedia.org/wiki/James_Bamford) sobre a NSA.
    * Puzzle Palace.
    * [The Shadow Factory](https://en.wikipedia.org/wiki/The_Shadow_Factory).
* Livros do [James Risen](https://en.wikipedia.org/wiki/James_Risen).
* [This Machine Kills Secrets](http://www.amazon.com/Machine-Kills-Secrets-EmpowerWhistleblowers-ebook/dp/B007HUD7LU/).
* [No Place to Hide](https://www.schneier.com/blog/archives/2014/05/new_nsa_snowden.html).
* [Underground](http://www.underground-book.net).  -  [Hacker Crackdown](http://www.gutenberg.org/ebooks/101).
* [Cypherpunks](https://en.wikipedia.org/wiki/Cypherpunks_(book)).
* The Like Switch: [A Former FBI Agent Explains How to Tell When Someone Is Lying to You \| Inc.com](https://www.inc.com/jack-schafer/an-fbi-agent-on-how-to-detect-deception.html).
* NSA's Transformation: An Executive Branch Black Eye by Edward Loomis.
* [Vigiância Líquida](http://www.surveillance-studies.net/?p=702).
* [Spies for Hire: The Secret World of Intelligence Outsourcing](http://www.cartamaior.com.br/templates/materiaMostrar.cfm?materia_id=22351).
* [Exploding the Phone](http://explodingthephone.com/).
* Livros do [Jacques Henno](http://www.cartamaior.com.br/templates/materiaMostrar.cfm?materia_id=22455).
* [The Rise of the Computer State](http://www.nytimes.com/1983/03/27/magazine/the-silent-power-of-the-nsa.html).
* Livros do [James Bamford](https://en.wikipedia.org/wiki/James_Bamford).
* [Secret Manoeuvres in the Dark: Corporate Spying on Activists](http://www.amazon.com/Secret-Manoeuvres-Dark-Corporate-Activists/dp/0745331866).
* Livros do [James Risen](https://en.wikipedia.org/wiki/James_Risen).
* Obra de [Somerset Maugham](https://en.wikipedia.org/wiki/Somerset_Maugham).
* Obra de [Compton Mackenzie](https://en.wikipedia.org/wiki/Compton_Mackenzie).
* [The American Black Chamber](https://en.wikipedia.org/wiki/The_American_Black_Chamber).
* [The Soul of a New Machine](https://en.m.wikipedia.org/wiki/The_Soul_of_a_New_Machine).
* [The Codebreakers: The Story of Secret Writing (by David Kahn)](http://www.david-kahn.com/book-david-kahn-code-codebreakers-cryptography.htm).
* [No Place to Hide](https://www.schneier.com/blog/archives/2014/05/new_nsa_snowden.html).
* [This Machine Kills Secrets](http://www.amazon.com/Machine-Kills-Secrets-EmpowerWhistleblowers-ebook/dp/B007HUD7LU/).
* [Takedown](http://www.takedown.com) ([resenha](https://lispmachine.wordpress.com/2008/06/16/takedown-by-tsutomu-shimomura-john-markoff)).
