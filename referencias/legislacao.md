# Legislação

* Esta não é uma lista completa.

## Legislação Brasileira

### Âmbito Federal

* [Guia de Direitos](http://www.guiadedireitos.org).

* [Manual de sobrevivência na selva de bits: evitando as ações judiciais contra publicações na Internet](https://tuliovianna.org/2010/08/23/manual-de-sobrevivencia-na-selva-de-bits-evitando-as-acoes-judiciais-contra-publicacoes-na-internet/).

* [Lei 9453/97](http://www2.camara.gov.br/legislacao/legin.html/textos/visualizarTexto.html?ideNorma=349410&seqTexto=1&PalavrasDestaque=):
  * Retenção de documentos:

    > "§ 2° Quando o documento de identidade for indispensável para a
    > entrada de pessoa em órgãos públicos ou particulares, serão seus
    > dados anotados no ato e devolvido o documento imediatamente ao
    > interessado."

* [Lei 9.296/96: Interceptação Telefônica](http://www.planalto.gov.br/ccivil/LEIS/L9296.htm) (Lei do Grampo).

* [Lei 9883/99: Criação da ABIN e do SBI](http://www.planalto.gov.br/CCIVIL/LEIS/L9883.htm)

* [Decreto 4376/2002: Funcionamento e organização do SBI](http://www.planalto.gov.br/ccivil/decreto/2002/D4376.htm)

* [CF/88 - XII do artigo 5 - Sigilo da correspondência](http://www.planalto.gov.br/ccivil_03/Constituicao/Constituicao.htm#art5xii):

  > "XII - é inviolável o sigilo da correspondência e das comunicações
  >        telegráficas, de dados e das comunicações telefônicas, salvo, no último caso,
  >        por ordem judicial, nas hipóteses e na forma que a lei estabelecer para fins de
  >        investigação criminal ou instrução processual penal;"

* [CF/88 - XXXIII do artigo 5 - Habeas Data](http://www.planalto.gov.br/CCIVIL/Constituicao/Constitui%E7ao.htm#art5xxxiii)

   > "XXXIII - todos têm direito a receber dos órgãos públicos informações
   > de seu interesse particular, ou de interesse coletivo ou geral, que
   > serão prestadas no prazo da lei, sob pena de responsabilidade,
   > ressalvadas aquelas cujo sigilo seja imprescindível à segurança da
   > sociedade e do Estado"

* [Lei 9507/97: Alteração do Habeas Data](http://www.planalto.gov.br/ccivil/Leis/L9507.htm)

* [Lei Carolina Dieckmann - Cibercrimes](http://www.planalto.gov.br/ccivil_03/_ato2011-2014/2012/lei/l12737.htm).

* [Lei Azeredo - Cibercrimes](http://www.planalto.gov.br/ccivil_03/_Ato2011-2014/2012/Lei/L12735.htm).

* [Marco Civil da Internet](http://www.planalto.gov.br/ccivil_03/_ato2011-2014/2014/lei/l12965.htm).

* [Lei de Segurança Nacional - LEI Nº 7.170, DE 14 DE  DEZEMBRO DE 1983](http://www.planalto.gov.br/ccivil_03/LEIS/L7170.htm).

* [Lei das Organizações Criminosas - LEI Nº 12.850, DE 2 DE AGOSTO DE 2013](http://www.planalto.gov.br/ccivil_03/_Ato2011-2014/2013/Lei/L12850.htm).

### São Paulo

*  [Lei Municipal (São Paulo) Nº 13.541/2003](http://www3.prefeitura.sp.gov.br/cadlem/secretarias/negocios_juridicos/cadlem/integra.asp?alt=25032003L%20135410000) ([ver alterações](http://www3.prefeitura.sp.gov.br/cadlem/secretarias/negocios_juridicos/cadlem/alteracoes.asp?c=L+135410000)):

   > Dispõe sobre a colocação de placa informativa sobre filmagem de
   > ambientes, e dá outras providências

## Legislação Internacional

### Geral

* [Mutual legal assistance treaty (MLAT)](https://secure.wikimedia.org/wikipedia/en/wiki/Mutual_Legal_Assistance_Treaty).

### EUA

* [Privacy Act of 1974](https://secure.wikimedia.org/wikipedia/en/wiki/Privacy_Act_of_1974).
* [Foreign Intelligence Surveillance Act (FISA)](https://secure.wikimedia.org/wikipedia/en/wiki/Foreign_Intelligence_Surveillance_Act).
* [Executive Order 12333](https://en.wikipedia.org/wiki/Executive_Order_12333) (substituída pela [USSID SP0018](https://www.eff.org/files/2013/11/21/20131119-odni-united_states_signals_intelligence_directive_18_jan_25_2011.pdf)).
* [CALEA](https://secure.wikimedia.org/wikipedia/en/wiki/Communications_Assistance_for_Law_Enforcement_Act).
* [Electronic Communications Privacy Act](https://secure.wikimedia.org/wikipedia/en/wiki/Electronic_Communications_Privacy_Act).
* [USA PATRIOT Act](https://secure.wikimedia.org/wikipedia/en/wiki/Patriot_act) (em especial a seção 215).
* [National security letter](https://en.wikipedia.org/wiki/National_Security_Letter) (em especial a seção 702).

### Reino Unido

* [Regulation of Investigatory Powers Act 2000 (RIPA)](https://secure.wikimedia.org/wikipedia/en/wiki/RIPA).

### França

* [Justiça dos nomes de domínios in Francia](http://www.midiaindependente.org/pt/blue/2007/10/398649.shtml).
