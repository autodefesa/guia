# Comunicação Digital

## Funcionamento

Trataremos agora a respeito de segurança em sistemas digitais. Para isso, usaremos
uma versão modificada da teoria clássica da comunicação.


                         grampo ativo ou passivo
                                   |
                                   |
      Emissor/a ---meio----------meio-------------meio------ Emissor/a
      Receptor/a         -----mensagem-1------>              Receptor/a
          1             ](----mensagem-2-------                 2

Classicamente, diremos que toda a comunicação é baseada pelo menos por um
**emissor** que envia uma **mensagem** para um **receptor** através de
um **meio**. A comunicação pode ser bidirecional, indicando que uma das
pontas da comunicação pode ser emissora e receptora de mensagens ao mesmo
tempo.

Esse modelo está incompleto no que diz respeito à segurança e à privacidade:
ele não contém um elemento que está cada dia mais presente nas comunicações,
que é o **grampo** ou **escuta**, isto é, a interceptação da comunicação.

É fundamental considerarmos a interceptação ou grampo como entidade sempre
presente na comunicação.

Primeiro, porque queremos seguir uma regra geral de simplicidade: se uma
comunicação **pode** ser grampeada, assuma que ela está sendo grampeada.
Independentemente do grampo estar sendo realizado ou não, ao assumí-lo, você
já se protege de antemão.

Em segundo lugar, porque a forma como a comunicação flui no nosso mundo prático
se dá através da **cópia** da informação por onde ela passa até chegar ao seu
destino. O próprio movimento de uma comunicação, por exemplo de uma onda
sonora, pode ser explicado simplificadamente em termos da **cópia** da
informação para a posição subsequente seguida pelo apagamento dessa informação.

Isso simplesmente decorre do fato de que uma mensagem se movendo em um meio
percorre uma porção do espaço até chegar ao seu destino. Nesse caminho, a mensagem
pode ser interceptada sem que nem o emissor ou o receptor consigam perceber. Pode
até ser que eles consigam, mas isso não é garantido.

O próprio fato da comunicação passar de um ponto a outro do espaço pode ser
entendida, metaforicamente, como a interceptação da comunicação por aquele
ponto do espaço. Assim, diremos que a propriedade de todo meio de comunicação
é a sua **grampeabilidade**.

Note que os fenômenos envolvidos na transmissão física de informações são mais
complexos, mas esta nossa noção intuitiva é suficiente para entendermos porque
o grampo deve ser assumido como parte integral da comunicação, mesmo que ele
não esteja ocorrendo.

No caso da comunicação por meios digitais, essa noção intuitiva acaba
necessariamente sendo um **fato**! Um meio de comunicação digital pode ser
entendido, de maneira bem simplificada, como se fosse uma brincadeira de telefone
sem fio.

Segundo a Wikipedia,

> Telefone sem fio é uma tradicional brincadeira popular, na qual uma pessoa
> fala uma palavra ou frase (o "segredo") ao ouvido de outra pessoa ao seu
> lado, de modo que os demais participantes não escutem ou descubram
> imediatamente qual é o "segredo".
>
> A que ouviu o segredo tenta então repeti-lo para o próximo participante, e
> assim por diante até chegar ao último participante, que deve contar o
> segredo em voz alta. Uma das regras do jogo é que o segredo não pode ser
> repetida ao ouvinte da vez.
>
> Por esse motivo é comum o segredo ser mal entendido e por isso passado ao
> demais ouvintes de forma cada vez mais deturpada, chegando totalmente
> diferente ao ouvinte final, e isso é o que torna a brincadeira divertida. É
> possível competir dois grupos para ver qual grupo chega com a palavra mais
> fielmente ao destino.
>
> -- https://pt.wikipedia.org/wiki/Telefone_sem_fio_(brincadeira)

No caso, a grande diferença entre um meio de comunicação digital é que seu
objetivo **intencional** é que a mensagem (ou o **segredo**) não seja deturpado
ao chegar ao seu destino, mas que chegue de modo inalterado. Aliás, os sistemas
de comunicação foram passando do analógico para o digital justamente porque no
digital é mais simples garantir a integridade da informação. Mas essa é outra
história...

A brincadeira do telefone sem fio tem ainda outra coisa muito importante a nos
ensinar sobre os meios de comunicação digitais: em ambos, a **mensagem** só
é um segredo para quem está de fora do jogo: tanto as pessoas os ou os elementos
de comunicação envolvidos na brincadeira precisam receber a informação para que
possam passá-la para frente.

Ou seja, o **segredo** não está sendo muito bem protegido, né? Se for uma
informação sigilosa que só a primeira e a última pessoa do telefone sem fio
deveriam conhecer, então o segredo foi para o brejo logo no início da
brincadeira.

Chamaremos esta nossa concepção de comunicação com emissor, receptor, mensagem,
meio e grampo de **Teoria da Comunicação Hacker**. E chamaremos de **grampeabilidade**
esta estranha propriedade dos meios de comunicação -- especialmente digitais -- de
poderem ser grampeados.

É importante entender que num determinado meio de comunicação podem haver vários grampos
instalados, pois em cada ponto por onde trafega a informação, pode ser instalado um
interceptador que desvie ou copie a mensagem. Assim, toda a vez que pensarmos
num meio de comunicação digital, tentaremos esboçar um diagrama básico de funcionamento
para, em seguida, mostrar onde podem ser instalado o aparato de vigilância e como se
defender dele.

       ponta                       meio                          ponta
    |---------|-----------------------------------------------|---------|
                  grampo 1       grampo 2       grampo n
                     |              |               |
     emissor ](-> aparelho 1](-> aparelho 2](-> aparelho n](-> receptor
     receptor                                                  emissor

Daqui para a frente, **assumiremos que todos os meios de comunicação estão grampeados**!

AAAAH!!!! Isto é o fim do mundo????

Não.

Veremos que é possível ter um nível razoável de segurança mesmo se houver um
grampo instalado nos meios de comunicação. Porém, se as pontas da comunicação
estiverem grampeadas, estão o jogo está perdido.

As pontas da comunicação são as portas de entrada e saída entre os meios de
comunicação e seus emissores(as) e receptores. Se elas estiverem comprometidas,
não há muito o que fazer.

Ao longo do guia, trataremos basicamente de formas de proteger as pontas da
comunicação e a mensagem contra a interceptação e adulteração da nossa comunicação.

## Ataques

Já entedemos o funcionamento bem básico e esquemático dos meios de comunicação digital
e vimos que eles são intrinsecamente vulneráveis à interceptação de mensagens.

Mas quais são os ataques específicos que podem acontecer? Neste capítulo trataremos dos
principais.

### Interceptação de dados

O ataque mais básico, e que já citamos de algum modo, é a **interceptação de dados**.

Existem dois tipos: os passivos, que simplesmente copiam silenciosamente a comunicação,
e os ativos, que interceptam e modificam a comunicação.

Os grampos ativos são os mais perigosos, pois podem modificar o conteúdo da
mensagem ou mesmo inviabilizar as medidas de segurança adotadas para a proteção
da mensagem em meios hostis.

Por outro lado, os grampos ativos são mais fáceis de serem detectados caso a
comunicação esteja protegida com **criptografia forte e bem implementada**,
como veremos adiante no curso.

### Interceptação de metadados

Outro ataque básico é a **interceptação de metadados** que, em algumas circunstâncias,
é conhecida como **retenção de dados**.

Dizemos que dados são o conteúdo da mensagem, enquanto que os metadados são os **dados
de endereçamento** da mensagem, isto é, as metainformações necessárias para que a mensagem
consiga chegar até o seu destino.

Os sistemas modernos de comunicação são baseados em **protocolos**, que são basicamente
convenções bem definidas de como máquinas podem trocar informações entre si, evitando que a
comunicação seja interrompida ou que haja falhas nas transmissões.

Em tais protocolos, é a própria mensagem que possui
as informações básicas sobre o seu destino e muitas vezes por onde passou. Isso
é muito semelhante ao sistema postal, no qual as cartas possuem endereços
escritos no envelope e vão recebendo carimbos conforme passam de um lugar para
o outro.

São essas informações, chamadas de metadados que dizem aos sistemas para onde a
mensagem deve ser enviada, quem a enviou, em que momento, etc.

Metadados tipicamente contém remetente, destinatário, data de envio e podem
incluir assunto, sistemas por onde passou, localização das partes envolvidas na
comunicação, etc.

Também podem receber informações adicionais, como duração completa da comunicação,
horário de encerramento, quantidade total de informação trocada, etc.

Você pode pensar em metadados como podendo ser compostos por quaisquer informações
sobre a mensagem, exceto o próprio conteúdo da mensagem.

      ___________
     |           |    quem fala com quem (emissor, receptor),
     | metadados | -> etc: diz ao meio como enviar a mensagem,
     |           |    ou seja, trata do endereçamento, arquivamento,
     |-----------|    isto é, do ciclo de vida da comunicação.
     |           |
     |  dados    | -> o que é dito, isto é, a mensagem.
     |___________|

Na realidade, dados e metadados formam uma matrioska com muitas camadas de
significado ou discurso: o que é dado para uma dada interpretação pode ser
metadado para outra. Ou seja, se uma informação é dado ou metadado vai depender
de como um determinado sistema ou ator a interpreta. Em outras palavras, dados
e metadados são noções bem relativas a respeito da informação.

Mas por que essa diferenciação é importante? Além disso, metadados não são
informações inofensivas e cuja captura é irrelevante para a segurança?

É importante diferenciar dados de metadados porque hoje em dia o processamento
de metadados representa a maior parte da vigilância automática: enquanto que
uma boa interpretação do conteúdo da comunicação atualmente necessita da ação
humana, os metadados podem ser interpretados e processados por computadores sem
intervenção humana.

Essa diferença decorre do fato de que os metadados estão codificados de acordo
com os protocolos de comunicação digital. Assim como os metadados são processados
automaticamente para que uma mensagem chegue ao seu destino, eles também podem
ser processados para fins de vigilância.

Ou seja, quando mensagens são interpretadas automaticamente, seus metadados
também são processados automaticamente, enquanto que os dados são, em geral,
processados apenas parcialmente, necessitando de alguma pessoa para fazer a
interpretação completa.

Mesmo nos casos em que a interpretação parcial dos dados é realizada, o seu custo
computacional ainda é alto. É muito barato guardar dados e metadados mas
processar automaticamente apenas os metadados. E é mais barato ainda apenas gravar
e processar automaticamente os metadados.

Os metadados não são irrelevantes! Muito pelo contrário!

Pela facilidade de coleta, processamento e interpretação, os metadados são informações
muito sensíveis. Com eles, é possível reconstruir toda a rede social de comunicação
entre um conjunto de usuários(as), descobrindo quem fala com quem, quais são as pessoas
mais comunicativas, quem se relaciona mais com quem e assim por diante. Tudo isso sem
precisar prestar atenção no conteúdo da comunicação!

Noutras palavras, a coleta dos metadados permite a criação do "grafo" social,
que é o desenho da rede de relacionamento entre as partes envolvidas na
comunicação e é uma informação tão valiosa quanto a coleta dos dados.

Podemos notar o quanto captura apenas dos metadados já é suficiente para obter muitas
informações sobre a comunicação: detalhes de quem está comunicando com quem, assunto,
duração das mensagens, etc.

No entanto, a interceptação de metadados acabou por não ser considerada como
grande violação da privacidade, o que é um grande perigo.

Existem inclusive leis em todo o mundo que obrigam empresas que dão acesso à
internet ou que distribuem conteúdo de gravarem automaticamente metadados de
acesso dos usuários dos seus sistemas. Os metadados são mantidos por períodos
de meses ou até anos.

Essa prática é conhecida como **retenção de dados** e é justamente realizada
para facilitar a identificação de usuários.

Em algumas situações a retenção de dados pode ser proibida, mas mesmo assim ela
ocorre. Agências governamentais e empresas podem estar coletando informações
de usuário com ou sem consentimento.

Uma interceptação de comunicação pode, ao mesmo tempo, grampear toda a mensagem,
isto é, tanto os seus dados quanto os seus metadados. Assim, é importante
pensar em medidas de proteção tanto dos dados quanto dos metadados da
mensagem. Diferentes tecnologias protegem dados, metadados ou ambos.

## Defesas

Quais as defesas contra a interceptação de mensagens, seja de dados, metadados
ou ambos?

Neste capítulo trataremos dos tipos genéricos de defesa, enquanto que nos
capítulos posteriores trataremos efetivamente das ferramentas práticas.

É bom notar que a segurança da informação é um campo em mudança constante.
Muitas vezes uma solução tem um "prazo de validade" que depende de alguma
nova descoberta de falha ou mudanças na própria arquitetura dos sistemas.

Por exemplo, uma solução que funciona hoje pode ser insuficiente no futuro.

### Defesas físicas

Podemos pensar em defesas físicas como o controle efetivo dos dispositivos,
equipamentos e linhas de transmissão de mensagens.

Isso requer um custo muito alto e depende de muitas pessoas e equipamento para
que seja efetivo. E é um problema de pouca solução: se mais pessoas e
equipamentos são necessários para fiscalizar e proteger as pessoas e o
equipamento existente, quem vai proteger essas novas pessoas e equipamentos?

Ou seja, a proteção física de toda uma infraestrutura nem é prática e nem é
totalmente efetiva.

Podemos, no entanto, escolher proteger as partes da comunicação mais sensíveis
a ataques.

Se tivermos de dar prioridade a algumas partes, diria que precisamos proteger
as pontas da comunicação e utilizar criptografia de ponta a ponta para que
a mensagem criptografada possa trafegar num meio de comunicação desprotegido.

A mensagem pode estar criptografada durante o trânsito e tornar inefetiva
a interceptação no meio de comunicação, mas é nas pontas da comunicação que
ela precisa ser descriptografada. Se as pontas estiverem desprotegidas ou
em poder do atacante, o risco de captura da mensagem é muito alto.

Assim, uma defesa física das pontas é importante. Uma ponta pode ser seu
computador ou seu telefone móvel. Deixar esses aparelhos em locais
desprotegidos ou longe da sua atenção não é uma boa ideia.

Ao mesmo tempo, devemos considerar alguns aspectos:

1. O velho dito de que "a ocasião faz o ladrão": você não precisa ser um
   alvo, isto é, não é preciso que alguém esteja atrás de você para que
   seu equipamento seja comprometido.

2. Defesa física não significa apenas roubo do seu aparelho, mas também a
   instalação de equipamentos físicos de grampo.

3. Mesmo que você defenda fisicamente o seu equipamento, ele ainda estará
   sujeito a invasões de software. Tirando o roubo de equipamento para
   fins econômicos, as ameaças de software são as mais importantes.
   Mas ter um mínimo de cuidado com a defesa física não faz mal a ninguém.

Ou seja, a defesa física é importante, necessária mas não suficiente para
proteger sua comunicação.

### Defesas Computacionais

Do que adianta ter um equipamento fisicamente seguro se o seu software está
cheio de problemas ou se os protocolos de comunicação utilizados são inseguros?

Você pode proteger:

* Os softwares e as informações que estão armazenadas no seu computador.
* As mensagens que entram e saem do seu computador.

As mensagens e o conteúdo armazenado no computador podem ser protegidas usando
criptografia.

Não existe um único tipo de criptografia, protocolo ou programa de computador
que faça isso. Também não existe um único sistema criptográfico que proteja
todas as informações do computador.

Em algumas situações você até pode agregar diversas comunicações num único
canal criptografado, mas infelizmente em geral a criptografia precisa ser feita
caso a caso. Ao longo do guia veremos diversas ferramentas que possuem suporte
a criptografia.

### Defesas Jurídicas

A interceptação e retenção de dados e metadados pode ser feita legal ou
ilegalmente. Sabemos que ela pode acontecer mesmo nas situações em que a lei
proíbe.

Legislações fortes podem evitar a interceptação generalizada da população, mas
isso vai depender também do contexto político.

É importante conhecer seus direitos. Aqui isto não será detalhado, mas deixamos
referências de onde você pode encontrar essas informações.

### Defesas Sociais

As defesas sociais são aquelas tomadas por todo um grupo, seja um conjunto de
amigos, família, empresa, grupos de interesse, comunidade ou mesmo de toda uma
sociedade.

Essas defesas podem ser combinadas, o que é fácil de se fazer num grupo
pequeno, para que exista uma uniformidade no padrão de segurança daquele
grupo. Conforme o número de pessoas aumenta, ter uma mesma política de
segurança passa a ser mais difícil.

É importante levar em conta que o nível de segurança tende a ser o nível de
segurança do elo mais fraco. Ou seja, mesmo que algumas pessoas tenham um
nível mais alto de autodefesa, o grupo continuará bem vulnerável por conta das
pessoas com nível mais baixo.

Isso pode ser resolvido por compartimentalização, o que nem sempre é bom,
mas também pela busca de um padrão mínimo de segurança que todas as pessoas
do grupo queiram concordar, consigam aprender e pratiquem.

# Resumo

1. Temos que considerar a interceptação ou grampo como entidade sempre
   presente na comunicação, independentemente dele estar ou não ocorrendo.

2. Os ataques fundamentais da vigilância das comunicações são
   a interceptação de dados e a de metadados. Dados constituem o conteúdo
   da mensagem e metadados são informações sobre a mensagem, por exemplo
   endereçamento. Ambos os tipos de interceptação são perigosos.

3. Contra a interceptação de mensagens existem defesas físicas, computacionais,
   jurídicas ou sociais.

# Atividades

1. Qual seria o "Kit Básico" de segurança que toda a pessoa de um grupo
   deveria ter, por exemplo colegas de trabalho? Esboce um grupo hipotético
   e um kit básico de procedimentos e ferramentas de segurança.

   Você pode se basear nas atividades de capítulos anteriores caso você
   já tenha esboçado seu Checklist de Segurança.

# Referências

* [Telefone sem fio (brincadeira) - Wikipédia, a enciclopédia livre](https://pt.wikipedia.org/wiki/Telefone_sem_fio_(brincadeira)).
* [Vigilância das comunicações pelo Estado Brasileiro e a proteção a direitos
  fundamentais](http://www.internetlab.org.br/wp-content/uploads/2016/01/ILAB_Vigilancia_Entrega_v2-1.pdf).
  Em especial os quadros das páginas 10 a 13.
