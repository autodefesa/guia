# Autenticação com Senhas

## Funcionamento

Para ter acesso a determinados sistemas ou lugares, pode ser necessário
fornecer algo para que ocorra uma **autenticação**, isto é, uma permissão
de acesso.

O processo de autenticação consiste em fornecer uma prova de acesso a
um sistema e consequentemente receber a permissão de acesso ao mesmo.

Existem vários tipos de autenticação:

1. A autenticação pode ser baseada em algo que você carrega: por exemplo
   um cartão de crédito, um crachá ou documento de identificação.

2. A autenticação pode ser baseada em algo que só você ou um grupo
   restrito de pessoas sabe. Chamamos essa informação de **senha**.

3. Ela também pode se basear em alguma característica física sua
   e nesta caso estamos falando de **biometria**.

Neste curso falaremos apenas sobre senhas, que é a forma de autenticação
mais utilizada na comunicação digital.

Biometria pode ser forjada e algo que você carrega no bolso pode ser roubado.
Mas extrair uma senha da sua mente já envolve mais trabalho. Daí o poder das
senhas!

A ideia de uma senha é muito simples: só você ou um grupo restrito de pessoas
tem acesso à senha, isto é, só quem deve ter a permissão de acessar um sistema
é quem deve ter as credenciais de acesso.

## Ataques

Alguns sistemas são baseados num único fator de autenticação. Outros em dois ou
até três: algo que você carrega consigo, algo que você sabe e algo que faz
parte de você.

Note que, no limite, todas essas três características podem ser roubadas ou
forjadas, com inúmeros graus de sofisticação. Ou seja, a autenticação a
um sistema será sempre limitada.

Os principais ataques à autenticação com senha são:

* Esquecimento: você esqueceu sua senha e ficou fora do sistema!

* Ataques marotos: alguém pode simplesmente te induzir a fornecer
  a senha. Por exemplo num sistema falso mas que pareça ser o original.

  Ou alguém que pergunte a senha pra você, por exemplo se oferecendo
  para avaliar se a sua senha é boa ou ruim. Existem até sites que
  se oferecem a isso. Não confie neles! Não confie em ninguém para
  guardar ou avaliar sua senhas!

* Surfistas de ombros, do inglês **shoulder surfing**, é o ataque para
  obtenção de senhas baseada no fenômeno do zoião, isto é, em alguém
  ou algum dispositivo observando diretamente você digitando sua senha.

  Existem também métodos indiretos para fazer essa observação.

  Um método simples para evitar esse tipo de safadeza é tapar a visão
  do teclado e das mãos no momento da digitação. Por exemplo, você pode
  tampar seu laptop com a tela enquanto digita a senha.

* Interceptação no meio de transmissão ou recepção de uma senha. A senha pode
  ser digitada num teclado que possui um grampo do tipo keylogger instalado.
  Ou a senha pode ser enviada para outro local sem que haja criptografia
  implementada na transmissão. Ou mesmo essa criptografia pode ser ruim e
  revelar o conteúdo da senha.

* Alguém pode extrair senhas em interrogatórios. Ou achar senhas salvas
  no próprio dispositivo de acesso, em anotações ou até em lixo de escritório,
  acredite se quiser!

* Às vezes, uma senha nem precisa ser roubada. Ela pode ser descoberta:
  * O chamado "Ataque de Força Bruta" computacional consiste em descobrir uma
    senha usando uma "busca" por tentativa e do erro. Computadores conseguem
    tentar várias combinações num período de tempo curto.

    Se o tamanho de uma senha for curto, o que pode ocorrer em sistemas
    que impõem um tamanho máximo, então o tempo para descobrir a senha
    por tentativa e erro também é curto.

    É claro que tentar todas as combinações possíveis pode demorar muito
    tempo. Para reduzir o intervalo entre várias tentativas e um sucesso,
    os atacantes partem de um conjunto de hipóteses sobre uma senha.

    Uma das hipóteses é que a senha é feita de uma ou mais palavras conhecidas.

    Existem os chamados dicionários, que são conjuntos de senhas ou palavras
    usadas como base nas tentativas. Esses dicionários podem ser compostos
    por conjuntos de senhas mais comuns encontradas por aí ou estatisticamente
    relevantes. Sim, existem estatísticas sobre senhas populares! E podemos
    dizer que uma das piores características de uma senha é a sua popularidade!

    Assim, ataques de força bruta podem tentar inúmeras combinações de senhas
    e palavras comuns.

    Ataques mais refinados podem ainda utilizar outros padrões estaticamente
    conhecidos para diminuir ainda mais o conjunto de possibilidades de
    descoberta.

  * Se você usa como senha alguma informação sobre a sua vida, de pessoas
    próximas ou algo do tipo, você estará automaticamente diminuindo a
    segurança da sua senha.

    Muita gente usa datas importantes, como aniversários e outras
    informações pessoais como senhas, porque são mais difíceis de serem
    esquecidas. Mas, com isso, acabam se esquecendo que esse tipo de
    escolha facilita a obtenção de senhas por algum atacante que consiga
    obter essas informações.

    Mesmo que você misture datas e informações pessoais conhecidas,
    a quantidade de tentativas necessárias para descobrir sua senha
    é baixa o suficiente para uma série de tentativas automatizadas
    ter sucesso.

  * Um "espaço de senhas" de um dado programa é composto por todas as senhas
    posíveis de todos os tamanhos até o tamanho máximo suportado pelo sistema.

  * Um ataque computacional simplificado de "força bruta" é aquele que tenta
    todas essas opções possíveis, sem priorizar nenhum tipo de combinação
    específico, e tende a demorar muito, muito mesmo.

  * Ataques computacionais de força bruta mais sofisticados fazem algumas
    _suposições_ acerca da senha utilizada. Exemplos de suposições:
    * De que a senha é composta por palavras.

    * De que as palavras estão num dado idioma.

    * De que a senha tem um tamanho médio/limite (por exemplo de 8 a 16
      caracteres).

    * De que a senha contém informações específicas sobre a vida da pessoa
      (datas de nascimento, nomes de parentes etc). Este tipo de suposição
      costuma ser feito para ataques direcionados a pessoas específicas.

  * A cada escolha que você faz para seu padrão de senhas, o
    espaço possível de combinações diminui. Existem menos palavras
    em português de até 20 caracteres do que sequências de caracteres
    quaisquer de até 20 caracteres.

  * Atacantes, em princípio, não sabem exatamente quais padrões de
    senha alguém utiliza, mas tem uma noção estatística dos padrões
    mais utilizados por populações de pessoas (oriundos de vazamentos
    de bancos de dados de senhas etc). Então podem se basear nestes
    padrões para fazer suposições iniciais.

  * A partir dessas suposições, podem utilizar "dicionários" (_wordlists_,
    listas de palavras) otimizados especificamente para quebra de senhas
    (por exemplo ordenando as tentativas ao usarem as palavras mais
    comuns), juntamente com outras técnicas para reduzir o "espaço
    de busca" e aumentar as chances de encontrar a senha.

  * Outros ataques incidem sobre o "hash", uma espécie de codificação
    da senha que fica armazenada:
      * Buscam então, por força bruta ou outros meios, encontrarem
        qualquer sequência de caracteres que produza o hash,
        isto é, a versão codificada da senha.

* Em casos extremos, senhas podem ser obtidas por ameaças diretas:
  * Ataques físicos de força bruta são aqueles em que pessoas são coagidas a
    revelar senhas.

## Defesas

Como proteger uma senha de roubos? Aliás, o que é uma boa senha?

Podemos pensar em algumas características importantes para uma senha decente:

1. Memorizável: uma senha muito difícil de lembrar pode levar ao seu esquecimento
   e ser difícil de digitar.

   Já uma senha muito fácil de lembrar também pode ser muito fácil de alguém
   descobrir. Senhas muito fáceis em geral também tem um tamanho pequeno,
   então pense num tamanho mínimo e memorizável quando criar sua senha.

2. Difícil de descobrir: quanto mais difícil de descobri-la, melhor, mas isso
   pode acarretar numa complexidade da senha que a torna difícil de lembrar.

3. Pouco ou não compartilhada: se você usa a senha para uma coisa, e uma única
   coisa apenas, é mais difícil dela ser descoberta. Quanto mais compartilhada,
   maior o risco, pois a superfície de ataque à senha aumenta.

   Esta característica vem diretamente do princípio da compartimentalização:
   se uma senha for comprometida, o dano estaria restrito apenas a um ou
   poucos sistemas.

   Uma senha roubada pode ser usada como tentativa para invadir outros sistemas.
   Se você usa a mesma senha para mais de um sistema, e ela for roubada, trate
   logo de mudar a senha em todos esses sistemas.

A verdade é que não existe uma regra geral para uma boa senha. Porque qualquer
regra acaba fornecendo um padrão para a construção de senhas. E aqui queremos
justamente libertar sua imaginação do máximo possível de padrões.

O importante é respeitar ao máximo essas três características: ser memorizável,
difícil de descobrir e minimamente compartilhada entre pesssoas e sistemas.

É importante que ninguém saiba qual é o seu padrão de senhas; se você usa
palavras; se essas palavras estão em um ou mais idiomas; se são inventadas; se
você usa símbolos; se você usa números etc.

Misturar palavras (reais ou inventadas), assim como símbolos, aumenta
significativamente as chances de resistir a ataques de força bruta.

Se você confia e treinou bem sua memória, pode até usar somente sequências
aleatórias de caracteres, pois aí não há escolha de dicionários etc de
atacantes que abrevie os ataques de força bruta.

Se você administra um sistema, certifique-se de que ele use as funções hash ou
de expansão de senhas que atualmente sejam consideradas suficientemente fortes.

### Exemplos de criação de senhas

Vamos dar alguns exemplos de criação de senhas razoáveis. Estas senhas que vamos
gerar agora não são seguras, já que as estamos divulgando
não só para você, mas para o resto do universo!

A intenção aqui é mostrar algumas técnicas possíveis para a criação de senhas
para que você possa descobrir qual é a forma que funciona mais para você.

Aqui vão algumas técnicas:

1. Crie uma sequência de símbolos o mais aleatória possível. Por exemplo
   **6e"I[g[r 7YV!7P**.

   Muitos sistemas permitem que a senha possuam espaços e até combinações de
   caracteres especiais, o que é interessante. Mas, nesses casos, você pode se
   confundir se tiver de digitar a senha em teclados ou sistemas com diferentes
   configurações.

   Essa senha pode ser gerada de várias maneiras. Mas tome cuidado que
   às vezes a nossa seleção manual pode não ser tão boa. Por exemplo,
   digitar a esmo no teclado pode produzir sequências bem preditáveis
   pelo fato de nossa digitação estar "viciada" a teclar estatisticamente
   em um grupo restrito de teclas ou a seguir um padrão de digitação
   já bem determinável.

   Assim como é fácil gerar essa senha, também é fácil programar um computador
   para fazer o mesmo e tentar usar senhas geradas aleatoriamente para acessar
   sistemas.

   Esse tipo de senha também tende a ser difícil de ser lembrado,
   então seu uso deve ser avaliado com cuidado.

2. Crie uma palavra qualquer. Isso mesmo, invente uma palavra!
   Por exemplo **fletuciladamente**. Não faço a mínima ideia
   do que ela significa. Na real, ela não quer dizer nada.
   Tanto melhor!

   A facilidade aqui é que temos uma memória muito
   boa para decorar palavras pronunciáveis. E com pouca dificuldade
   podemos criar senhas de tamanho razoável.

   Note que existem muitos padrões gramaticais que são aplicados sem que
   levemos em conta ao criarmos palavras.

   Por exemplo, há uma tendência de uma ou duas consoantes serem seguidas por
   uma vogal e assim por diante. Em tese não seria difícil para um atacante
   construir um programa que reproduza essas regras para construir um
   dicionário de palavras pronunciáveis.

3. Que tal misturar essa palavra com alguns caracteres?
   Podemos pensar em **flEtuci|0d4mente**.

   É comum a substituição de letras por seus "equivalentes" em números, isto é,
   por números cuja forma se assemelha às letras distorcidas.

   Assim, *E* pode ser trocada por *3*. Mas muito cuidado em basear sua
   segurança somente nesse tipo de troca, já que ataques minimamente
   sofisticados podem tentar a mesma coisa sem nenhuma dificuldade!

4. Que tal usar várias palavras, de preferência inventadas?
   Podemos pensar em **impelícia devora ecriterbeção***. Apesar de
   ser uma senha grande, ela não é tão difícil de memorizar, especialmente
   porque temos uma tendência de fazer associação de ideias mesmo com
   palavras sem significado.

   Também, em tese, não seria difícil fazer um programa que gere esse
   tipo de combinação.

   Você pode obter palavras buscando-as aleatoriamente num dicionário,
   tendo o cuidado de não repetir os padrões que temos ao abrirmos livros,
   mas ao invés disso usando, por exemplo, sequências de jogadas com dados
   para determinar o número de uma página, de uma linha, etc.

   Existe até um método chamado de **Diceware** que é baseado justamente
   em jogadas com dados e listas de palavras.

Existem muitas outras técnicas possíveis. Qual usar? A resposta é muito
pessoal. Se você puder escolhar, escolha o maior número possível de
técnicas ou tente usar algo mais aleatório possível que você consiga decorar.

### Memorizando senhas

O limite da senha depende da capacidade e vontade de cada pessoa.

Se você tiver dificuldades com memorização mas quiser tentar uma senha mais
complexa, você pode tentar primeiro decorar a senha e só quando se sentir
seguro ou segura proceder com a alteração no respectivo sistema.

Tanto a criação quanto a memorização de senhas são processos que, para funcionar
bem, dependem de autoconhecimento: qual é o nosso processo de criação de senhas?
Quais são aquelas que conseguimos memorizar? Isso é algo bem pessoal.

Uma maneira válida para memorização, porém controversa, é a anotação de senhas.
Controversa porque você pode perder ou se esquecer de apagar a anotação.

Contudo, pode ser razoável você manter a senha com você durante o processo de
memorização.  Mas isso depende muito da situação em que você se encontra. Se o
fizer, lembre-se ao menos de queimar o papel assim que tiver decorado a senha!

### Compartilhamento e gerenciamento de senhas

Tudo bem. Já temos uma ideia do que precisamos para melhorarmos a qualidade
das nossas senhas. Mas teremos de fazer isso para todas as nossas senhas?

Vivemos num mundo de muitos sistemas, muitas contas, e por isso mesmo temos
que ter muitas senhas. O que fazer se devemos evitar ao máximo reutilizar
a mesma senha em diversos serviços?

Podemos pensar em duas abordagens:

1. Usar **círculos** ou **níveis de senhas**. A ideia aqui é reduzir a
   reutilização de senhas dependendo do nível que um serviço for crítico
   aplicando o princípio da compartimentalização.

   Serviços mais básicos, com os quais você não se preocuparia tanto com
   invasões, poderiam utilizar uma mesma senha. Talvez essa senha não
   precise ser tão complexa.

   Um nível intermediário seria composto apenas por sistemas nos quais
   a perda dos dados seria mais danosa e por isso teria uma senha
   mais complexa. Um nível avançado poderia ter uma senha mais difícil
   ainda.

   Esse agrupamento também pode ser feito pelo nível de segurança que
   determinado sistema oferece. Existem muitos sistemas de baixa qualidade
   que podem ser invadidos a qualquer momento.

   Note que, potencialmente, o comprometimento da senha de um nível
   ou compartimento pode comprometer a segurança de todos os serviços
   que se encontram no mesmo nível.

2. Para alguns serviços, você pode até escolher o salvamento da senha
   nos seus dispositivos de acesso, caso você os confie para isso.

   Se o seu dispositivo estiver com armazenamento criptografado e
   não se tratar de um serviço sensível, talvez essa não seja uma má ideia.

   Neste caso você não precisa de uma senha compartilhada.
   Neste caso você talvez possa até esquecer sua senha!

   Caso você precise resetá-la, por exemplo ao perder ou ganhar um dispositivo,
   você pode solicitar ao serviço uma senha nova por email, por exemplo. Mas
   cuidado para não esquecer a senha do email ou do canal de comunicação que
   você use para receber a nova senha. E mude essa nova senha assim que
   recebê-la!

3. Outra abordagem é utilizar um software de gerenciamento de senhas.

   Basicamente, ele cria um pequeno banco de dados com todas as
   suas senhas. Esse banco de dados é protegido criptograficamente
   com uma **senha mestre**. Você precisa lembrar dessa senha mestre
   para "destravar" esse banco de dados, mas em seguida pode acessar
   qualquer uma das senhas salvas e copiá-las para utilização em
   outros programas, como, por exemplo, num navegador web.

   Gerenciadores de senha também podem ser usados para criar senhas aleatórias
   para você, poupando seu esforço para criar senhas apenas para aquelas que
   você precisa decorar.

   É possível até que você nunca precise ver uma senha: seu gerenciador
   pode criá-la sem que você a veja e você pode copiá-la e colar
   noutro software do seu computador.

   Assim, você minimiza não só a quantidade de senhas que precisa
   decorar, mas também diminui o contato desnecessário com a sua
   senha.

   Existem vários gerenciadores de senha disponíveis.

   Dê preferência a gerenciadores de senhas que sejam instaláveis
   no seu computador para evitar a possibilidade da sua senha
   mestre ser capturada por alguma invasão ou má intenção de
   um serviço remoto de hospedagem de senhas.

   Também dê preferência para softwares livres, pos eles permitem
   a auditoria do código.

   Os gerenciadores são ótimos também no processo de memorização
   de senhas. Com eles você não tem motivos para anotar uma senha
   num papel, pois a qualquer momento pode consultar a senha
   usando o próprio gerenciador.

A melhor abordagem, na nossa opinião, é usar o gerenciador de senhas.

Assim, podemos minimizar o número de senhas sem perda significativa
de segurança.

Imagine então a seguinte situação onde uma pessoa possui um conjunto
mínimo de senhas:

1. Senha para ligar e usar um computador com armazenamento criptografado.

2. Senha para destravar o gerenciador de senhas do computador.

3. Senha para ligar e usar o telefone móvel com armazenamento criptografado.

4. Senha do banco para pode realizar operações financeiras sem precisar
   consultar nenhum dispositivo.

Essa pessoa precisa saber apenas 4 senhas! O inconveniente é que, para
usar outras senhas, ela precisará ter acesso ao seu computador e ao seu
gerenciador de senhas.

Além disso, é recomendável que ela possua um backup do seu computador,
incluindo a base de dados do gerenciador de senhas. Já que, no caso
de perda do computador, ela pode recuperar suas senhas a partir
de um backup criptografado.

Lembre-se que o arquivo de senhas deve estar disponível em algum lugar
acessível. Se você deixá-lo apenas em volumes criptografados cujas senhas você
não souber de cabeça irá criar um problema lógico que poderá impedir acessos
futuros às suas senhas, já que para acessar o arquivo de senhas você precisaria
acessar uma senha que está dentro do próprio arquivo de senhas. Portanto,
certifique-se que você sempre consiga acessar um backup do seu arquivo de
senhas só com senhas que você guarda na própria cabeça.

A quantidade total de senhas que uma pessoa precisa decorar vai
depender muito do planejamento que uma pessoa fizer e do que for
mais confortável para ela.

Algumas pessoas podem ter apenas uma ou duas senhas que permitam acessar o
gerenciador de senhas.

Outras podem necessitar de mais senhas na cabeça para uso diário. Pode até
acontecer que você comece a decorar senhas de tanto usá-las.

### Digitando senhas

O momento da digitação de uma senha é especialmente vulnerável já que é
a hora em que ela passa da sua cabeça para o computador, passando por um
meio intermediário onde ela possivelmente possa "vazar". Alguns cuidados
ajudam:

* Apenas digite sua senha quando o computador estiver esperando por ela; pode
  acontecer de você digitar seu usuário e logo de cara começar a entrar com a
  senha mesmo antes do computador pedi-la. Nesse meio tempo, pode acontecer da
  senha aparecer na tela, principalmente em computadores mais lentos.

* Experimente digitar qualquer coisa no lugar da senha para ver como o
  computador se comporta; existem ocasiões em que o computador preenche cada
  caractere digitado com um asterisco (\*), outras a senha pode até aparecer
  normalmente (!); depois de se certificar de que está tudo bem com o pedido de
  senha, apague tudo o que você digitou anteriormente e entre com sua senha
  correta.

* Verifique se as teclas Num Lock e Caps Lock do teclado estão ligadas,
  desligando-as caso desejado.

### Limite da confiabilidade de senhas

Por mais que uma senha seja longa e difícil, sempre é possível que
alguém a descubra por tentativa e erro. Mas se você tomar os cuidados da
seção anterior, vai demorar muito tempo até que alguém consiga
quebrá-la.

É possível ainda que você passe por interrogatórios ou até mesmo por
torturas. Nesse caso, dependerá apenas do seu estômago se você fornecerá
ou não a senha para o torturador. O importante, nessa discussão, é você
ter em mente que senhas nem sempre implicam na a inviolabilidade das
suas informações e que você deve ter consciência do que pode acontecer
caso seus dados sejam obtidos por terceiros. Torturas e interrogatórios
são casos extremos, porém pode acontecer de terceiros descobrirem sua
senha por outros meios, tanto pela tentativa e erro quanto por falhas de
protocolo ou falhas de implementação.

## Resumo

Boas senhas possuem as seguintes características:

1. São memorizáveis.
2. Difíceis de descobrir.
3. Pouco ou não compartilhadas.

## Atividades

1. Expanda o seu Checklist de Segurança incluindo uma estratégia de senhas
   para você. Se preferir, pense num cenário de curto prazo e noutro de
   médio ou longo prazo para que você consiga melhorar a qualidades
   e seu esquema de senhas com calma.

## Referências

* [Abre-te Sésamo: as senhas da nossa vida digital | Oficina Antivigilância](https://antivigilancia.org/pt/2015/06/abre-te-sesamo-as-senhas-da-nossa-vida-digital-2/).
