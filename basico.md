# Conceitos Básicos

## O que é segurança?

Para os fins deste guia, vamos considerar que **segurança
é a proteção** a determinadas ameaças -- ou **riscos de falha** -- em nossas atividades.

Por exemplo, alpinistas usam uma série de equipamentos que possuem funções de
proteção contra quedas. Da mesma forma, o cinto de segurança é um equipamento
feito basicamente para proteção contra acelerações bruscas.

Tanto escalar montanhas quanto viajar em aviões são atividades que envolvem riscos
de falha. Muita gente não deixa de viajar de avião por conta dos riscos de queda
justamente pelos procedimentos e equipamentos de segurança envolvidos.

Outro aspecto da segurança, portanto, é sua capacidade de nos **encorajar** a desempenhar uma
atividade ao minimizar riscos e também por nos tornar conscientes sobre quais riscos
estamos submetidos/as e do quanto estamos protegidos/as.

## Segurança versus paranóia

Isso coloca a segurança no campo oposto da **paranóia** -- já que a paranóia é justamente
o conjunto de procedimentos contra ameaças reais ou imaginárias que faz com que uma pessoa
não tome iniciativa.

O Guia de Autodefesa Digital foi feito para que você tenha consciência das ameaças
e proteções no universo da comunicação eletrônica de forma que você continue realizando
suas atividades mas de forma consciente e segura, ao invés de se isolar num universo
próprio de paranóia ou simplesmente ignorar qualquer medida de segurança e agir como quem
diz "está tudo dominado mesmo, né?".

## Praticidade e eficácia: segurança versus conforto

Nem sempre os procedimentos de segurança são os mais confortáveis. A experiência nos mostra
que os procedimentos que são ao mesmo tempo práticos e eficazes são aqueles que acabam sendo
adotados pelas pessoas.

Considere a seguinte recomendação de segurança: caso queira despistar quaisquer possíveis
espiões de te seguirem a uma reunião importante, saia 5 horas antes da sua casa, tome 4 ônibus
com destinos aleatórios e só então siga para o ponto de encontro.

Sem questionar a eficácia de tal procedimento, podemos ver logo de cara que ele é muito custoso.

Um grupo até pode adotar alguma coisa complicada assim em momentos extremos caso considere que
isso seja eficaz. Mas, se usar isso no dia-a-dia, a tendência natural é que essa convenção não
seja respeitada. E isso pode ser danoso ao grupo, pois ele passa a não seguir um acordo mas
ao mesmo tempo contar com ele para a sua segurança.

Isso pode acontecer não só num grupo, mas em algum procedimento de segurança que você escolher
adotar: ele pode ser tão difícil e complicado que depois de um tempo você larga mão.

## Viabilidade: Redução de danos

É muito comum pessoas quererem adotar um montão de práticas de segurança de uma vez só, da
mesma forma como alguém pode tentar largar um vício de uma vez -- como por exemplo parar
de fumar ou adotar a dieta vegetariana.

Mas mudanças radicais de hábito nem sempre resistem ao tempo: até a curto prazo elas podem
ser abandonadas. A experiência mostra que é muito mais viável ir aos poucos, adotando mudanças
incrementais na sua vida.

## Segurança: dado cultural

Aqueles procedimentos que são práticos, eficazes e viáveis podem acabar sendo
incorporados à própria cultura de um grupo. Exemplos mais simples disso são as
práticas de higiene, como lavar as mãos ou escovar os dentes: são medidas
preventivas práticas, baratas e eficazes contra doenças.

## Economia da segurança

Em resumo, existe uma economia da segurança: uma prática de segurança é dada
por alguns fatores básicos:

1. Custo de implementação e manutenção, ou seja, não só custo econômico mas
   também a quantidade de trabalho que dá para manter a prática.

2. Eficácia da medida, ou seja, o quanto determinada prática de segurança
   tem um efeito de eliminar a ameaça ou reduzi-la, por exemplo ao fazer
   com que o custo do ataque aumente.

3. Probabilidade de ataque, que é uma estimativa, muitas vezes bem aproximada
   ou feita de pura intuição, sobre o quanto uma ameaça é provável de acontecer.

Idealmente, queremos adotar as práticas que sejam mais eficazes e menos custosas
aos eventos de ataque que sejam mais prováveis! Esse é o resumão básico sobre
segurança!

## Resumo

Vamos fazer um resumo rápido dos conceitos apresentados até o momento?

1. Segurança é a **proteção** a determinadas ameaças -- ou **riscos de falha**
   -- em nossas atividades.

2. Paranóia, ao contrário, é o mecanismo usado para não agir por conta de
   riscos reais ou imaginários.

3. Existe uma relação entre conforto e segurança que deve ser balanceada.

4, Adote procedimentos de segurança aos poucos, reduzindo os danos de forma
sustentável ao invés de tentar mudanças radicais que não sejam duradouras.

5. Relação econômica na segurança: custo, eficácia e probabilidade de ataque.

6. Procedimentos práticos e eficazes tem mais chance de se tornarem culturais.

## Atividades

1. Liste os procedimentos de segurança que você já utiliza ou atividades que
   você realiza, mesmo que de forma cotidiana e automática, que podem ser
   consideradas como procedimentos de segurança. Exemplo: andar olhando
   discretamente para trás de vez em quando para checar se há alguém te
   seguindo.

2. Liste quais você acha que são os riscos mais prováveis à sua segurança.
   Quais seriam aqueles que você acha que deve combater primeiro?
