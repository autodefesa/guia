# Roteiros de Oficinas

Esta seção constitui um conjunto de roteiros modulares para ciclos de oficinas
teóricas e práticas, com técnica e política sobre privacidade, vigilância,
monitoramento e espionagem de massas em meios digitais.

Os temas estão divididos e podem ser aplicados na sequência ou
separadamente conforme a necessidade do grupo.

Ela abrange o uso de ferramentas mais seguras usando o sistema
operacional GNU/Linux e tópicos como criptografia, anonimato, segurança
de conteúdo e de metadados, além de uma introdução à segurança em
dispositivos móveis.

Além de recomendar práticas e ferramentas, o roteiro introduz o
“mindset” de segurança, para que as próprias pessoas possam avaliar suas
salvaguardas.

A segurança é abordada do pontos de vista técnico, sociológico,
cultural, econômico e político. Existem várias formas de se acabar como
movimentos sociais e a segurança pode ajudar não só em ofensivas
violentas como também em casos de cooptação e desmobilização.

A análise da segurança a partir dos modos de produção, da forma de
organização social e dos níveis tecnocientíficos existentes oferecem
resultados de boa acurácia (open intelligence). Justificativa: o rombo
contábil não pode ser imenso. Isso, porém, não exclui as possibilidades
do desconhecido, do secreto e do espectulativo: cabe aos grupos fazerem
suas análises, sínteses, etc.

A idéia destes roteiros é, mais do que dar o alimento, ensinar a produzi-lo.

```{toctree}
:maxdepth: 1
:glob:
oficinas/*
```

## Material de apoio

Não obrigatório, porém recomendável, ter todo o material para realizar a
oficina offline, incluindo pacotes dos aplicativos que serão instalados:

* Lousa ou flip chart.
* Caneta pra lousa branca ou caneta piloto.
* Tomadas / réguas de luz (pelo menos 15 pontos de luz).
* Canetas e cartões coloridos (azuis, verdes, amarelos, vermelhos, rosas e
  brancos).
* Hub/switch ethernet e wireless (4 portas no mínimo).
* Cabos de rede (4 de 2 metros cada).
* Pendrives de 4GB.
* Projetor / datashow.
* Hub USB.
* Discos USB externos para backups cifrados.
* Mini-NAS ou disco com:
    * Loja F-Droid com apks úteis: OsmAnd, signal, briar, etc.
    * Espelhos locais APT (debian e ubuntu).
    * Cópias de distros (Debian, Ubuntu, etc) 32 e 64 bits com integridades
      verificadas.
    * Cópia checada do Virtual Box para Windows e OSX caso alguém não tenha
      feito backup da sua máquina mas queira ter uma noção do ambiente
      GNU/Linux.
