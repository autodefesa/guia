# Telefones celulares

## Funcionamento

Como um celular funciona?

O celular é basicamente um rádio comunicador que permite a comunicação entre duas partes que
estejam distantes uma da outra. Cada uma das partes possui um número único para que possa
ser contatada. Isso provavelmente você já sabe.

O que talvez você não saiba é que, como um telefone móvel é um aparelho pequeno, ele não
tem potência suficiente para que suas ondas de rádio cheguem até destinos que estejam muito
distantes.

Para contornar esse problema foi desenvolvido o sistema de telefonia celular, onde diversas
antenas -* ou células, também chamadas de Estações Rádio Base (ERBs) -- são instaladas num
território e fazem a comunicação entre os aparelhos de telefone móveis e o resto do sistema
telefônico.

    aparelho celular <-> estação rádio base <-> sistema telefônico <-> estação rádio base <-> aparelho celular

O interessante dessa tecnologia é que os aparelhos de celular podem se mover de um lugar para
o outro sem que haja interrupção na comunicação: conforme um aparelho se afasta de uma estação
rádio base e se aproxima de outro, ele passa por um processo de troca de estação, se registrando
na ERB nova e em seguida se desregistrando da velha.

A comunicação em rede de celular pode consistir no tráfego de conversas telefônicas, mensagens
de texto simples ou dados genéricos.

Mas um aparelho de celular não faz apenas isso!

Agora que sabemos o básico da comunicação em rede celular móvel, vamos
adicionar o elemento que estava faltando no celular moderno, também conhecido
por smartphone: o computador.

    smartphone: celular clássico + computador

É lógico que mesmo os telefones celulares clássicos possuem um computador responsável pela comunicação
e funcionamento do aparelho, porém nós ressaltamos que o smartphone é um celular clássico e mais um
computador porque este é um computador muito poderoso e ainda porque o processamento dos aparelhos
modernos são feitos por dois elementos:

* Processador de banda base, ou baseband, que é o responsável pela comunicação usando os protocolos
  da rede de celular.

* Processador genérico usado para a interação com o usuário(a), como processamento de aplicativos,
  tela, fone e botões.

Os celulares modernos ainda possuem uma série de outros dispositivos:

* Adaptador para conexão de redes sem fio -- wireless/wifi.

* Sensores de posicionamento global (GPS e GLONASS).

* Sensores de movimento.

* Interfaces de comunicação de curta distância (Bluetooth/NFC).

Essa quantidade de dispositivos pode ser controlada não só pelos aplicativos que rodam
no celular como também por agente remotos.

## Ataques

No capítulo anterior vimos como o celular funciona. Ele tem tantas formas de ser atacado que
ele é **uma verdadeira catástrofe em termos de segurança e privacidade**!

Façamos uma rápida passagem pela lista de ataques a estes dispositivos:

1. Ataques ao computador: como um smartphone também é um computador, você pode pensar em todos
   os ataques possíveis que abordamos na aula passada, exceto aqueles que não se aplicam por
   depender de interfaces que o smartphone não possui.

   Ataques de acesso físico podem ser realizados com o auxílio de alguns
   equipamentos especializados em extração de dados de telefones móveis!

   Um software espião -* por exemplo um aplicativo mal intencionado -- pode
   gravar o que você fala, o que você digita, tirar fotos, registrar sua
   posição, ler arquivos e conteúdo de conversas, etc.

   Esses ataques podem ocorrer tanto nos componentes tradicionais do computador -* processador,
   memória de curto e longo prazo, etc, quanto no processador de banda base responsável pelas
   comunicações de rádio com a rede de telefonia.

2. Interceptação de dados: como qualquer meio de comunicação digital, o sistema de telefonia
   móvel está sujeito ao grampo.

   As companhias telefônicas podem realizar o grampo, uma vez que elas são donas da maior parte
   da infraestrutura por onde trafega a comunicação; mas ela também pode ser feita por qualquer
   ator que possua um equipamento específico, seja invadindo o local de uma Estação Rádio Base
   para implantar um grampo -* o que é mais arriscado pois é mais fácil ser percebido -- ou
   usando os aparelhos conhecidos como **maletas**, que nada mais são do que dispositivos para
   interceptação ativa ou passiva.

   Tanto conversas telefônicas, mensagens de texto ou dados genéricos podem ser interceptados
   dessas maneiras.

   Alguns padrões de comunicação celular possuem criptografia, como o GSM, mas que é tão fraca
   e fácil de quebrar que nós nem consideramos que essa comunicação está protegida. Comunicações
   como SMS podem basicamente serem lidas por qualquer agente que consiga interceptá-las.

3. Interceptação de metadados: por questões de cobrança, as companhias telefônicas já registram,
   por padrão, informações de ligações como origem, número de destino, data, hora e duração
   da chamada.

   No caso dos telefones celulares uma informação adicional também pode ser registrada por padrão:
   a localização aproximada do telefone através da informação sobre qual torre o telefone esteve
   conectado.

   Ainda é possível obter a localização do aparelho em tempo real e com grande precisão.

Os telefones móveis são dispositivos rastreadores **por design**: eles podem estar em contato
com várias estações rádio base ao mesmo tempo, cada uma delas registrando esse contato.
Sabendo a potência do sinal do aparelho em cada estação, é possível facilmente determinar
a posição do aparelho com precisão.

Por isso, dizemos que **o celular é um aparelho rastreador que possui a funcionalidade
de realizar chamadas telefônicas**.

## Defesas

Gente, tenho más notícias quanto aos smartphones. É muito difícil tê-los e possuir segurança
e privacidade em níveis relativamente altos.

Hoje, o telefone móvel é o calcanhar de Aquiles da segurança da informação. Não porque ele
seja a única grande vulnerabilidade existente, mas porque é aquela que não tem uma mitigação
que permita usar esse tipo de aparelho sem perda significativa de privacidade.

A solução simples consiste em não utilizar telefones celulares. Porém, hoje, está cada vez mais
difícil viver sem eles. Em poucos anos a sociedade se tornou extremamente dependente deste
aparelho de tal modo que é muito difícil viver sem ele. Assim, as medidas aqui sugeridas
são bem paliativas.

Muitas das defesas descritas na aula sobre computadores podem ser adaptadas para o contexto
dos telefones móveis pois seguem a mesma lógica, como é o caso de ter dois dispositivos:
um deles rodando poucos aplicativos e outro rodando qualquer coisa.

### Medidas básicas

Vejamos algumas medidas bem básicas para proteger seu smartphone.

* Travar a tela com senha: é melhor do que usar um padrão gráfico de
  travamento, pois as possibilidades são maiores. Na pior das hipóteses você
  estará dando um trabalho adicional para o atacante, o que pode te dar tempo
  para tomar outras medidas como apagar remotamente o conteúdo do telefone ou
  desabilitá-lo.

* Criptografar o armazenamento interno e usar memória externa só para
  aplicações não-sensíveis.

  Sistemas como o Android suportam criptografia no armazenamento interno, mas
  nem todo telefone suporta criptografia no cartão SD externo, então a
  recomendação aqui é para usar apenas a memória interna do telefone.

  Como medida adicional, em situações de risco em que você consiga se antecipar
  de um perigo de perda do telefone, tente desligá-lo antes pois isso tornará a
  criptografia do armazenamento interno ainda mais efetiva contra ataques pós-furto.

* Usar senha no SIM card: para complicar um pouco mais a vida de ladrões comuns,
  você ainda pode ativas os códigos PIN e PUK do seu SIM Card.

* Software anti-furto para desabilitar o seu telefone é uma possibilidade viável
  se você teme o roubo do celular como forma de tomarem controle dos seus dados.

  Com esses softwares, você pode configurar para que o apagamento de dados ou a
  inutilização do aparelho sejam acionados remotamente por você em caso de
  perda do dispositivo.

  Alguns desses softwares também rastreiam o seu dispositivo caso você pense
  em recuperá-lo de algum modo.

  Mas cuidado! Esses softwares também podem acabar monitorando sua vida diária!
  Informe-se antes de usá-los.

### Medidas anti-grampo ambiental

Se você for conversar pessoalmente com alguém e quiser evitar qualquer tipo de grampo que pode
estar instalado no seu telefone, pense nas seguintes possibilidades:

1. Desligar o celular quando for conversar com alguém: esta é uma medida bem simples,
   porém pode não ser totalmente eficaz: um software malicioso poderia simular o
   desligamento do telefone e ao mesmo tempo mantê-lo ligado fazendo escuta ambiental.

2. Deixá-lo num apostendo separado também é uma opção. Quanto mais afastado e isolado
   do local onde a reunião será realizada, melhor. Assim possíveis escutas ambientais
   instaladas no telefone não conseguirão captar a conversa.

3. Retirar a bateria: nem todo smartphone tem bateria removível. Aliás, a tendência
   atual é da maioria dos smartphones terem bateria interna, de modo que seja quase
   impossível desligá-los completamente.

   Naqueles que possuem, a retirada da bateria é uma medida interessante para
   evitar escutas ambientais.

4. Deixá-lo em casa em situações sensíveis: das medidas anti-grampo mais simples
   esta é a melhor: além de você ficar fora do raio de atuação de um possível
   grampo no celular, ele não estará rastreando a sua localização: para um espião
   que acredite estar de rastreando, ele poderá acreditar que você está em casa.

   Melhor ainda, ele terá dificuldades de descobrir que algumas pessoas estão
   reunidas num mesmo local se todas deixarem seus telefones em casa.

Falaremos em aplicações de comunicação mais seguras para evitar grampos em conversas
telefônicas na aula sobre mensageria.

### Softwares: medidas básicas

O que mais afeta hoje a privacidade de toda uma população é a quantidade de
dados coletada automaticamente por aplicativos.

Por um lado, muitos aplicativos oferecem várias funcionalidades gratuitamente.
Por outro, você está trocando com eles a sua privacidade. Pior que isso, até
serviços pelos quais você paga estão coletando informações a seu respeito.

A arquitetura dos smartphones foi pensada exatamente para isso. Então é muito
difícil de utilizá-la sem o efeito colateral de fornecer dados em excesso para
empresas e governos.

Vou deixar então algumas dicas que não eliminam toda a vigilância realizada pelo
seu telefone, mas que reduzem um pouco:

1. Preste atenção nas permissões solicitadas por cada aplicativo que você
   instalar.

   Repare que aplicativos maliciosos podem encontrar maneiras de burlar
   restrições no sistema.

2. Quanto menos aplicativos você usar, melhor. Isso depende do que você quer
   para a sua vida e para as pessoas que estão à sua volta. Mesmo aplicativos
   que pareçam ser inofensivos podem causar danos.

3. Dê preferência para softwares livres ou abertos. Além da loja de aplicativos
   padrão do seu telefone, você pode instalar lojas que oferecem apenas softwares
   livres, como é o caso do **F-Droid** para o Android. Sempre procure uma
   opção livre antes de buscar por um software proprietário.

### Softwares: medidas avançadas

Se você quiser realmente usar um smartphone com um padrão de segurança mais
alto, considere usar um telefone com sistema operacional livre ou aberto. Você
pode, por exemplo, usar uma versão do Android padrão sem as customização das
operadoras ou dos fabricantes, que tendem a colocar softwares com muitos bugs
ou aplicativos com funcionalidades associadas à vigilância.

Você pode usar algum outro sistema baseado em Android como o **LineageOS**.

Atenção que, ao substituir o sistema operacional do seu smartphone, você pode
perder a garantia do aparelho.

Ainda, a substituição exige que você desbloqueie o aparelho, isto é, fazer o
**root** nele, o que pode diminuir a segurança pois aplicações terão mais
privilégios de execução. Nesse caso, tome cuidado com as aplicações que você
vai usar. Recomendaria nem usar a loja de aplicativos padrão, mas sim apenas
aquelas que oferecem software livre e formas autenticadas de obteção de
softwares, como o **F-Droid**.

Nas outras aulas serão apresentados softwares de comunicação e navegação
específicos para telefones móveis.

## Resumo

1. O telefone móvel é um dispositivo de rastreamento.

2. É possível ter um uso menos nocivo do telefone em termos de privacidade,
   mas não existe possibilidade razoavelmente segura.

## Atividades

1. Expanda seu Checklist de Segurança para incluir seu telefone móvel.
