# Limites da segurança

## Introdução

Já falamos sobre várias características da segurança e também sobre aspectos
sobre a privacidade. Agora precisamos mostrar quais são os seus limites.

Queremos responder à seguintes perguntas:

1. É possível afirmar com certeza se estou ou não sendo invadido(a)?
2. Existe segurança totalmente eficaz, isto é, 100% infalível?

### Ceticismo e ignorância

Digamos que a segurança pode ser o ramo da ciência que mais se beneficie
com o **ceticismo**. Quanto mais duvidarmos da realidade e dos fatos, mais chances
teremos de criar e usar procedimentos seguros.

Lembre-se que **ceticismo** é diferente de **paranóia**. No nosso caso, pensaremos
no ceticismo como uma dúvida constante mas que não impede que sigamos em frente.
Podemos até assumir algumas coisas como verdades práticas, mas sempre deixaremos
espaço para a dúvida.

Pois bem, neste nosso ceticismo, diremos que é muito difícil provar que algo não
existe. Podemos dizer que determinada coisa não existe por nunca termos nos encontrado
com ela, mas nada garante que não possamos encontrá-la se nos dedicarmos por tempo
suficiente.

Em outras palavras,

    A ausência de evidência não é a evidência de ausência.

Assim, podemos descobrir se a nossa segurança está sendo violada. Mas nunca saberemos
de antemão se ela está ou não sendo violada. E nem saberemos a quantidade de violações.

Em outras palavras, a procura por evidências de invasões será sempre incerta e incompleta.
Podemos contudo, nos proteger das ameaças mais factíveis, baratas e prováveis.

Mas calma! A quantidade de invasões em curso pode sim ser zero! Estamos falando apenas
da nossa **ignorância** quanto à existência e quantidade de invasões e não nas invasões
que de fato possam estar acontecendo.

Da mesma forma que não podemos assumir que os possíveis oponentes desconhecem nossos
procedimentos de segurança, não podemos assumir que conhecemos todas as ameaças que
operam contra nós.

Podemos, através da investigação -- as chamadas **auditorias de segurança** --
descobrir várias invasões de segurança que podem estar ocorrendo contra nós.
**Mas nunca conseguiremos saber se descobrimos todas!**

Assim, por simplicidade, podemos até assumir que todos os seus sistemas estão
invadidos ou em vias de serem invadidos. E a partir dessa perspectiva
desoladora começar a pensar em procedimentos de segurança. Esta é a abordagem
**prática** que adotaremos, isto é, ela é **pragmática**.

É possível descobrir se você está sendo invadido(a). Mas nada garante que isso seja
descoberto. E não é possível dizer se você não está sendo invadido(a).

Sim, viver é perigoso. Podemos diminuir nossa ignorância, mas nunca por completo.

    O ceticismo pragmático é a nossa melhor defesa contra uma ignorância invencível.

### Sistemas abertos

Além disso, o mundo é um sistema aberto. Nós e nossos sistemas são também
abertos. Porque para interagir no mundo é necessário ter alguma abertura.
Qualquer abertura afeta o funcionamento interno de um sistema e eventualmente
pode ser usada para perverter o funcionamento do sistema.

Nesse sentido, toda interação é uma construção de interdependência e sujeita
a riscos. Contra isso não há o que fazer além de ter consciência e levar esse
fato em conta ao pensar sua segurança.

Ou seja, **não existe segurança completa** em sistemas que admitem graus de abertura.

E, na natureza, apenas sistemas fechados e isolados não trocam informação com o
mundo exterior, o que não é o caso de sistemas vivos, sociais, informacionais.

Talvez o mais fundamental sobre segurança é o fato de que, até onde vai
a nossa ignorância, qualquer sistema é incompleto e possui falhas.

Por isso, não existe sistema 100% seguro. Mas lembremos que **poder** não é o
mesmo que **ser**. Um sistema **pode** ser invadido, mas não quer dizer que
ele **esteja** sendo invadido.

## Arquiteturas abertas e fechadas

Mesmo quando já conhecemos o funcionamento de algo ainda assim temos dúvidas se
não nos debruçamos o suficiente sobre todas as possibilidades de mal
funcionamento.

Assim, o mero fato de não conhecermos o funcionamento de algo já nos lança
dúvidas se aquilo não possui uma falha que pode ser explorada por alguém.

Por isso, não podemos confiar em sistemas que não ofereçam meios para
serem inspecionados.

No caso de hardware e softwares para comunicação digital, como veremos ao longo
do curso, é importante que eles possuam arquitetura aberta e possam ser não
apenas inspecionados mas também corrigidos caso sejam encontradas falhas.

Isso implica que precisamos dar preferência para os chamados **softwares livres
e abertos** e também aos **hardwares livre ou aberto**. Eles não são
necessariamente mais seguros -- apesar de muitos serem -- mas eles permitem que
sejam investigagos com mais facilidade.

## Resumo

1. Não existe segurança total ou sistema infalível.

2. Em segurança, recomenda-se adotar o ceticismo pragmático: é possível
   descobrir se você está sendo invadido(a), mas isso nem sempre acontece. Pior
   que isso, não é possível dizer que você não está sendo invadido(a).

3. Temos que evitar basear nossa segurança em sistemas fechados que não
   podem ser auditados.

4. Não assuma de antemão como sendo malícia uma vulnerabilidade encontrada
   que pode ser explicada por incompetência ou por fatos naturais.

## Atividades

1. Pense, mas não escreva, nas situações nas quais você descobriu que estava
   sendo invadido/a. Como você reagiu? Sua segurança melhorou desde então?
