# Conceitos Avançados

## Introdução

Até aqui tratamos do comportamento de práticas de segurança de forma bem genérica:
elas são defesas viáveis, práticas e eficazes contra ameaças prováveis.

Tudo bem, mas quais são essas práticas? Como devemos adotá-las? Por onde começar?

Trataremos agora de mais alguns conceitos importantes que vão nos ajudar
a entender como priorizar as defesas consideradas mais importantes.

Estes são tópicos avançados e, se preferir, você pode pulá-los!

## Segurança se dá por níveis (camadas)

Em certo sentido podemos pensar em segurança como a estabilidade de um
edifício: quanto mais molenga for o terreno onde ele está construído, mais
chances de queda de todos os seus andares. Da mesma forma, se suas fundações
forem fracas, todos os andares estarão comprometidos. Imagine agora um prédio
onde apenas o último andar é muito fraco, e que seu desmoronamento não implique
no abalo dos andares inferiores, ou que esse abalo não seja significativo.

Diríamos então que, neste prédio hipotético, a solidez dos andares superiores
dependem não só da sua própria solidez mas também da solidez dos andares
inferiores. Mesmo que um andar seja bem feito ele sofrerá de problemas
estruturais se os andares inferiores foram mal feitos.

A segurança, e especialmente a segurança digital, funciona de modo muito
parecido: se as bases da segurança não são sólidas, então as proteções
oferecidas por um procedimento que se encontra no meio do seu "edifício da
segurança" não será muito eficaz.

Por exemplo, não adianta muito você ter uma senha de acesso muito poderosa se
você a utiliza num computador que já estiver invadido, por exemplo, por
softwares que registram e extraem o que é digitado, os chamados "keyloggers".

Em outras palavras, a segurança se dá por níveis, ou camadas: defesas que se
referem a um determinado nível do edifício -- ou pilha -- da segurança não
protegem de ameaças que operam nos níveis inferiores.

Isso acontece quando há uma hierarquia de dependências entre coisas: se uma
dependência falha, então aquilo que depende também tende a falhar.

Ou seja, em muitas situações o mais importante é melhorar a segurança a partir
das camadas, ou níveis, mais baixos, numa abordagem infraestrutural.

É claro que em algumas ocasiões existe uma mútua dependência. Nesse caso, dizemos
que as medidas de segurança mutuamente dependentes se encontram no mesmo nível.

## Simplexidade: a Complexidade Necessária

É válido observar que sistemas, procedimentos ou vidas mais complexas e
interdependentes podem ter mais vulnerabilidades do que situações mais simples
e independentes.

Um dos **princípios** que vão nos guiar daqui em diante é o da **complexidade
necessária**, ou seja, tentaremos sempre buscar o caminho mais simples, porque:

1. O mais simples é o mais simples de entender.
2. Sendo mais simples de entender, é mais simples de conhecer suas fraquezas e defesas.
3. Mais simples pode ser mais robusto por ser mais simples de adotar e manter.
4. Simplicidade pode ter relação direta com economia e custos: mais simples, mais barato,
   não só financeiramente.

Aqui, **simples será usado como um sinônimo para complexidade necessária**, já
que mesmo as coisas mais simples possuem sua complexidade. Estamos falando de
**simplexo**, a noção complexa do simples que não é simplória.

Recomenda-se, então, que medidas de segurança sejam avaliadas também no quanto
de complexidade necessária elas possuem: naquilo que elas são mais complicadas,
são mais complicadas por um motivo importante e necessário?

Assim, se duas medidas de segurança que possuem a mesma capacidade de defesa,
tenderemos a escolher a mais simples, pois ela não acarreta em complexidades
desnecessárias.

Esta é basicamente a versão, no campo da segurança, do princípio da **Navalha
de Occam**, onde uma hipótese é escolhida entre diversas concorrentes e equivalentes
pela sua simplicidade. Ou seja, o princípio do universo econômico. Mas é bom
lembrar que este é um princípio guia e não necessariamente uma verdade absoluta.

## Interdependência: Compartimentalização e Pontos de Falha

A partir do que discutimos até o momento fica como consequência o fato de que
a interdependência entre sistemas e pessoas pode acarretar em falhas em cascata
quando existe alguma violação de segurança: a invasão num ponto pode comprometer
todos os outros pontos dele dependentes.

Aplicando o princípio da complexidade necessária, podemos pensar também no
conceito de **dependência necessária**, sendo a adoção de procedimentos de
segurança que sejam dependentes ou interdependentes apenas o quanto for preciso.

Os termos usados para **dependência necessária** são **compartimentalização** ou
**desacoplamento**, isto é, isolar partes, pessoas ou procedimentos de segurança
de um sistema para que falhas numa parte não impliquem na falha de outras ou mesmo
do todo.

A cada sistema que for crucial para o funcionamento de outro, ou seja, a cada
sistema do qual outros dependam, daremos o nome de **ponto de falha**. O ideal
é sempre minimizarmos os pontos de falha. O pior caso é aquele conhecido como
**Ponto Singular de Falha** ou **Singular Point of Failure (SPF ou SPOF)**:
aquele cuja falha implica na falha de todo o sistema.

O corpo humano é um exemplo de sistema que possui diversos pontos singulares de
falha, os chamados órgãos vitais: enquanto o corpo ainda permanece vivo sem
membros como braços e pernas, ele falhará se perder um coração, pulmão ou
cérebro.

**Resiliência** é a propridade de um sistema de minimizar pontos de falha
e resistir, ou seja, permanecer funcionando e se restaurar após falhas.

## Tipos de ameaças

Existem várias formas de classificar as falhas de segurança. Adotaremos a
seguinte:

1. Negação de Serviço é a mais simples de entender. Consiste em bloquear o
   funcionamento de um sistema de modo que, na prática, ele não consiga operar
   normalmente.

   A negação de serviço por si só não toma o controle de todo o computador
   nem obtém dados sensíveis. Apenas deixa um sistema temporariamente inutilizado.

   Uma forma simples de negação de serviços é um programa que trave ou
   que pare de rodar por conta de algum problema. Programas que travam
   sozinhos são bem suspeitos como portas de entrada para esse tipo de ataque.

   A negação de servido é uma falha na segurança da informação pois ataca
   diretamente a propriedade da **disponibilidade**.

2. Execução arbitrária de código: nesta falha, um atacante consegue realizar
   operações no sistema para as quais ele não deveria ter autorização.

   Ele pode, por exemplo, invadir um computador e fazer com que ele execute
   programas diversos.

3. Escalada de Privilégios é uma falha onde um atacante ganha privilégios
   ou acesso a partes não autorizadas de um sistema.

Essas falhas podem ser exploradas simultaneamente!

### Natureza das falhas

Qual é a natureza das ameaças à nosa segurança? Podemos pensar em:

1. Ameaças que existem naturalmente, isto é, que são consequências da
   natureza de operação de dispositivos, procedimentos, etc.

2. Ameaças que existem por conta da estupidez, ignorância ou falta
   de atenção de quem desenvolveu os dispositivos, procedimentos, etc.

3. Ameaças que foram colocadas intencionalmente, ou seja, por malícia
   de quem desenvolveu os dispositivos, procedimentos, etc, visando
   manter um canal aberto para possíveis invasões. Isso pode ser
   feito na surdina e até em colaboração com agências de vigilância
   de governos.

Como saber de que tipo de vulnerabilidade estamos falando? Essa distinção
faz alguma diferença?

Pelo critério da Navalha de Occam, podemos dizer que não há diferença
entre ameaças que foram colocadas por estupidez ou malícia, pois os danos
que elas podem causar são os mesmos.

Essa versão específica de navalha é chamada de Navalha de Hanlon. O que é
explicado por estupidez não precisa ser explicado em termos de malícia.

Podemos ainda dizer que o que é explicado pela natureza dos sistemas
não precisa ser explicado nem pela estupidez, nem pela malícia.

De modo que nossa prioridade deve ser nos proteger de qualquer ameaça e só
então tratar de entender a intencionalidade das vulnerabilidades: de onde elas
vieram e porquê ainda estão lá. Este é um princípio importante para que não
nos desesperemos ante a qualquer possibilidade de ameaça.

Afinal, viver é perigoso.

## Qual a diferença entre segurança e privacidade?

Neste curso vamos considerar uma definição específica de privacidade.

Enquanto segurança é a preparação antecipada a ameaças diversas que podem
nos causar danos, a privacidade é toda e qualquer informação que queremos proteger.
É qualquer informação que não queremos que seja tornada pública ou que queiramos
manter disponível apenas a um círculo restrito de pessoas.

### Privacidade é algo coletivo

Ao contrário do que se imagina, a privacidade não se restringe apenas a uma pessoa.
Isso valeria apenas num mundo de pessoas isoladas ou com informações que não são
compartilhadas com ninguém.

Pelo fato de nos comunicarmos, -- consequência da vida em sociedade --
informações sobre nós e sobre outras pessoas trafegam de um lado para outro e de
pessoa em pessoa, o que implica que a privacidade é, necessariamente, uma propriedade coletiva.
Qualquer pessoa, mesmo que seja parte do seu círculo privado de comunicação,
pode entregar a sua privacidade ao divulgar uma informação sua que ela possui.

### Privacidade é algo político

No mundo contemporâneo, a existência da privacidade é uma consequência de uma
vida em sociedade na qual dentro da própria sociedade existem ameaças a seus
integrantes e principalmente a conjuntos inteiros de integrantes (classes,
gêneros, raças, etc). A privacidade é, então, usada para que permita a articulação
entre um mesmo grupo social para que possa se preparar para uma disputa aberta.

A privacidade, portanto, é algo inerentemente político, entendendo política
como uma disputa entre antagonistas. De modo que podemos chamar de **sigilo**
a defesa das informações privadas de um grupo.

Fica fácil assim pensar no sigilo como importante inclusive nas guerras.

## Segurança por obscuridade

É muito, muito comum que se confunda segurança com privacidade. São coisas
distintas.

Quando alguém tenta usar a privacidade como medida de segurança, estará adotando
o que chamamos de **segurança por obscuridade**, o que de fato **não é segurança**.

Para nos guiar, vamos utilizar o **Princípio de Kerckhoffs**, que em resumo diz
o seguinte:

    Assuma que o inimigo conhece o sistema.

Em versão menos resumida, podemos dizer que, por tal princípio, devemos assumir
por simplicidade, mesmo que não seja o caso, que o atacante de um sistema conhece
todos os detalhes do funcionamento desse sistema.

O que não quer dizer que ele saiba ou tenha invadido o sistema, ou mesmo que possui
as informações que estejam protegidas dentro do sistema.

Ou seja, a segurança de um dado sistema não pode ser baseada na suposição de que
o oponente não conhece o funcionamento do sistema. Além de ser uma suposição que
pode ser falsa, não podemos assumir que por mero desconhecimento um oponente não
seja capaz de descobrir uma brecha no sistema.

Por exemplo, você pode deixar a porta da sua casa fechada mas destrancada se quiser,
mas não pode assumir que ninguém vai tentar abri-la, independentemente de alguém
saber ou não que a porta está destrancada.

Se ninguém estiver de olho na sua casa, o fato da porta estar destrancada pode
ser considerado como um risco baixo, mas se você for um alvo específico de
alguém então o risco é mais alto. Mas você nunca sabe efetivamente o tamanho
desse risco e não deve contar com a suposição de que ninguém sabe que você não
tranca a porta. Considerando que é muito rápido e fácil você sempre trancar a
porta, o melhor a fazer é trancá-la e assumir que todo mundo sabe como uma
porta funciona, como se todo mundo fosse um bom chaveiro que soubesse inclusive
arrombar a porta.

Assim você passará a se preocupar com a resistência da sua porta e com a
qualidade da sua fechadura e no tempo que demora para que alguém consiga
arrombá-la.

Em segurança, não podemos assumir a obscuridade como proteção. Podemos até
tornar nossos procedimentos secretos, mas não podemos assumir que pelo fato de
serem secretos eles são desconhecidos.

## Segurança da informação

Para concluir este capítulo e considerando que este não é um guia de segurança
geral, mas de segurança em comunicação digital, enunciaremos os princípios
básicos da segurança da informação.

Grosso modo, podemos dividir a segurança da informação em algumas propriedades.
As consideradas mais importantes são as seguintes:

1. Confidencialidade: é a garantia de que comunicação apenas poderá
   ser interpretada pelas partes envolvidas, isto é, mesmo havendo interceptação
   por terceiros o conteúdo da comunicação estará protegido.

   Isso significa que, numa comunicação entre você e outra pessoa, haverá
   confidencialidade se apenas vocês tiverem acesso ao conteúdo da comunicação.

2. Integridade: é a garantia de que o conteúdo da comunicação não foi
   adulterado por terceiros.

   Ou seja, na comunicação entre você e outra pessoa, vocês conseguem identificar
   se alguém alterou o conteúdo das mensagens.

3. Disponibilidade: é garantia de que o sistema de comunicação estará acessível
   sempre que necessário. Este é um requisito de segurança porque a falta de
   comunicação pode ser muito prejudicial.

4. Autenticidade: garante que cada uma das partes possa verificar se está de
   fato se comunicando com quem pensa estar se comunicando, isto é, a garantia
   de que não há um impostor do outro lado da comunicação.

Opcionalmente também podemos falar de:

5. Não-repúdio: garantir que as partes envolvidas na comunicação não possam
   negar ter participado da comunicação. Esta propriedade é desejada em
   sistemas nos quais haja um controle sobre quem realizou determinados tipos
   de operações.

   O oposto do não-repúdio é a negação plausível, no caso onde não é possível
   determinar com certeza se determinada pessoa participou da comunicação.

   Alguns sistemas foram criados para possuir a propriedade do não-repúdio,
   enquanto outros são baseados na negação plausível.

6. Anonimato: é garantia de que as partes envolvidas na comunicação não possam
   ser identificadas.

7. Auditabilidade: o sistema de comunicação tem seu funcionamento conhecido
   e pode ser auditável por qualquer pessoa que tenha acesso e conhecimento.

Nem sempre os sistemas satisfazem todas essas propriedades, seja intencionalmente
ou não. É importante observar o que cada sistema oferece em termos dessas propriedades.

Em muitas situações, é possível combinar diversos sistemas que ofereçam propriedades
distintas de segurança da informação para obter o máximo de propriedades possíveis.

## Resumo

Hora do resumo dos conceitos apresentados:

1. Segurança se dá por níveis/camadas: o comprometimento da segurança numa
   camada afeta a segurança de todas as camadas superiores e/ou dependentes.

2. Em segurança, damos preferência a procedimentos e ferramentas que obedeçam o
   princípio da simplicidade, isto é, da complexidade necessária.

3. A interdependência entre sistemas e procedimentos pode acarretar em pontos
   de falha. Contra isso, utilizamos compartimentalização.

4. A privacidade é toda e qualquer informação que queremos proteger. Ela é algo
   coletivo e político.

5. Segurança por obscuridade, de fato, não é segurança. Assuma que o inimigo
   conhece todas as suas defesas.

6. Os princípios mais importantes da segurança da informação são: confidencialidade,
   integridade, disponibilidade e autenticidade.

## Atividades

1. Caso você já tenha feito uma lista dos procedimentos de segurança que você utiliza
   ou gostaria de utilizar, você conseguiria organizá-los numa hierarquia de dependências,
   isto é, qual depende de qual?
