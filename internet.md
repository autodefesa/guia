# A Internet e a Web

## Funcionamento

Os meios de comunicação digital estão convergindo rapidamente para a internet.
Assim, o entendimento da segurança da informação na internet pode ser
aproveitado para muitos casos de uso.

Nesta aula falaremos sobre o funcionamento básico da internet, que pode ser
aplicado tanto ao contexto do acesso via computadores quanto smartphones.

A Internet é uma **rede de dados digital** baseada em diversos **protocolos**.

Por **rede** entendemos computadores que podem trocar mensagens entre si.

Já a **Web** pode ser definida como a parte da internet que interage com os
usuários.

Apesar desta aula ser mais focada no navegador, a maior parte do que é dito
aqui vale para outros aplicativos, tanto em computadores quanto em smartphones.

O uso seguro da web depende muitos fatores, por exemplo:

1. Problemas de segurança nos dispositivos de acesso. No caso deste curso,
   estamos falando de computadores e smartphones.

2. Fatores clássicos da segurança da informação: autenticidade, disponibilidade
   integridade e confidencialidade de cada interação realizada na internet.

4. Identificação e monitoramento de usuários.

5. Privacidade e uso de dados pessoais.

## Ataques

Vamos nos concentrar nos principais ataques realizados na web:

1. Malware. É importante assumir que qualquer informação que recebemos
   da web pode conter algum código malicioso.

   Esta é a forma mais acessível para invadir o sistema de alguém, pois não
   depende de ter acesso privilegiado a nenhum sistema. Qualquer pessoa pode
   fazer, não apenas empresas e governos mal intencionados.

   Assim, tome cuidado ao abrir anexos e sites pouco confiáveis. Na dúvida,
   use alguma defesa apresentada na aula sobre computadores, como por exemplo
   abrir anexos usando um sistema virtualizado.

2. Vulnerabilidades no navegador também são portas de entrada possíveis para
   malware. A exploração de vulnerabilidades torna possível a infecção sem
   qualquer participação do usuário.

3. Sítios falsos: será que você está acessando o site correto ou é um clone
   tentando te convencer a fornecer informações pessoais ou te fornecendo
   informações falsas?

4. As redes sociais são também fontes de ataques à sua segurança.

   O modelo de negócios da maioria dessas plataformas é baseado na oferta
   do serviço em troca das sua interação. Todas as informações que você
   fornece às redes sociais podem ser revendidas ou usadas para qualquer
   fim estratégico, como propagandas, experimentos psicológicos ou
   controle social.

5. Dados pessoais e vazamentos. Pense que a publicação de um conteúdo é um
   caminho sem volta: podem até esquecer do que você disse, mas você não tem
   garantias que um conteúdo sumirá com o tempo e nem que você conseguirá
   apagar todas as cópias existentes.

   Considere que toda a mensagem, áudio, foto ou vídeo que você enviar
   para qualquer pessoa pode ser vazada. Esse vazamento pode ser restrito
   a um círculo de pessoas ou pode ser irrestrito.

   O vazamento pode ser involuntário ou não: pode ter a participação
   de quem recebeu sua mensagem ou pode ser feito por alguém que invadiu
   o sistema da pessoa.

   Pode ser alguém que invadiu um sistema onde estão essas informações.

   O vazamento nem sempre é focado em você: pode ser que toda uma plataforma de
   conteúdo tenha os dados vazados de todos os seus usuários e sua informação
   estar no meio.

### Quais são as informações coletadas por cada serviço?

Virtualmente, qualquer coisa que enviamos para eles, sabendo ou não.

É fácil nos conscientizarmos de quais informações enviamos voluntariamente,
porém é mais difícil perceber informações adicionais, ou metadados, que
são enviados automaticamente pelos softwares que interagem com esses
serviços.

Navegadores web enviam, por padrão, uma série de informações que
podem ser usadas para nos identificar. Por exemplo:

* Informações específicas do navegador e o do sistema operacional,
  como versões, plugins suportados e assim por diante.

* Qual foi a página anterior visitada antes da página atual.

* Informações persistentes de interação com sites conhecidas como **cookies**,
  que podem ser criadas por qualquer site e ficam armazenadas no seu computador.

### Quais são as informações que nos identificam e rastreiam nossos hábitos?

Rastreadores embarcados nos sites, como coletores de estatísticas e botões do
tipo "curtir" conseguem estimar se estamos autenticados na respectiva rede
social, qual o nosso login, etc.

Uma única página da web pode vir embutida com rastreadores de diversos
serviços.

Este é o grande resumo que vale para redes sociais, mensageria, dados de
formulário, informações de buscas, etc: Basicamente **TUDO** o que você envia
na internet pode ser guardado indefinidamente, integrado a bancos de dados ou
vazado.

E bastam poucas dessas informações para que seja possível nos identificar
unicamente.

## Defesas

Como podemos nos defender de uma situação em que qualquer interação pode ser
registrada e utilizada indefinidamente? Existe uma saída para a segurança da
informação ou este é o fim da privacidade?

Estas são boas perguntas. A história dirá. Por hora, temos algumas medidas
de segurança possíveis para melhorar um pouco nossa situação.

1. Garantir a segurança da informação básica: a comunicação criptografada
   usando **HTTPS** é a forma básica de se transferir informações na web.

   O uso do HTTPS nos dá mais garantias de que estamos acessando o sítio
   legítimo e não uma versão falsa. Também garante que a comunicação não
   poderá ser interpretada ou adulterada por interceptadores.

   O uso do HTTPS depende da oferta deste pelo site ou serviço que você
   queira acessar. Um sítio que use HTTPS terá o seu endereço no navegador
   começando por **https://**, como por exemplo **https://wikipedia.org**.

   O fato do HTTPS estar disponível num site não implica necessariamente que a
   conexão é segura. O HTTPS possui vários problemas, como a dependência da
   certificação criptográfica feita por terceiros que podem ser invadidos ou
   serem compelidos a emitir certificações falsas.

   Ainda, o HTTPS pode estar mal implementado nos sites.

   Se você quiser avaliar a qualidade de uma conexão HTTPS, você
   pode testá-la usando um aplicativo específico ou então um serviço
   como o SSL Labs.

   Se ele for bem implementado, pode fornecer propriedades adicionais
   como sigilo futuro e usar algoritmos criptográficos bem fortes.

2. Usar logins e serviços somente quando necessário: você precisa estar
   autenticado(a) o tempo todo nas redes sociais? Quanto menos você usá-las,
   menos irão te rastrear.

3. Limitar o que o seu navegador pode fazer.

   Você pode limpar os arquivos locais armazenados pelo seu navegador,
   como cookies e histórico de navegação.

   Isso impede que eles sejam reutilizados, o que em si já é uma medida que
   dificulta a identificação de usuário.

   Isso também impede que esse tipo de informação seja obtida caso seu
   dispositivo seja invadido.

   Você também pode configurar seu navegador para nunca guardar esse tipo
   de informações ou para apagá-las toda vez que você for desligar o
   navegador.

   Uma medida adicional pode ser limitar a capacidade do seu navegador
   de processar sites, como por exemplo desligar o processamento do Javascript.
   Isso pode quebrar o funcionamento de alguns sites mas, por outro lado,
   pode impedir que sites maliciosos possem executar qualquer tipo de
   instrução no navegador.

4. Você pode usar serviços que possuam uma melhor política de privacidade.
   Por exemplo, você pode usar um mecanismo de busca alternativo como
   o **Duckduckgo**, que respeita muito mais a sua privacidade do que
   outros mais conhecidos.

5. Você pode utilizar um navegador especial feito para proteger a
   sua privacidade.

   Recomendamos utilizar o [Tor Browser
  ](https://www.torproject.org/pt-BR/download/) no seu computador e no seu
   smartphone Android, ou o [Onionbrowser](https://onionbrowser.com/) no
   iOS.

   Ambos os softwares utilizam a rede **Tor**, que é uma plataforma de
   navegação mais anônima. Ela utiliza criptografia e uma grande rede de
   computadores distribuídos pela internet que dificulta muito a localização
   dos usuários que estão navegando.

   O Tor Browser é um navegador que usa a rede **Tor** em todas as suas
   conexões. Isso significa que ao navegar usando o Tor Browser você já
   estará, por padrão, dificultando sua localização na internet.

   Mas **ATENÇÃO**: certifique-se de sempre usar conexão HTTPS ao acessar
   qualquer site usando o Tor Browser, do contrário você estará muito
   mais suscetível a ataques de interceptação e de site falso.

   O Tor Browser também possui uma série de modificações de segurança
   para que a sua navegação fique mais segura.

   Já o [Orbot](https://guardianproject.info/apps/org.torproject.android/)
   é uma aplicação que permite que outros aplicativos no smartphone utilizem
   a rede Tor.

## Resumo

Navegar na internet pode parecer algo inofensivo, só que muitas
vulnerabilidades em navegadores estão sendo exploradas para inúmeros
fins, dentre eles a obtenção de dados do usuário disponíveis pelo
navegador. Fora isso, sítios maliciosos podem levar o internauta a
revelar voluntariamente seus dados. Contra essas e outras
vulnerabilidades, tome as seguintes providências:

* Limpe sempre seu histórico de navegação, os arquivos temporários (cache) e os
  cookies.
* Não salve no navegador suas senhas de formulários.
* Saiba suspeitar quando um sítio é falso, ou seja, quando ele aparenta ser o
  sítio de uma empresa ou organização (em geral bancos) mas na verdade é apenas
  pretende obter suas senhas ou dados pessoais.
* Use, sempre que disponível, conexão segura (https ao invés de http).
* Utilize alguma ferramenta de navegação anônima, como o
  [Tor](https://www.torproject.org/pt-BR/).

## Referências

* [PrivacyTests.org](https://privacytests.org/): comparativo entre navegadores web no quesito privacidade.
* [Panopticlick](https://panopticlick.eff.org/).
* [Browserprint](https://browserprint.info/#fingerprint).
* [BrowserLeaks](https://browserleaks.com/).
* [Orbot para dispositivos Android | security in-a-box](https://securityinabox.org/pt/Orbot_main).
* [Orfox: A Tor Browser for Android – Guardian Project](https://guardianproject.info/apps/orfox/).
