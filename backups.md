# Backups Criptografados

## Funcionamento

Backups são importantes para não perdermos nossas informações no caso
de roubos, falhas de dispositivos ou erros humanos no apagamento de dados.

Backups fazem parte da segurança da informação porque eles estão associados
à propriedade da disponibilidade.

O princípio básico de um backup é a cópia de informações de um lugar para outro
para que os dados não sejam perdidos no caso de perda do local original, seja
um computador ou smartphone.

Para que esses backups tenham alguma segurança, é importante que eles
estejam criptografados. Afinal, do que adianta protegermos nossos dados
com criptografia nos nossos dispositivos se nossos backups não estiverem
também criptografados?

O funcionamento da criptografia para backups é análogo ao que tratamos no
capítulo sobre computadores. Até o mesmo software pode ser utilizado, com a
diferença que ele estará realizando a criptografia num volume de armazenamento
externo ao computador.

Por isso, prosseguiremos adiante já tratando de duas estratégias de backup:

1. Utilizando um dispositivo de armazenamento externo. Pode ser um pendrive
   USB se você quer fazer backups de poucos arquivos. Ou pode ser um disco
   rídigo de capacidade suficiente para armazenar todos os seus arquivos.

   Sistemas operacionais livres como o Debian já suportam criptografia
   em discos externos, usando por exemplo o padrão LUKS.

2. Usando algum serviço de hospedagem na internet, o que tipicamente é chamado
   de **nuvem**. Uma "nuvem" nada mais é, nesse caso, do computador de outra
   pessoa.

Note que se você utilizar um local externo para armazenar todos os seus dados e
não deixar nada armazenado no seu computador você não estará de fato fazendo
backup, mas sim usando o armazenamento externo como armazenador principal das
suas coisas.

Para que o local externo seja um backup, ele deve ser uma **cópia** dos
seus arquivos. E uma cópia que você não mexe a não ser que for substituí-la por
outra cópia, se for o caso, ou para acessar seus arquivos. Fora isso você não
escreve nela.

## Ataques

Tratemos de alguns ataques ao backup:

1. Problemas do backup em disco externo:
   * O disco pode ser roubado juntamente com o seu computador!
   * O disco pode e vai falhar em algum momento e aí você
     precisará comprar um outro disco para substituí-lo.

2. Problemas do backup na nuvem:
   * Não permita que a criptografia dos arquivos seja feita na nuvem.
     Ela deve ser feita nos seus dispositivos e só então os arquivos podem
     ser transferidos para o computador remoto. Caso contrário, tanto o
     serviço de armazenamento quanto algum invasor podem ter acesso aos
     seus dados antes deles serem criptografados.
   * Leve em conta que a "nuvem" não é uma entidade mágica. Ela pode
     estar sujeita à falhas ou o serviço pode ser interrompido. Quando
     você armazena seus backups num sistema que você não tem o controle,
     você está delegando a terceiros a guarda do seu backup, o que envolve
     uma perda de autonomia.

## Defesas

As defesas básicas para um bom backup externo são estas:

1. No caso de backups em discos externos, use o método do "backup offsite",
   isto é, um backup do seus dados que fique longe do seu computador, de modo
   que o roubo de um deles não acarrete na perda de todos os seus dados.

2. No caso dos backups na nuvem, vale o que foi dito anteriormente:
   apenas transfira dados que já estejam criptografados. O processo de
   criptografia deve ocorrer, sempre que possível, nos dispositivos que
   você controla.

   É claro que na vida você pode acabar tendo que usar a nuvem para
   armazenar e editar documentos sem criptografia, mas pelo menos agora
   você saberá quais os riscos que você estará correndo para que possa
   dosar esse uso.

Pense em como proceder no caso do seu computador ser roubado. Como
você acessaria os dados do backup? Você lembraria da senha? A senha
seria a mesma senha do armazenamento criptografado do seu computador?

Lembre-se que você não está limitado ou limitada a utilizar um único método.

Você pode ter um ou mais backups criptografados em discos externos, backups
criptografados na nuvem e até andar com um pendrive USB com um backup
criptografado dos seus dados mais importantes.

Planeje-se também para realizar backups e testes periódicos dos backups.

Apenas fazer backups de tempos em tempos não é suficiente: é preciso checar
se os backups estão realmente sendo realizados e se todos os dados estão
sendo copiados corretamente.

## Resumo

* Backups são fundamentais. Na dúvida, faça sempre backups criptografados.

## Atividades

1. Faça backups criptografados dos seus dados de acordo com as recomendações
   desta aula :)

2. Planeje uma rotina periódica de realização e testes de backups. Qual
   a frequência ideal para você?

## Referências

* [VeraCrypt](https://veracrypt.codeplex.com).
