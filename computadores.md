# Computadores

## Funcionamento

O computador se transformou no elemento básico da comunicação digital.

Precisamos conhecer o funcionamento de um computador em linhas gerais para
que possamos entender a maioria das vulnerabilidades dos sistemas digitais.

Computadores são sistemas bem complexos. Aqui usaremos um modelo bem simplificado
mas que seja suficiente para mostrar como um computador pode ser invadido.

Na história da tecnologia, sabemos que para cada função que uma ferramenta ou
instrumento deveria cumprir existem formas específicas de montagem, escolha de
materiais, etc.

É o que acontece com máquinas. Por exemplo, um motor tem um arranjo e um conjunto
específico de peças, é feito de determinados materiais, etc. Existem diversos
tipos, ou linhagens de motores, mas todos possuem algumas características
essenciais que os diferenciam, por exemplo, de um barco a remo.

O mesmo acontecia nos primórdios da eletrônica e da comunicação digital: era
construído um aparelho específico para cada aplicação. Um tipo de equipamento
cumpria a função de radiocomunicador, outro de sistema de alarme, um outro
usado como sensor de temperatura e assim por diante.

O computador é diferente de todas essas máquinas específicas porque ele é uma
máquina genérica: ele possui uma complexidade suficiente que permite que ele
simule o comportamento de qualquer outra máquina simulável.

Isso não significa que qualquer computador pode ser usado como guindaste, mas
que qualquer computador pode simular o comportamento de um guindaste.

Hã? Qual a diferença entre simular e realizar?

Imagine um sinal de trânsito composto pela cor verde, indicando a veículos
e pedestres para que sigam adiante; amarelo, indicando atenção pois em breve
a indicação irá mudar; e vermelho, indicando que veículos e pedestres
aguardem até a cor verde para prosseguir.

Sabemos que um sinal de trânsito vai do verde para o amarelo e em seguida para
o vermelho, voltando então para o verde. E sabemos que ele permanece em cada
uma dessas situações por um intervalo de tempo. Dizemos que cada uma dessas
situações são **estados** da máquina conhecida como **sinal de trânsito**.

Simular, no nosso caso, pode ser entendido como marcar mentalmente qual
a cor atual do sinal, manter a contagem do tempo, e mudar a cor ou estado
do sinal quando o tempo passar. Se além disso usarmos placas coloridas
e formos para a rua exibi-las de acordo com o estado atual da nossa
contagem mental, estaremos não apenas simulando como nos comportando como
um sinal de trânsito.

Assim, para que um computador seja efetivamente um guindaste, basta que
liguemos a ele o braço mecânico de um guindaste e a simulação passa a
efetivamente controlar uma aplicação real.

O computador pode assumir o comportamento de outras máquinas. Inclusive, um
computador pode simular seu próprio funcionamento, isto é, um computador pode
simular um computador! Incrível, né?

É nesse sentido que dizemos que o computador é uma máquina genérica, também
chamada de máquina abstrata. O computador processa informações e esse
processamento pode ser ligado a mecanismos que realizem atividades de acordo
com essas informações?

Mas como isso pode ser feito? O que o computador tem que o faz ser uma máquina
genérica, diferentemente das máquinas específicas?

O segredo está no fato de que o computador é um dispositivo que aceita um conjunto
grande de instruções de operação de natureza lógica, matemática. Essas operações
são, por exemplo, de somar ou subtrair dois números ou mesmo de buscar por mais
instruções.

As instruções são seguidas por uma parte do computador, chamada de processador,
ou Unidade de Processamento Central (CPU).

As instruções ficam guardadas em dispositivos de armazenamento de longo prazo
(também chamados de disco ou disco rígido) mesmo quando o computador estiver
desligado.

Durante a operação do computador, essas instruções permanecem também numa
memória de curto prazo, conhecida como Memória de Acesso Aleatório (RAM).

Para que o computador possa interagir com o mundo externo, e principalmente com
humanos, ele é provido de elementos de entrada e saída de dados, como, por exemplo,
teclado, mouse, tela de vídeo, caixas de som, interfaces de rede, etc.

E, para que essas partes possam ser interligadas, existem canais de comunicação
conhecidos como barramentos de dados.

Assim, um computador pode ser pensado simplificadamente como um agregado dos
seguintes elementos:

* Processador (CPU): executa instruções genéricas para manipulação e transferência
  de dados da e para a memória, para dispositivos de armazenamento ou para interfaces
  de entrada e saída.

* Memória de Curto Prazo (RAM): é uma memória usada para armazenar instruções e
  dados que serão manipulados pelo processador, enviados ou recebidos pelo
  armazenamento de longo prazo ou para as interfaces de entrada e saída.

  As informações guardadas na RAM desaparecem naturalmente e aos poucos depois
  que um computador é desligado.

* Armazenamento ou Memória de Longo Prazo (Disco): é o dispositivo que guarda
  as informações para acesso posterior. Essas informações não são apagadas
  naturalmente depois que um computador é desligado, estando disponíveis
  quando o computador é religado.

* Entrada e Saída (Vídeo, teclado, mouse, som, rede): são os dispositivos
  que nos colocam em contato com o computador e permitem a interação dele
  com o mundo à sua volta.

* Barramentos: conectam os elementos do computador uns aos outros. Tipicamente
  o barramento conecta o processador e a memória com os outros dispositivos.

  O armazenamento de longo prazo e os dispositivos de entrada e saída podem
  acessar informações que estejam na memória de curto prazo usando o processador
  como intermediário ou então acessar diretamente a memória, o que veremos
  que pode ser uma forma de ataque ao sistema.

        --------------------------------------------
       /                                            \
    memória ------------- armazenamento         entrada e saída
       \                       |                     /
        \                      |                    /
         \---------------- processador ------------/

Damos o nome de **hardware** a esses dispositivos físicos. Hardware significa
ferramenta dura, uma referência à resistência natural que ferramentas físicas
oferecem para serem modificadas, simplesmente porque são feitas de matéria.

Damos o nome de **software** aos conjuntos de instruções -- ou código -- que
podem ser interpretadas pelo computador. Software significa ferramenta
maleável, isto é, à facilidade de mudança da função do computador apenas com a
mudança das instruções dadas a ele. Mudando o software, mudamos o funcionamento
do computador.

Software e hardware. Instruções maleáveis que fazem com que um equipamento
duro opere de inúmeras maneiras, cumprindo determinados **programas** de operação.

Mas como o computador opera?

A operação rotineira de um computador se dá do seguinte modo:

* Ao ser ligado, o computador busca num dos seus dispositivos de armazenamento
  de longo prazo chamado de BIOS algumas instruções básicas para que ele
  consiga reconhecer os outros dispositivos instalados, como a memória,
  as interfaces de entrada e saída e outros dispositivos de armazenamento
  de longo prazo.

  Essas instruções compõem um tipo de software conhecido como **firmware**,
  que são os programas feitos para que os dispositivos de hardware possam
  ser controlados. Ou seja, o software da BIOS é responsável pelo funcionamento
  básico do computador.

* Depois que o computador é ligado e executa, isto é, processa as instruções
  da BIOS, ele procura por outros softwares nos outros dispositivos de armazenamento
  de longo prazo.

* A maioria dos computadores que usamos não possuem um único software instalado
  no seu armazenamento de longo prazo, mas sim diversos softwares. No entanto,
  para que um computador possa executar vários softwares simultaneamente, é
  utilizado um software gerenciador conhecido como Sistema Operacional.

* Se o computador encontra um sistema operacional armazenado em disco, isto
  é, num armazenamento de longo prazo, ele começa o processamento desse
  sistema operacional e em seguida o computador estará disponível para
  executar outros softwares, chamados de **aplicações**.

### Resumindo

O computador é uma máquina genérica que pode realizar qualquer tarefa que pode
ser explicada numa **receita** chamada de **algoritmo**. Como ela é uma máquina
genérica, ela pode fazer tudo quanto é coisa.

Em outras palavras, não existe a priori uma atividade intencional no
computador, já que as pessoas que o projetaram não estavam pensando numa
aplicação específica.

Toda a intenção de uso do computador depende do seu dono, porém é muito difícil
fazer com que uma máquina genérica desempenhe apenas um conjunto mais restrito
de operações que sejam consideradas seguras. Assim, é quase que um milagre que
seja possível ter o mínimo de segurança com um computador!

Nosso modelo de computador é muito simplificado: além de cada elemento dele ser
muito complexo, hoje em dia a maioria dos computadores é na verdade uma rede de
computadores menores. Cada um deles rodando seu próprio software.

## Ataques

Por que tratamos de tantos detalhes de funcionamento de um computador?

A resposta é direta: porque muitos ataques são baseados exatamente no funcionamento
de todo ou de parte do computador.

Além do mais, o computador se encontra num dos níveis mais básicos de proteção
da informação. Considerando que a segurança se dá em níveis e que o comprometimento
de um nível compromete todos os níveis superiores, a perda da segurança no computador
acarreta na perda da segurança em todas as aplicações que rodam nele.

A seguir detalharemos os tipos de ataque mais comuns a computadores.

### Acesso físico

O primeiro tipo de ataque possível é o de acesso físico.

Quando alguém tem acesso físico direto, tempo e conhecimento pode usar o fato
de estar em contato com o computador para instalar softwares ou hardwares
espiões e também analisar os dados que estiverem armazenados.

O acesso físico não autorizado pode ou não ser detectado. A ausência de sinais
não implica que um computador não tenha sofrido uma invasão de acesso físico.

Aqui estamos pensando no atacante como sendo um/a analista forense que obteve
o seu computador ligado ou desligado e que quer obter as informações nele contidas
ou instalar software e hardware malicioso e devolvê-lo a você para que essas
informações possam ser extraídas posteriormente.

Vamos listar alguns destes ataques físicos:

1. Falta de criptografia nos dados que estão dentro do computador: a criptografia
   no armazenamento de longo prazo é uma medida que protege os dados no caso de
   uma captura ou invasão física do dispositivo.

   Ou seja, se o seu computador for roubado e o armazenamento estiver criptografado,
   seus dados podem estar a salvo! Isso só funcionará bem se o computador estiver
   desligado quando for roubado, como veremos mais à frente.

   Se você não utiliza criptografia no armazenamento. Bom, infelizmente os dados
   armazenados estarão disponíveis a qualquer pessoa que obter acesso físico
   ao seu computador, independente dele estar ligado ou desligado.

2. Keyloggers e data loggers:

   Dispositivos espiões básicos incluem os registradores de digitação, chamados
   de keyloggers, que gravam e eventualmente enviam os dados digitados para
   algum local remoto.

   Tais keyloggers podem inclusive registrar a senha usada para destravar
   o armazenamento criptografado do computador.

   Com acesso físico, também é possível a instalação de extratores gerais
   de dados e não apenas da digitação do teclado, mas também do conteúdo
   da memória RAM ou de qualquer outro ponto de medição do computador.

   Tanto a versão software quanto hardware dos loggers podem ser de difícil
   detecção. No caso dos modelos em hardware mais grosseiros, uma inspeção
   cuidadosa no hardware pode detectá-los.

3. DMA, ou Acesso Direto à Memória é um esquema que permite dispositivos
   do computador acessarem diretamente a memória de curto prazo, sem que
   o conteúdo da memória tenha que passar pelo processador.

   DMA é um método desenvolvido para acelerar grandes transferências de
   dados entre dispositivos e a memória, liberando o processador para
   realizar outras atividades.

   A vulnerabilidade aqui é explícita: sem controles apropriados, dispositivos
   poderiam não apenas ler todo o conteúdo da memória, incluindo senhas usadas
   para criptografia de armazenamento, mas também para adulterar o conteúdo
   da memória.

   Alguns dispositivos que utilizam a conexão Firewire tem capacidade de
   leitura direta da RAM e podem ser facilmente espetados num computador.

4. Canais laterais.

   Alguns ataques parecem mais esotéricos, porém já foram comprovados em
   experiências de laboratório e podem se tornar bem viáveis a médio prazo.

   Eles são chamados de **ataques de canal lateral** pois obtém informações
   fora das vias principais. Em geral é mais difícil de se defender contra
   eles pois precisam de intervenções mais pesadas no hardware.

   Não se assuste! Hoje os ataques a canais laterais ainda são muito raros
   e foram incluídos no curso mais para você ter consciência de que fenômenos
   físicos estão muito envolvidos na computação.

   Eles incluem:

   * Tempestade eletromagnética, ou simplesmente TEMPEST: equipamentos
     elétricos são como antenas: emitem radiações eletromagnéticas. Com a
     aparelhagem apropriada e uma boa antena, é possível medir essas emissões
     e delas extrair informações sensíveis.

     Em alguns casos, já se conseguiu até extrair chaves criptográficas!

     E isso pode ser feito a curtas distâncias, da ordem de metros. Atacantes
     podem se posicionar na sala ao lado e obter informações com bastante conforto
     e sem o constrangimento de serem pegos com a mão na massa!

   * Ruídos sonoros: de forma similar ao TEMPEST, equipamentos elétricos também
     emitem ruídos sonoros, muitos deles inaudíveis pelo ser humano porém captáveis
     com microfones especiais. Tais ruídos também podem extrair informações de
     do computador a curta distância.

     Ruidos de digitação do teclado também podem revelar informações sobre a
     digitação, baseados em pequenas diferencas de tempos levados na digitação
     sucessiva de teclas: teclas mais distantes demoram diferencialmente mais tempo
     para serem alcançadas pelos dedos. Tais diferenças poderiam ser utilizadas
     para reconstruir o que é digitado.

     Em alguns casos seria até possível diferenciar pequenas variações do ruído
     da própria tecla!

   * Interferência em cabeamento é outro tipo de vazamento indireto de informações
     do computador. Em tese pode ser detectado no cabeamento elétrico, de rede ou
     mesmo de som.

   * Microsismografia: o sensor de movimento de um telefone móvel pode ser posicionado
     na mesma mesa onde se encontra o teclado de um computador para detectar pequenas
     variações de deslocamento da mesa e com isso tentar determinar quais teclas
     estão sendo digitadas.

### Falhas de hardware

Classificaremos aqui como ataques a hardware aqueles que são baseados em
dispositivos físicos mas que não precisam necessariamente ser explorados
presencialmente por um atacante. Ou seja, estes são ataques ao hardware que
também podem ser feitos remotamente e via software.

Essas falhas podem ser usadas como **portas dos fundos**, também chamadas de
**backdoors**, ou seja, portas de entrada alternativas ao computador que estão
disponíveis a quem souber atacar.

Falaremos aqui um pouco sobre as principais falhas em determinados dispositivos:

* Processador: começaremos pelo pior caso, que são os problemas em processadores.
  Algumas desses backdoors existem e são documentados, como é o caso dos
  gerenciadores remotos, que são subprocessadores contidos dentro dos próprios
  processadores usados para dar acesso remoto a inúmeras funções de hardware,
  como reiniciar a máquina, alterar configurações, etc.

  Esse tipo de ferramente é conhecido como **out-of-band management**, que
  podemos chamar de gerenciamento remoto indireto e que pode ser acionado
  mesmo que o computador esteja desligado ou em estado de espera.

  Às vezes também são chamados de **Lights-out Management (LOM)**.

  Podemos entender esse tipo de hardware como uma espécie de interruptor
  ou controle remoto do computador.

  Das tecnologias mais comuns, destacam-se:

  * O Intelligent Platform Management Interface (IPMI).
  * E mais recentemente o Intel Management Engine (ME).

  O funcionamento desses dispositivos é obscuro e muitas vezes eles nem mesmo
  podem ser desligados!

* Wireless, ou wifi: a conexão de rede sem fio também tem seus problemas:
  * Monitoramento de dispositivos: os dispositivos de rede sem fio possuem
    identificadores únicos que são chamados de **endereços MAC**, ou endereços
    de controle de acesso de meio. Esses endereços são utilizados na comunicação
    entre os dispositivos de rede wireless.

    Qualquer dispositivo wireless pode observar e gravar todos os endereços
    MAC que estão ao seu alcance. Não é preciso modificar o dispositivo para
    realizar essa coleta. Qualquer computador com o mínimo de configurações
    pode fazê-lo.

    Isso inclui não só o endereço dos pontos de acesso à rede mas de todos os
    computadores e outros dispositivos com wireless habilitado.

    Com a coleta dos endereços MAC, é possível monitorar o deslocamento de
    dispositivos num mesmo local -- por exemplo shopping center ou aeroporto --
    e até mesmo construir bancos de dados globais dessa informação.

    Podemos entender os endereços MAC como sendo um metadado que permite
    a identificação de um computador.

  * Problemas no firmware ou software, também chamado de **driver**, de operação.

    Isso é agravado pelo fato de que muitos softwares de operação são
    protegidos por segredos industriais e tem seu código fechado para análises.

  * Problemas na criptografia utilizada: é possível usar rede wireless sem
    criptografia alguma e nestes casos toda a comunicação é interceptável
    por qualquer computador que possua dispositivo wireless.

    Por esse motivo que é muito difundida o uso de criptografia em redes
    sem fio. No entanto, existem alguns padrões de criptografia muito fracos
    e com vulnerabilidades bem conhecidas como é o caso do WEP. Esse tipo
    de criptografia ruim pode ser quebrada com relativa facilidade.

* Rede com fios, também chamada de **ethernet**: analogamente ao caso da rede sem
  fio, a rede ethernet também é baseada em endereços MAC que podem ser coletados
  por qualquer outro computador conectado à mesma rede local.

  O software que controla a rede ethernet também pode conter várias falhas e,
  além disso, o ethernet é um dos canais principais utilizados para o **out-of-band
  management** que acabamos de mencionar.

* Bluetooth é um padrão de compartilhamento entre dispositivos que também pode
  estar sujeito tanto a ataques de leitura de endereço MAC quanto as outras
  vulnerabilidades existentes para redes sem fio.

* USB é um tipo de conexão para dispositivos que é muito prático mas que também
  pode ser uma grande fonte de problemas. Além de falhas de hardware e software,
  o padrão USB ainda tem uma funcionalidade que pode ser facilmente explorada:
  dispositivos USB podem ser "anunciar" ao sistema como sendo outros dispositivos.

  Assim, um pendrive USB podem se anunciar como outro tipo de hardware, por
  exemplo, uma placa de rede, fazendo com que o sistema tente inicializá-la
  como tal, o que pode se transformar num vetor de ataque ao sistema.

  Esse tipo de vulnerabilidade é conhecido como **BadUSB**.

  Tome cuidado ao utilizar dispositivos USB de terceiros ou encontrados por aí.
  E mesmo os seus dispositivos USB podem ser infectados com firmware malicioso.

* Teclado e mouse sem fio também podem ser grampeados. Alguns fabricantes destes
  produtos informam que os dispositivos sem fio são protegidos por criptografia
  forte, porém é difícil avaliar essa segurança de fato porque em geral os
  softwares que eles rodam não estão facilmente disponíveis para auditoria.

  Por isso, de preferência não use esse tipo de produto.

* Microfones: computadores recentes, especialmente laptops, vem equipados com
  microfones que podem ser ligados arbitrariamente por softwares espiões para
  operá-los como escuta ambiental.

* Câmera: o mesmo acontece com as câmeras existentes nos computadores mais novos,
  que também podem ser ligadas à revelia do usuário.

### Falhas de software

A exploração de falhas de software é muito mais comum, dentre outros fatores porque
elas são mais simples de ser disseminadas.

O limite da exploração das falhas do sistema é o comprometimento não só dos dados
do computador mas também a utilização da máquina para outros fins.

Existem vários vetores de ataques possíveis, mas o conceito de infecção básico
é sempre o mesmo: alguma fonte de dados ou software que são processados pelo
computador contém instruções que conseguem contornar o comportamento esperado,
produzindo erros, defeitos ou até mesmo conseguindo executar instruções arbitrárias.

A possível existência de backdoors e falhas, ou bugs de software é análoga ao
que acontece com o hardware: os softwares não estão livres de defeitos, sejam
oriundos de falhas de design, de programação ou até intencionalmente colocados.

Essas falhas podem ser exploradas manualmente. Isto é, uma pessoa pode operar
remotamente ou diretamente um computador para invadi-lo com a execução
passo-a-passo de operações. Isso acontece mas não é tão comum.

O que é mais comum é a invasão automatizada, muitas vezes sem nenhuma intervenção
humana. Procedimentos automatizados para a invasão desses sistemas são chamados
de **exploits**, os exploradores de falhas.

Existem vários tipos de exploits e muitas formas de classificá-los. Aqui dividiremos
em dois grupos:

1. Exploits remotos, quando o software de invasão roda remotamente a partir de um
   outro computador. Isso implica que existe alguma conexão de rede entre os dois
   computadores.

2. Exploits locais, que chamaremos de **malware**.

   Quando o vetor é um software que vai rodar no computador a ser infectado, ele é
   chamado de **malware**, ou software malicioso, que não precisa necessariamente
   ser executado pelo usuário. Muitos malwares aparentam ser arquivos comuns, e
   não programas, mas quando abertos disparam a execução de código arbitrário.

   Por exemplo, uma simples imagem ou um documento de texto podem conter instruções
   que aproveitam brechas nos softwares interpretadores desses arquivos para
   executar comandos arbitrários num sistema.

   A pessoa que abrir esses arquivos não perceberá o mal funcionamento, porque
   os arquivos aparentemente abrirão da forma correta: ela conseguirá ver a imagem
   e ler o documento, mas paralelamente o exploit estará rodando.

   Em laboratório até já se estudou a possibilidade de infecção através do
   microfone do computador!

   A infecção por exploits podem ocorrer pela rede ou por qualquer outra forma
   de entrada de dados, como discos externos, CDs e pendrives USB.

   O método de propagação do malware também pode ocorrer na forma de **vírus**
   -- programas auto-reprodutíveis que infectam outros programas -- ou
   **worms**, vermes -- programas mal-intencionados que se reproduzem.

   Alguns malwares se disfarçam como softwares inofensivos que desempenham
   alguma tarefa de fachada mas que, além disso, realizam alguma atividade
   escusa sem que o usuário perceba, como roubar dados do usuário, espioná-lo,
   etc. Estes malwares ganham o nome de **Cavalo de Tróia**.

   Vejamos alguns tipos de **malware** de acordo com o que eles fazem com o
   seu computador:

   1. Ransomware: nesse tipo de ataque os arquivos da vítima são criptografados
      com uma chave que só o atacante possui. Para liberar os arquivos, o atacante
      exige o pagamento de um resgate. É uma espécie de sequestro de arquivos.

   2. Spyware: são softwares espiões, cujo objetivo é espionar o usuário, roubar
      arquivos, etc.

   3. Robôs zumbis em botnets: muitos invasores não estão interessados em você
      ou nos dados do seu computador, mas sim em utilizar seus recursos computacionais
      para desempenhar tarefas.

      Estamos aqui falando de apropriação de recursos computacionais para todo o
      tipo de fim, incluindo enviar SPAM, invadir outras máquinas, propagar malware
      e assim por diante.

O caminho de uma invasão nem sempre é linear: kits de invasão podem consistir em
malwares e exploits remotos que disparem várias condições especiais no computador,
produzindo negação de serviço, execução arbitrária de código e até escalada de privilégios.

## Defesas

Existem muitas formas de defender o seu computador. Algumas são mais eficazes
do que outras. Algumas são mais fáceis do que outras.

Uma coisa que precisamos entender sobre o atual estado de coisas é que ele
é conveniente para parte da indústria de software e para governos que desejam
espionar as pessoas.

Parte da indústria que vende a solução dos problemas depende da indústria que
vende os problemas. Sabemos que soluções para computação mais seguras existem
e são viáveis, porém não interessam realmente à indústria. Quando não há uma
monopolização do mercado para forçar a continuidade de produtos ruins, a
própria competição degrada a segurança, pois a prioridade passa a ser
o lançamento de inovações em alta velocidade, sem tempo para se preocupar
com aspectos como a privacidade dos usuários.

Por isso, não podemos nem devemos esperar que as soluções de segurança sejam
desenvolvidas por esses atores. Em alguns casos podemos esperar sentados.
Noutros casos, as soluções que surgem são remendos que não atacam a raíz do
problema, como é o caso dos anti-virus.

Ter um computador com alto nível de segurança, hoje em dia, não é tarefa fácil,
mas também não é uma tarefa difícil ou extremamente difícil. Requer dedicação e
às vezes um pouco de recursos.

Agora, ter um computador com um nível razoável de segurança e acima da média já
está sim ao alcance de quem quiser.

Mostrarei algumas defesas, que vão do razoável, acima da média, até o alto
nível. Assim você pode ir se preparando aos poucos, com calma e até chegar no
nível que você achar suficiente.

### De que computador estamos falando?

Mas antes de falar sobre essas defesas, vale a seguinte pergunta: de computador
estamos falando:

1. Estamos falando de qualquer computador, isto é, o computador disponível
   no seu trabalho, escola ou numa lan house?

   Nestes casos estamos falando de um **Computador Público**, isto é, de
   computadores que estão fora do seu controle: você não sabe quem mais os
   utiliza, o que está instalado neles, se estão desprotegidos ou mal
   configurados, se existem mecanismos de espionagem instalados e assim por
   diante.

   Esta é uma situação bem duvidosa em termos de segurança. Evite esse tipo
   de computador para realizar qualquer atividade que tenha algum significado
   à sua privacidade. Se você não puder evitar você poderá ao menos utilizar
   algumas das defesas que mencionaremos adiante.

   Pode até ser que o computador público seja confiável em termos de segurança,
   a depender da competência de quem o administra e também da confiança de você
   tem de quem o opera, mas isso não está garantido a priori.

2. Estamos falando do seu computador pessoal, isto é, de um computador que
   é seu e está em seu controle.

   Se você protege esse seu computador com algum esquema de segurança e
   o utiliza para lidar com informações sensíveis à sua privacidade, diremos
   que este é o seu **Computador Pessoal Confiável**.

   Este então é um computador com o qual você estabelece uma relação de
   confiança, por mais que você não confie totalmente que ele esteja livre de
   invasões.

   Caso você não proteja esse seu computador de modo algum, então estamos
   tratando de um **Computador Pessoal** que é quase igual a um **Computador
   Público** em termos de segurança.

Por isso digo: o fator número zero na segurança do computador é saber **quem**
controla o computador. É a partir disso que temos condições de intervir no seu
funcionamento para melhorar suas defesas e evitar que ataques sejam
perpetrados.

### Acesso físico

As primeiras defesas estão no nível do acesso físicos:

1. Ter o seu próprio **Computador Pessoal Confiável** é um passo importante nesse
   sentido.

   Se você ainda não tem um computador pessoal e acha que esta seja uma medida
   importante para a sua privacidade, considere adquirir um. Você não precisa
   começar com o último modelo. Dê uma olhada nos usados. Se comprar um computador
   usado, tente pelo menos substituir seu disco por um novo, pois esse é um dos
   componentes que mais falham nos usados. Também dê uma olhada na expectativa
   de vida da bateria, no caso de um laptop.

   Depois, você pode prepará-lo para ter um bom nível de segurança e mantê-lo
   sempre em bom funcionamento, mas é importante também evitar que ele seja
   fisicamente capturado, o que traria fortes suspeitas de instalação de
   grampos.

   É uma boa política você não usar mais o seu computador depois que ele for
   capturado, não importa como e por quem. Se você descobriu que ele foi capturado,
   procure não utilizá-lo mais.

   Pode ser que você não tenha condições financeiras para simplesmente deixar de
   usá-lo. Neste caso, tente conseguir ajuda para alguém com conhecimentos
   fazer o **exorcismo** da máquina, isto é, tentar tirar na medida do possível
   qualquer indício de invasão, reinstalar o sistema, etc.

   Ou, se você não conseguir esse tipo de assistência, tente vendê-lo para alguém
   ciente do problema e com condições de reutilizá-lo sem sofrer danos. Assim
   você consegue pelo menos recuperar o dinheiro para comprar um hardware novo.

   Problema maior ainda é saber **se** o seu computador pessoal foi capturado
   ou não. A não ser que você faça vigílias constantes e mantenha seu computador
   sempre junto a si, você nunca vai saber. De fato, algumas pessoas que são
   alvos de vigilância pesada acabam por adotar a prática de sempre andarem
   junto do seu computador.

   Mas essa é uma prática para casos extremos. Num nível de segurança acima
   da média, você pode simplesmente evitar de deixar seu computador dando
   sopa em qualquer lugar. Deixe-o apenas em lugares que você tiver uma
   confiança mínima a respeito da segurança. Não estamos falando apenas
   de roubo, mas também da implantação de hardware e software malicioso
   usando acesso físico à máquina.

2. Usar defesas básicas, que são muito simples de serem adotadas:
    * Se for um laptop, usar um cadeado do tipo Kensington para evitar
      roubos ligeiros.

    * Deixar a tela travada se tiver de deixar o computador ligado e
      desatendido, o que evita os atacantes curiosos e menos refinados.

    * Aqui vale a regra do **Protect Your Belongings**, ou Proteja
      os seus pertences. Também tente não ostentar demais o equipamento
      e manter uma linha mais low profile.

3. Criptografia de armazenamento: protege os arquivos armazenados no
    computador, mas não protege todos os softwares.

    Os softwares básicos de inicialização (BIOS e carregador do sistema
    operacional) continuam expostos à adulteração por atacantes físicos e
    remotos.  Um atacante pode colocar um leitor de senhas no seu computador e
    capturá-las no momento que você as digita ao iniciar seus discos
    criptografados, num ataque conhecido como **Evil Maid**, em referência aos
    serviços de quarto em hotéis que podem ser compostos por espiões
    disfarçados.

    Apesar disso, a criptografia de disco oferece boa proteção caso o seu
    computador seja capturado e esteja desligado.

    Note que isso implica que você pode criptografar não só os seus arquivos
    mas também o próprio sistema operacional, o que chamamos de **Full Disk
    Encryption (FDE)**, mas mesmo assim você ainda estará à mercê de ataques
    mais sofisticados como o Evil Maid.

    Se o seu computador for capturado enquanto estiver ligado, então um
    atacante poderá tentar ler as chaves de criptografia de disco diretamente
    da memória RAM, em ataques de **análise forense de memória**.

    Vou dizer mais uma vez. A criptografia de disco não oferecerá grandes
    proteções se o sistema for roubado ou invadido enquanto ele estiver ligado,
    pois nessa condição as informações estarão disponíveis para acesso.

    O mesmo acontece se o computador for capturado enquanto estiver no estado
    semi-desligado de baixa atividade conhecido como **suspensão**. Nesse
    estado, muitas partes do computador estão desligados mas a memória de
    curto prazo (RAM) estará ativa e armazenando a chave criptográfica do seu
    dispositivo criptografado.

    Em algumas situações é possível resgatar as senhas criptográficas momentos
    depois que o computador é desligado, a depender do tipo de memória RAM que o
    computador utiliza, num ataque conhecido como **Cold Boot**.

    * Ande com o computador sempre desligado, pois isso evita que um adversário
      habilidoso consiga extrair senhas que estejam na memória operacional
      (RAM) do computador.

    * Desligue o computador quando não for usá-lo. Alguns sistemas possuem a
      funcionalidade de **hibernação**, que, ao contrário da **suspensão**,
      armazena o estado da memória RAM no disco criptografado e posteriormente
      desligando efetivamente a máquina.

      Na dúvia se seu sistema utiliza hibernação ou suspensão, recomendo que
      você simplesmente desligue seu computador quando não for utilizá-lo.

4. Defesas contra canais laterais: estas são mais difíceis, porém são contra
   ataques não muito difundidos hoje em dia até onde se sabe. Aqui coloco
   apenas ilustrativamente.

   Você tanto pode comprar computadores que possuem blindagem a diferentes
   tipos de emanações, que são difíceis de encontrar, ou tentar utilizá-los
   sempre fora da tomada ou dentro de instalações que você julga serem seguras.

Estas são algumas das defesas físicas possíveis para computadores.

### Hardware

Agora falaremos das defesas contra ataques ao seu hardware que independem
de acesso físico à máquina!

A recomendação básica é: se você não estiver usando um dispositivo de entrada
ou saída, deixe-o desligado ou desativado.

1. Uma das defesas mais básicas consiste em cobrir a câmera com um adesivo,
   para que, mesmo que um software espião ligue a sua câmera, ele não
   consiga capturar alguma imagem.

2. Com relação ao Bluetooth, mantenha-o ligado apenas durante o uso ou,
   se não for utilizá-lo, desabilite-o completamente.

3. Para as redes sem fios, procure utilizar apenas pontos de acesso
   que usem criptografia do tipo WPA2+AES e uma senha de tamanho
   razoável.

   Se você for configurar um ponto de acesso wireless, use também
   o padrão WPA2+AES.

   Mesmo assim, não conte com essa criptografia para a proteção dos seus dados
   e procure usar serviços que também utilizem criptografia, de modo que você
   esteja utilizando diversas camadas de criptografia ao mesmo tempo.

   Considere que a criptografia do ponto de acesso oferece proteção baixa
   e só opera entre o seu computador e o ponto de acesso, deixando de fazer
   efeito conforme as mensagens trafegam no resto da rede.

4. Backdoors de hardware: em alguns casos é possível corrigir a falha através da
   atualização do firmware, do software que o utiliza ou pela substituição do
   dispositivo. Em outros casos tudo o que podemos fazer é desabilitar o
   aparelho ou conviver com a falha.

   * Se o seu computador possui conextão firewire, desabilite-a. Habilite
     apenas durante o uso.

   * Se possível, desabilite a manunteção remota, o que é feito pela
     BIOS. Alguns processadores mais novos não permitem que isso
     seja desabilitado. Então você pode escolher conviver com essa
     possibilidade de invasão ou então tentar comprar um computador
     cujo processador não possui essa funcionalidade ou que ao mesmo
     permita o seu desligamento.

   Outra defesa contra backdoors é a utilização de hardware livre, mas
   essa é uma alternativa ainda pouco acessível.

5. Nível hardenado: só para ilustrar, vou falar de algumas coisas que você
   pode fazer se quiser ter um computador de nível **hardenado**, isto é,
   muito acima da média. Mas atenção: isso acarreta em mudanças que podem
   quebrar seu computador se você não souber fazer corretamente. Isto
   é por sua conta e risco e não nos responsabilizaremos por quaisquer
   danos:

   * Subsituição da BIOS, isto é, do firmware principal. Existem
     projetos como o Libreboot que implementação versões em software
     livre da BIOS do computador. Só funciona para alguns modelos
     específicos de computadores.

   * Ativação de senha da BIOS para inicialização do computador ou
     para mudar configurações básicas. Essa não é uma super medida
     de segurança mas pelo menos pode atrasar o tempo de trabalho
     de um atacante.

   * Desligamento físico de alguns dispositivos, como microfones e
     adaptadores bluetooth. Estando fisicamente desligados pelo corte
     de fios e pinos, eles não tem como ser ativados.

   * Troca de dispositivos proprietários por versões mais abertas:
     é a substituição de componentes do computador como placas de
     rede e vídeo por alternativas que sejam mais compatíveis com
     software livre e que também tenham um histórico de funcionamento,
     segurança e estabilidade maiores.

   * Randomização de endereço MAC: é possível configurar o seu
     computador para que ele sempre utilize um endereço MAC aleatório
     tanto para conexões com fio padrão ethernet quanto para wireless.

     Isso evita o rastreamento de dispositivos via coleta de endereço
     MAC, mas não evita outros tipos de identificação.

   * Defesas contra ataques na USB, isto é, mitigação ao BadUSB,
     são em geral bem desconfortáveis. Elas vão desde a desabilitação
     das portas USB, passando pela habilitação durante o seu uso, o
     que evita um atacante plugue um dispositivo USB malicioso mas
     não evita que você faça isso deliberadamente quando usa um
     dispositivo infectado sem saber.

     Outra alternativa é o uso das chamadas "camisinhas USB", consistindo
     em usar um outro computador para fazer a conexão USB com o dispositivo,
     compartilhando-o com o seu computador através de algum outro meio.
     Esse tipo de defesa é poquíssimo usada.

     Outra mitigação possível é o bloquear todos os dispositivos USB exceto
     aqueles que apresentarem os identificadores correspondentes aos
     dispositivos que você conhece. Novamente, isso pode eliminar algumas
     tentativas de ataque mas não impedem que um dispositivo malicioso
     utilize um dos IDs autorizados.

   * Air gap, ou isolamento físico do computador, é a prática de deixar
     o computador o mais isolado possivel do mundo externo. Isso implica
     em cortar qualquer possibilidade de conexão de rede e usar apenas
     meios considerados seguros para transmissão de arquivos.

     O conceito de Air gap é controverso, porque os computadores foram
     feitos para operar em rede.

     Manter um computador desse tipo atualizado é uma tarefa complicada
     e nada garante que algum arquivo transferido para ele não contenha
     algum código malicioso que possa ser usado para a extração de dados.

### Software

Por fim, falaremos agora das defesas que podem ser adotadas no nível de
software.

1. Use **Software Livre**, que é o oposto do chamado **Software Proprietário**.
   Todo software livre é baseado em pelo menos quatro liberdades fundamentais:

   0. Rodar o software no seu computador.

   1. Estudar o software, isto é, analisar o seu código. Muitos softwares
      são escritos em linguagens mais inteligíveis para seres humanos do
      que o código que é interpretado pelo processador do computador.

      Para que possam rodar no computador, o código do software escrito por
      alguém, chamado de **código fonte**, precisa ser traduzido para a
      linguagem da máquina, procedimento conhecido como **compilação**.

      A liberdade de estudar o software implica na capacidade de qualquer
      pessoa de acessar o código fonte do software, e não apenas a versão
      compilada, também chamada de **binária**, do software, como é
      o caso dos softwares proprietários.

   2. Redistribuir o software sem restrições. Isso implica que você
      pode vender o software livre e prestar serviços com ele mesmo que
      você não tenha escrito o software! Por outro lado, você não pode
      impedir alguém de fazer o mesmo.

      Sempre tem alguém distribuindo software livre sem cobrar financeiramente
      por isso, então ele também é gratuito nesse sentido.

   3. Melhorar o software, isto é, alterar o seu código corrigindo falhas
      ou implementando novas funcionalidades.

   Todas essas quatro liberdades são essenciais para a segurança da informação:

   0. Rodar o software implica que você pode utilizá-lo no seu próprio computador,
      ao invés de ter que depender necessariamente do software rodando num computador
      que você controla e que portanto você não sabe se está em controle de atores
      mal intencionados.

   1. Estudar o software significa que qualquer pessoa pode estudá-lo em buscas
      de mal funcionamento e problemas de segurança.

   2. Redistribuir implica em colaborar para a difusão do software, o que pode
      atrair mais pessoas, por exemplo, para estudar o seu funcionamento e
      encontrar possíveis falhas.

   3. Melhorar o software implica que qualquer pessoa com capacidade técnica
      pode corrigir algum problema de segurança que foi encontrado.

   **Ter** essas quatro propriedades não implica necessariamente que essas
   quatro propriedades estão sendo **exercidas** automaticamente. O software
   é apenas uma porção de código, enquanto que a sua evolução depende da
   atividade de pessoas.

   Não é todo mundo que tem os meios de rodá-lo (é preciso ter um computador),
   de estudá-lo (é preciso de tempo e conhecimento), redistribuí-lo (o que hoje
   em dia depende mais de vontade) e de melhorá-lo (novamente, tempo e conhecimento).

   Por isso, com o software livre é muito mais fácil fazer uma auditoria de segurança
   pois para isso não é preciso pedir permissão a ninguém, mas isso não quer dizer
   que automaticamente todo o software livre é auditado.

   Essa transparência também é refletida nos anúncios de atualização dos softwares
   livres, que muitas vezes indicam quais vulnerabilidades estão sendo corrigidas.

   Assim, rodar software livre pode ser considerado um pré-requisito para a segurança
   da informação. É uma condição necessária, porém não suficiente para a segurança.

   Benefícios:

   * Usuário comum não tem superprivilégios por padrão nestes sistemas.

   * Arquivos anexados não são executados por padrão.

   * Canais -* ou lojas -* de instalação de software fazem parte do próprio
     sistema, minimizando a instalação de programas distribuídos por terceiros.

   * Não te espionam por padrão como outros sistemas operacionais proprietários.

  Problemas:

  * Arquitetura não é 100% segura. O foco do desenvolvimento tem sido performance
    e suporte e não segurança. Mas ainda assim são muito melhores do que os sistemas
    operacionais proprietários.

2. Use Sistemas Operacionais Livres!

   Você pode rodar não só programas livres mas todo o software de operação do seu
   computador pode ser livre.

   Eu recomendo você utilizar o Debian, que é um sistema operacional desenvolvido
   de forma comunitária e com uma política de segurança razoável.

   Se você não conseguir usar o Debian, uma alternativa é o Ubuntu, que é mantido
   por uma empresa e é baseado no Debian.

   Sistemas como o Debian já vem com perfis de segurança razoáveis e podem ser
   instalados de forma criptografada no armazenamento do seu computador.

3. Use virtualização!

   Computadores são máquinas que podem simular máquinas. Podem até simular
   a si mesmos. Uma máquina virtual é um programa de computador que simula
   um computador.

   No contexto da segurança, máquinas virtuais são criadas para criar um
   cordão de isolamento para que vulnerabilidades de um programa rodando
   dentro de uma máquina virtual não consigam escapar para o resto do
   computador.

   Na prática, é possível rodar um sistema operacional inteiro juntamente
   com aplicativos dentro de uma máquina virtual. Se você tem um computador
   razoável, pode tranquilamente rodar uma máquina virtual com navegador
   web, editor de textos e até ser capaz de assistir um filme!

   Este isolamento é muito bom mas não é isento de problemas. Falhas no
   software virtualizador podem ser utilizadas para que programas maliciosos
   consigam escapar da máquina virtual e infectar outras partes do seu sistema.

   Contudo, máquinas virtuais ainda oferecem um bom nível de proteção,
   em muitos casos permitindo até que você rode um sistema operacional
   ou softwares proprietários em ambiente virtualizado.

   Lembre-se que a virtualização protege o resto do sistema contra
   vulnerabilidades nas aplicações virtualizadas, e não o contrário!

3. Tenha mais de um dispositivo e compartimente o uso.

   Pode ser que por algum infortúnio você precise usar software proprietário.
   Ou que tenha que usar serviços de segurança duvidosa e assim por diante.

   Nesses casos, você pode utilizar mais de um dispositivo com perfis de
   uso distintos.

   Por exemplo, um dispositivo mais "sério", mais protegido e com dados sensíveis.
   E um outro dispositivo para qualquer outra coisa. Esse segundo dispositivo
   pode ser um tablet ou um computador velho.

   Pode ser um dispositivo apenas com software livre e outro em que seja
   permitido o uso de software proprietário.

   Esta pode ser uma boa alternativa caso você não confie na proteção
   oferecida pelas máquins virtuais.

4. Tenha mais de um sistema operacional no seu computador, compartimentalizando
   o uso de softwares mais e menos confiáveis assim como informações mais ou
   menos sigilosas, o que mitiga um pouco o problema de rodar softwares não tão
   confiáveis, mas não é tão eficaz pois a invasão de um sistema pode levar à
   invasão de outro.

   Adote essa alternativa apenas em casos que você precise de dois perfis de uso
   e as opções anteriores não possam ser adotadas.

5. Use sistemas operacionais super-seguros.

   Três sistemas operacionais livres e orientados à segurança são o Tails, o
   Subgraph e o Qubes.

   Todos os três podem ser rodados no modo **live**, isto é, não precisam ser
   instalados no armazenamento do seu computador e podem ser mantidos num
   pendrive USB. Se você usar um pendrive pequeno, ele pode até ficar dentro
   da sua carteira para você tê-lo à mão sempre que precisar e esta pode ser
   uma boa alternativa se você tem apenas um único computador e quer ter
   dois perfis de segurança distintos.

   O Tails é um sistema operacional baseado em Debian que é feito para oferecer
   segurança e anonimato e sobretudo não deixar rastros de que foi rodado.

   Ele possui uma série de melhorias de segurança e todo o tráfego de rede
   que ele gera segue através da rede de roteamento Tor, sobre a qual veremos
   adiante no curso.

   O Tails pode até ser utilizado em computadores de terceiros desde que você
   consiga reiniciá-los, oferecendo um nível razoável de segurança, a não ser
   que o hardware do computador já esteja bem comprometido.

   O Subgraph, até o momento que este curso foi feito, ainda não foi lançado
   oficialmente, mas promete ser uma versão melhorada do Tails. Além do tráfego
   roteado pelo Tor e diversas melhorias de segurança, o Subgraph ainda roda
   diversos programas dentro de ambientes virtualizados, garantindo o isolamento
   de aplicações como navegadores web e leitores de documentos.

   O Qubes é um sistema parecido com o Subgraph nesse sentido, com a diferença
   que não roteia todo o tráfego pelo Tor por padrão mas pode ser instalado
   no seu computador além de rodar no modo live e também possui uma maturidade
   maior pela sua idade.

   Outro sistema que você pode tentar é o OpenBSD, um dos mais seguros em
   existência. Demora um tempo para aprender a usar, mas ele é muito bem feito!

   Se quiser seguir o caminho do aprendizado, você pode pegar um sistema como o
   Debian e ir aplicando mudanças de segurança como por exemplo o PaX e o
   grsecurity, ou pelo menos tentar o AppArmor, que forçam políticas restritivas
   de execução de aplicações, dentre outras melhorias de segurança.

6. Rode a menor quantidade de software possível. Esta é a aplicação pura do
   princípio da simplicidade, ou complexidade necessária: quanto menos código
   você roda, menor é a probabilidade de falhas.

   Considerando que um computador típico roda uma quantidade de instruções
   correspondentes a milhões de linhas de código, a quantidade de vulnerabilidades
   de segurança é igualmente enorme.

   Esta pode ser uma opção controversa, mas ela tem suas razões considerando
   a quantidade enorme de falhas em software que são reveladas toda a semana.

   Se você puder e quiser seguir esta linha, procure também os softwares cujo
   código seja menor, sejam mais bem escritos, tenham poucas funcionalidades
   mas boa qualidade de funcionamento, tenham uma boa base de usuários e sobretudo
   um histórico de falhas baixo.

7. Mantenha seus softwares sempre atualizados!

   Toda semana existem várias atualizações de segurança para tudo quanto é tipo
   de software. Isso significa que, pelo menos a partir do momento desses lançamentos,
   as vulnerabilidades existentes nesses softwares passam a ser públicas.

   Isso implica que, caso você deixe de fazer uma atualização de segurança,
   você estará deixando softwares no seu computador que possuem falhas de
   segurança conhecidas.

   Por isso, mantenha sempre que possível todo o seu software atualizado.

   Isso não vai oferecer uma proteção total, mas já é um começo. Podem ser
   que existam vulnerabilidades nos softwares que você usa mas que não sejam
   conhecidas.

   Esse tipo de vulnerabilidade é chamada de **zero-day**, ou vulnerabilidade
   de zero-dias, indicando que ela ainda não é pública e por isso tem zero
   dias de vida pública.

   É até possível que alguém conheça essas vulnerabilidades mas não as divulga
   para justamente poder tirar vantagem da ignorância sobre elas.

   Ou seja, manter seu software atualizado não te protege contra falhas
   de zero dias, mas protege das falhas conhecidas que já foram corrigidas.

8. Não instale qualquer software.

   Esta é uma dica básica. Cuidado com o software que você for instalar
   no seu computador. Não execute qualquer programa que enviarem pra você.

   Se você está usando um sistema operacional livre como o Debian, use
   a própria central de instalação de programas que ele oferece para
   encontrar softwares que satisfaçam sua necessidade.

   Os programas são distribuídos nessa central num formato chamado de
   **pacote**, que são versões dos programas bem compatíveis com o sistema
   operacional, fazendo a instalação, remoção ou atualização de modo
   facilitado.

   Mesmo usando essa central de programas, tome cuidado: não é porque
   está listado na central que é um software seguro.

   Se você tiver na dúvida sobre algum software mas precisa rodá-lo,
   use a abordagem do isolamento com máquinas virtuais ou num computador
   secundário.

9. Não instale softwares de fontes desconhecidas.

   Você pode confiar num determinado software, mas cuidado ao baixá-lo:
   você está baixando do local oficial de distribuição? Esse download
   é feito com conexão criptografada? Você possui algum meio de verificar
   se você baixou o software legítimo e não uma versão contendo código
   malicioso?

   Existem algumas formas de fazer a checagem de integridade do software
   que você baixou.

   Sistemas livres como o Debian já fazem isso quando você instala um
   pacote, fazendo uma checagem criptográfica dos arquivos baixados.

   Mas você ainda precisa fazer isso ao menos uma única vez... quando
   for baixar o Debian em si.

   Para fazer essa checagem no Debian ou em qualquer outro software,
   consulte a documentação correspondente.

10. Anti-virus e detectores de intrusão.

    Um anti-virus pode te ajudar a eliminar alguns softwares maliciosos
    do seu computador, mas essa é uma media bem paliativa.

    Anti-virus são softwares que varrem o seu computador em busca de
    arquivos e regiões da memória infectados por softwares maliciosos
    já bem conhecidos.

    Eles tem várias limitações: não detectam softwares maliciosos
    ainda pouco conhecidos.

    Sistemas operacionais como o Debian, que são baseados no ecosistema
    conhecido como GNU/Linux são feitos numa arquitetura que torna mais
    difícil a contaminação por malware.

    Mesmo assim existem softwares similares aos anti-virus mas que também
    detectam outros tipos de invasão. Estes são conhecidos como
    **detectores de intrusão**.

11. Firewall.

    Outra media importante é a utilização de um **firewall**, que é uma
    barreira erguida nas conexões de rede do seu computador para filtrar,
    recusar ou descartar conexões indesejadas. Assim, você pode limitar
    o contato do seu computador na rede.

Estas são as principais defesas que você pode aplicar nos seus computadores.
Existem muitas outras!

## Resumo

1. O computador é uma máquina de processamento geral. Isso implica que ela pode
   ser utilizada por terceiros para obter suas informações.

2. Existem muitas falhas nos computadores em em todos os níveis: no hardware,
   no sistema operacional e nos programas utilizados, assim como muitas formas
   de defender.

   O básico inclui o uso de software livre, criptografia de armazenamento
   e manter o sistema sempre atualizado. Outras alternativas simples são ter
   mais de um computador por perfil de uso ou o uso de sistemas operativos
   mais seguros e que rodam sem instalação, como o Tails e o Subgraph.

## Atividades

1. Agora você pode completar o seu Checklist de Segurança para incluir
   uma política de defesa em relação ao uso de computadores.

   Assim, proceda criando um item na lista para tratar com os seus computadores
   e com computadores de terceiros.

   Você terá um computador sob seu controle? Outras pessoas terão acesso
   a ele?

   Você usará computadores que você não controla? Para que tipo de atividades?

   Você terá um segundo dispositivo para usar com atividades não muito
   sensíveis?

   Qual serão as defesas que você adotará em cada um desses casos?

1. Instale o [Tails](https://tails.boum.org/) num pendrive USB e teste-o em diversos computadores.

2. Faça o mesmo com o [Subgraph](https://subgraph.com/).

3. Teste o [Qubes](https://www.qubes-os.org/).

4. Gostou do GNU/Linux? Instale no seu computador usando armazenamento
   criptografado. Mas **ATENÇÃO**: faça backups de todos os seus dados num
   outro lugar antes de proceder com a instalação, pois ela passará por cima
   dos arquivos armazenados.
