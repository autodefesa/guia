# Introdução

Este não é um guia sobre guarda-costas digitais. Não vamos ensinar sobre como buscar
ajuda para se defender. Aqui não temos interesse em alguém fazendo nossa segurança.

Pelo contrário, queremos que você aprenda a se defender.

Autodefesa é a capacidade de uma pessoa ou grupo se proteger por contra própria.
Autodefesa digital é aplicação desse conceito no uso de tecnologia de comunicação.

O caminho da autodefesa é mais difícil. Requer que você estude e pratique. Mas também
é o caminho mais rico. Nele, você aprende mais e se torna uma pessoa mais completa e
capacitada para passar o conhecimento adiante.

Este guia está dividido em capítulos, cada um deles com um tema específico. Falaremos sobre
segurança em computadores, em dispositivos móveis e também na web.

Cada tema sempre será apresentado de acordo com a seguinte sequência:

1. Funcionamento de uma dada tecnologia.
2. Ameaças dessa tecnologia.
3. Defesas viáveis.

Você pode seguir os capítulos de forma independente. Você pode inclusive já pular diretamente
para algum assunto que seja do seu interesse ou necessidade imediata.

Mas caso você queira entender a lógica e os princípios básicos da segurança, recomenda-se
que você comece pelo material introdutório.

Assistir o guia todo também é uma boa ideia, pois a bagagem teórica de um
capítulo às vezes toma emprestado conhecimentos dos capítulos anteriores.

O guia é acompanhado de algumas atividades propostas para que você se exercite e tenha
um incentivo para aplicar os paradigmas de segurança da informação na sua vida cotidiana.

Mais importante do que você adotar todos os procedimentos de segurança sugeridos ao longo
do guia, é você entender os esquemas mentais de quem pensa em segurança, aquilo que chamanos
de "mindset". Tendo isso, você poderá fazer **escolhas conscientes** sobre a sua segurança
digital.

Por que este guia é importante?

Antigamente, para vigiar uma pessoa e obter informações sigilosas, era necessário manter
uma pessoa dedicada a esta tarefa. A espionagem era limitada principalmente pelo fator
humano. Espionar era um privilégio de poucos governos, empresas e instituições. Ser espionado
era um sinal de estar realizando alguma atividade de importância que despertou o interesse
de algum poder.

Aos poucos as instituições humanas vão mudando e surgem diversas formas de vigilância.
Em particular, com a difusão da informática os custos da comunicação vão diminuindo.

Com isso, os custos da espionagem na comunicação digital também vão caindo, porque um
mesmo aparato de vigilância pode coletar, processar e analisar milhões de comunicações
de milhões de pessoas a um custo baixíssimo.

Nós **vivemos na era da vigilância de massa**. Pode ser que você não seja uma pessoa de
interesse muito grande para algum poder, mas mesmo assim a sua comunicação pode estar
sendo automaticamente interceptada ou armazenada sem o seu consentimento.

Simplesmente porque é barato. E porque mesmo a informação de pessoas consideradas comuns
hoje tem valor estratégico. Para decisões políticas. Econômicas. Sociais.

A Educação, pelo menos em teoria, é a forma de preparar pessoas livres. Hoje,
um conhecimento mínimo de tecnologia e autodefesa digital é parte importante
para que uma pessoa possa exercer suas escolhas, sua profissão ou toda a sua
vida com liberdade.

Não vivemos em um mundo onde podemos ignorar completamente o seu funcionamento
e especialmente o funcionamento básico da tecnologia. Nem vivemos em um mundo
onde as tecnologias de comunicação são seguras por padrão. Vivemos numa
situação em que as tecnologias são mal feitas no que diz respeito à segurança e
seus problemas técnicos são usados contra nós.

Este guia foi feito para todos os níveis de entendimento. Ele serve para você
mesmo que você seja iniciante ou expert em segurança. Porque ele é um guia
completo. Ele contém o **mindset**, ou forma de pensar a segurança digital.

Também é um guia modular e cheio de conceitos. Se você quiser pode pular
direto para temas mais práticos e do seu interesse.

Se você aproveitar todo o guia, terá uma boa base para começar a se capacitar
para ser um profissional em segurança da informação realizando outros guias e
obtendo certificações se achar necessário.
