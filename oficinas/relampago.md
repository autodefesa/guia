# Oficina Relâmpago

## Missão

0. Explicar os conceitos básicos em segurança da informação
1. Recomendar práticas e ferramentas mais seguras
2. Introduzir a forma de pensar e agir em segurança para cultivar a autodefesa

## Formato

0. Mini-palestra
1. Dúvidas anônimas anotadas em cartões coloridos por tópico
2. Respostas às dúvidas no final da oficina
3. Pode tirar fotos dos slides
4. Não tirar fotos ou filmar ninguém, incluindo a equipe da oficina
5. Não gravar o áudio da oficina

## Conceitos

Conceitos que serão abordados brevemente (5 minutos cada tema):

0. [A Economia da Segurança](../basico.md).
1. [O Plano de Autodefesa](https://plano.autodefesa.org/planilha.html)
2. [A Segurança da Informação](../avancado.md).
3. [A Teoria da Comunicação Hacker](../comunicacao.md).

## Prática

O Plano Básico de Autodefesa Digital:

0. Senhas
1. Navegador Tor com HTTPS
2. Email mais seguro
3. Telefones móveis
4. Signal
5. Tarefas para a casa
6. Referências
7. Respostas e parte prática

## A Economia da Segurança (5 min)

1. Segurança: toda prática que nos ajuda a agir ao reconhecer e reduzir riscos;
   é o oposto da paranóia.

2. Balanço: adote as práticas de segurança que sejam mais eficazes e menos
   custosas aos riscos que sejam mais prováveis!

3. Redução de danos: adote aos poucos as práticas de segurança.

4. Privacidade: conjunto de informações que queremos proteger.

5. Níveis: segurança se dá por níveis e procuramos proteger primeiro os níveis
   mais fundamentais.

## A Economia da Segurança - 2

6. Compartimentalização: é a prática de segurança de isolar informações
   de acordo com a sua importância e necessidade. Por exemplo, falar com
   uma pessoa apenas o necessário para uma dada ação e manter algumas
   informações em círculos restritos de acesso.

7. Obscuridade: assuma que o inimigo conhece todas as suas defesas,
   mesmo que você não saiba se isso é verdade ou não. Isso vai te ajudar
   a contar apenas com a eficácia das suas defesas, e não com o fato
   dela ser ou não ser conhecida.

8. Resiliência: é a capacidade de resistir e se recuperar de ataques.
   Se as falhas não forem em pontos críticos, é possível se recuperar.
   Assim, é importante reduzir os pontos críticos de falha.

## O Plano de Autodefesa

É o guardachuva de toda a segurança!

    =======  ====== ============= ================= =====
    Item     Ameaça Probabilidade Defesa            Custo
    =======  ====== ============= ================= =====
    Celular  Roubo  Alta          Backups cifrados  Baixo
                                  Celular desligado Baixo
                                  Z                 Alto
             X
             Y
    =======  =====  ============= ================= =====

Detalhes [nesta planilha](https://plano.autodefesa.org/planilha.html).

## A Segurança da Informação

Propriedades úteis:

1. Confidencialidade: o conteúdo mensagem não será lida por pessoas ou sistemas indesejáveis.

2. Integridade: é a garantia de que o conteúdo da comunicação não foi
   adulterado por terceiros.

3. Disponibilidade: é a garantia de que o sistema de comunicação estará acessível
   sempre que necessário.

4. Autenticidade: garante que cada uma das partes possa verificar se está de
   fato se comunicando com quem pensa estar se comunicando, isto é, a garantia
   de que não há um impostor do outro lado da comunicação.

## A Segurança da Informação - 2

5. Negação plausível: o oposto do não-repúdio é a negação plausível, no caso
   onde não é possível determinar com certeza se determinada pessoa participou
   da comunicação.

6. Anonimato: é garantia de que as partes envolvidas na comunicação não possam
   ser identificadas.

7. Auditabilidade: capacidade de inspecionar os sistemas de comunicação.

## A Teoria da Comunicação Hacker


                      grampo ativo ou passivo
                                |
                                |
      Emissor/a --------------meio----------------- Emissor/a
      Receptor/a      -----mensagem-1------>        Receptor/a
          1           <----mensagem-2-------           2


* Ataques: Interceptação de Dados e/ou Interceptação de Metadados.
* Algumas práticas de segurança protegem apenas os dados, enquanto outras
  apenas metadados; outras, ambos.

## Senhas (cartões azuis) (10 minutos)

* Um segredo fácil de lembrar e difícil de descobrir para acessar sistemas.
* Como criar: dando asas à imaginação!
* Como memorizar: um pouco por dia; primeiro crie a senha, depois use.
* Como lidar com muitas senhas: uso, memória, círculos de senhas, gerenciadores de senha.

## Navegador Tor com HTTPS (cartões verdes) (15 minutos)

* O que é?
* Do que protege
* Do que não protege

## Email mais seguro (cartões amarelos) (10 minutos)

* O que é?
* Do que protege
* Do que não protege

## Telefones móveis (cartões rosas) (10 minutos)

* Problemas: GSM, GPS, Wifi, Bluetooth, NFC, Câmera, Sensores, Sistema
* Como contorná-los

## Signal (cartões laranjas) (20 minutos)

* O que é?
* Do que protege
* Do que não protege
* Boas práticas
     * Não usar o teclado padrão do telefone
     * Cuidado com o compartilhamento de contatos
     * Sempre usar mensagens temporárias
     * Frase senha de bloqueio do aplicativo
     * PIN code para evitar clonagem da conta
     * Verificação de contatos
     * Como perder um telefone corretamente

## Bônus (cartões brancos) (10 minutos)

* Tails
* Etc!

## Tarefas para a casa

0. Criar seu Plano Básico de Autodefesa
1. Criar senhas mais seguras
2. Instalar o Signal
3. Instalar o Navegador Tor
4. Criar email mais seguro
5. Ler o Guia de Autodefesa Digital
6. Bônus: usar o Tails

## Referências

* Guia de Autodefesa Digital: https://guia.autodefesa.org
* Slides desta oficina: https://guia.autodefesa.org/_static/slides/relampago.pdf

## Respostas e parte prática

?
