# Oficina Padrão

## Metodologia

* Documentação da oficina pode ser redigida e complementada colaborativamente
  pelos/as participantes e publicada após o evento!
* Em cada dia será passada uma leitura de base para o conteúdo da aula
  seguinte.
* Todo dia alguém trazer algum caso prático ou teórico de vulnerabilidade.
* Elaborada para que seja reprodutível, oferecendo material suficiente para que
  a mesma possa ser reproduzida dentro dos critérios de segurança oferecidos.

## Programação

### Palestra: Segurança, privacidade e espionagem: o que está acontecendo e como podemos nos proteger?

Palestra introdutória contextualizando a necessidade de privacidade e
anonimato utilizando modernas técnicas de criptografia frente às
recentes revelações sobre espionagem de massa.

### Dia 1: `Discussão teórica sobre vigilância, espionagem e segurança <teoria>`_.

Discussão de caráter técnico e político sobre o aparato existente de
espionagem e monitoramento, com uma introdução sobre os conceitos
básicos sobre segurança da informação.

Também será abordado:

* O uso e a qualidade de senhas.
* Conceitos básicos de criptografia, como chaves, impressões digitais e
  assinaturas.

### Dia 2: Oficina Prática: Install Fest GNU/Linux com disco criptografado.

Dia para botar a mão na massa e instalar GNU/Linux no seu computador, já
que o uso de software livre é um requisito para a segurança da
informação.

Pré-requisitos:

* Trazer seu próprio computador laptop.
* Fazer um backup dos dados do seu computador ANTES da oficina se
  quiser instalar o GNU/Linux!

### Dia 3: Discussão teórica e oficina prática: a rede de navegação anônima Tor e o sistema operacional amnésico Tails.

Discussão sobre anonimato e explicação sobre o funcionamento da rede Tor
(The Onion Router * O Roteador Cebola) e de um sistema operacional
orientado à segurança e anonimato conhecido como Tails (The Amnesic
Incognito Live System).

Também será abordado:

* O uso e os perigos das redes sociais.
* Conexões criptografadas (HTTPS).

Traga seu laptop com GNU/Linux e aprenda a configurar o Tor! Traga um
pendrive para instalar o Tails!

Se você não tiver um sistema GNU/Linux, traga seu computador mesmo assim
para descobrir como instalar o Tor Browser Bundle!

Ainda, neste dia haverá um plantão especial para ajudar pessoas que
tiveram dificuldade com seus GNU/Linux recém-instalados.

### Dia 4: Discussão teórica: o mensageiro instantâneo off-the-record (OTR) e o padrão de criptografia OpenPGP.

O OTR é sistema de comunicação segura usado por Edward Snowden que
possui:

* Criptografia ponto a ponto com sigilo futuro, isto é, possibilita que
  apenas as partes envolvidas na comunicação consigam interpretar as
  mensagens.

* Negação plausível, isto é, permite que ambas as partes neguem que
  tenham realizado a comunicação!

Já o OpenPGP foi o padrão que trouxe a criptografia para as massas e é
amplamente utilizado na comunicação por email e também para a assinatura
de dados e softwares.

### Dia 5: Oficina prática: usando o OTR e o OpenPGP.

Traga seu laptop com GNU/Linux e aprenda:

* Configurar um programa comunicador instantâneo para usar o OTR.
* Configurar um leitor de email com suporte a OpenPGP, além de gerar  
  o seu par de chaves.

E mais: ao final da oficina, uma mini-festa de assinatura de chaves OTR
e OpenPGP.

### Dia 6: Discussão teórica: segurança em dispositivos móveis.

A problemática da segurança nos dispositivos móveis será abordada,
indicando mitigações e possibilidades futuras.

### Dia 7: Revisão do conteúdo do ciclo de oficinas e aprofundamento de alguns temas.

### Dia 8: [Segurança para grupos](https://plano.autodefesa.org/specs/grupos.html)
