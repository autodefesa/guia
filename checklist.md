# Checklist de Segurança

## Introdução

Já falamos sobre o funcionamento ideal de práticas de segurança e sobre como elas
podem ser priorizadas.

Só nos falta um conceito final para que possamos tratar dos ataques e defesas,
que é o nosso Checklist de Segurança, também conhecido como modelo de ameaças.
Ele nos ajudará a organizar a nossa estratégia de segurança.

Para cada tecnologia ou atividade, dividiremos nossa análise de segurança
em três etapas:

1. Funcionamento: saber como algo funciona é o primeiro passo.
2. Ataques: entendendo, conseguimos avaliar as ameaças possíveis.
3. Defesas: conhecendo as ameaças, podemos pensar em defesas.

## Ameaças: usos não convencionais

Qualquer objeto técnico tem inúmeros usos possíveis. Mesmo que eles tenham sido
construídos para desempenhar determinadas funções, eles acabam por ter usos
que não foram colocados intencionalmente pelas pessoas que os projetaram.

Nós podemos usar um martelo como peso de papéis. Podemos usar o forno de
microondas para secar roupas.  Podemos usar uma porta para quebrar nozes, uma
furadeira para bater massa de bolo, e assim por diante.

Com um pouco de memória e criatividade, acho que você também pode se lembrar
ou mesmo inventar usos alternativos para vários objetos.

Existem vários desvios de função malucos e criativos. Outros, nem tanto.

O desvio de função é parte essencial na evolução da tecnologia. Inclusive
na tecnologia de causar danos a pessoas e sistemas.

Podemos então definir como **ameaça** qualquer desvio de função de um objeto técnico
ou situação que possa ser usada para nos causar algum tipo de dano ou prejuízo.

O martelo do exemplo anterior pode ser usado para ferir uma pessoa. O automóvel
para atropelar alguém. Aliás, você já notou que automóveis e martelos não se tornam
proibidos mesmo sendo armas em potencial?

Quando pensamos em algum objeto ou atividade física -- um patinete, uma
melancia ou mesmo uma escada -- temos grande facilidade em imaginar seu uso
convencional.

Mas tempos um pouco de dificuldade para entender os riscos do uso desses
objetos e atividades, como casas de madeira que podem entrar em combustão,
árvores que podem cair sobre a gente, sequestros relâmpagos, etc.

Quando se trata de objetos ou atividades que são difíceis de visualizar, nós
temos muito menos intuição ainda desses riscos, como é o caso do uso de
dispositivos de comunicação digital.

Temos, por exemplo, dificuldade de entender que uma comunicação que trafega
por fios elétricos pode ser interceptada por alguém.

Em parte porque automaticamente pensamos sempre no uso convencional.

Mas, também, porque não entendemos direito o funcionamento interno de diversas
tecnologias, de tal modo que temos uma limitação em imaginar seus usos não
convencionais.

## Exemplo

Nada melhor do que um exemplo para ajudar a entender o que é o Checklist da
Segurança.

Suponha então uma pessoa que realize as seguintes atividades diárias:

1. Ela acorda em casa.
2. Depois, se desloca para o trabalho.
3. Trabalha com informações sensíveis no computador.
4. Utiliza o telefone celular durante todo o dia.
5. Por fim, se desloca para a casa para descansar.

Vamos pensar no funcionamento de cada uma dessas coisas. Para isso podemos apelar
para modelos bem esquemáticos, ou seja, conceitos apenas com os detalhes essenciais
de cada uma dessas coisas:

1. Casa: caixa feita de material rígido, com portas trancáveis, usada para proteger
   pessoas e bens. Uma casa não é apenas isso, mas suponha que seja neste momento!

2. Deslocamento na cidade: podemos supor aqui que seja feito via transporte público.

3. Trabalho com o computador: mais pra frente, neste guia, trataremos do funcionamento
   esquemático do computador. Por enquanto podemos assumir que ele é uma caixa onde
   entram e saem informações.

4. Telefone celular: idem ao caso do computador anterior: podemos pensá-lo como uma
   caixinha por onde circulam informações.

Essa é a versão muito simplificada da vida de uma pessoa. Na prática, fazemos muito
mais coisas e num nível de detalha muito maior. O importante agora é começar com
o básico e complicar somente caso necessário.

O que pode dar errado em cada uma dessas situações? Se quiséssemos
detonar o dia dessa pessoa, neutralizá-la ou mesmo roubar as informações que ela
possui, o que faríamos?

1. Casa: arrombamento das portas; fingir que somos funcionários de alguma empresa
   para conseguir entrar e render seus ocupantes; esperar que a pessoa saia de casa
   e rendê-la; impedir que a pessoa saia de casa; corte de comunicação com o mundo
   exterior para que ela não consiga acionar a emergência, etc.

2. Deslocamento: a pessoa pode ser atropelada; desastres naturais podem impedi-la
   de chegar o trabalho; assaltos e sequestros, etc.

3. Trabalho com computador: o computador pode estar grampeado, levando à
   extração de todas as informações nele colocadas ou alterando as informações
   para causar danos; ele pode simplesmente parar de funcionar e impedir que a
   pessoa trabalhe, etc.

4. Telefone celular: veremos que ele pode ser utilizado para rastrear a localização
   da pessoa; ele pode estar grampeado e as comunicações da pessoa serem interceptadas,
   etc.

Note que a lista de ameaças sempre é interminável! Podemos continuar, continuar, continuar
indefinidamente, mas numa hora teremos que ordená-la e apagar algumas coisas dela, com
o seguinte critério:

    A ameaça é provável? O quanto ela me afeta?

Vale notar que muitas ameaças só são prováveis a partir do momento em que uma pessoa
se torna um alvo específico, pois são ameaças que tem um custo para serem realizadas.
No entanto, é difícil saber se somos ou não alvos.

Em seguida, podemos partir para as defesas possíveis:

1. Casa: paredes sólidas; portas reforçadas; fechaduras que resistam um bom tempo
   mesmo sendo atacadas por chaveiros hábeis; janelas blindadas; sistema de comunicação
   de emergência, etc. Você até pode pensar em morar numa casa escondida! Como seria isso?

2. Deslocamento: atenção no trânsito e nas demais pessoas; usar rotas de baixo risco;
   sair em horários diferenciados; alternar trajeto para evitar alguém despistando, etc.

3. Computador: adotar as medidas que serão tratadas neste guia, por exemplo usar criptografia
   no armazenamento e nas comunicações.

4. Telefone celular: idem ao caso do computador: adotar as medidas abordadas ao longo do
   guia, como por exemplo deixá-lo numa outra sala durante conversas sensíveis.

Da mesma forma como a lista das ameaças, a lista das defesas é interminável. Escolheremos
então as nossas defesas de acordo com os seguintes critérios:

1. Custo (financeiro, pessoal, etc).
2. Eficácia.

É importante que as defesas escolhidas de fato representem proteções contra as ameaças que
escolhemos combater. Mas podemos também deixar listadas as ameaças que não serão combatidas
no momento, para termos consciência de tudo o que nos ameaça.

Lembre-se também que nenhuma defesa é totalmente eficaz ou cobre todos os aspectos de uma
dada ameaça.

Juntando isso tudo, teremos o seguinte Checklist ou Modelo de Segurança:

* Casa:
   * Ameaças prováveis:
       * Arrombamento.
   * Defesas adotadas:
       * Portas e janelas reforçadas.
       * Fechaduras que demorem mais de X horas para serem neutralizadas.
   * Ameaças não cobertas:
       * Sequestradores disfarçados.
       * etc
* Deslocamento:
   * Ameaças prováveis:
       * Atropelamentos.
       * Assaltos.
   * Defesas adotadas:
       * Atenção ao andar.
   * Ameaças não cobertas:
       * Rastreadores de pessoas.
       * Falhas no sistema de transporte.
       * etc
* Trabalho no computador:
   * Ameaças prováveis:
       * Roubo.
       * Adulteração de informações.
   * Defesas adotadas:
       * Armazenamento criptografado (confidencialidade e integridade).
       * Comunicação criptografada (confidencialidade, integridade, autenticidade).
   * Ameaças não cobertas:
       * Defeitos no computador.
       * Instalação de programas maliciosos.
       * etc
* Telefone celular:
   * Ameaças prováveis:
       * Utilização como escuta ambiental.
   * Defesas adotadas:
       * Deixar o celular numa outra sala ao realizar conversas sensíveis ao vivo.
   * Ameaças não cobertas:
       * Interceptação de conversas telefônicas.
       * Interceptação em comunicadores instantâneos.
       * etc

Esta é uma versão bem simplificada do modelo, mas serve muito bem para
começar. Versões complicadas podem incluir inclusive ameaças oriundas
da própria adoção de determinadas defesas, etc. Mas não vamos levar
isso em conta porque, no momento, seria adotar uma complexidade
desnecessária.

Existem vários formatos possíveis para essa lista, descubra a forma que
for mais apropriada para a suas necessidades.

O mais importante é extrair do modelo as ameaças prováveis, as defesas,
seus limites e as ameaças que não serão cobertas por falta de tempo,
energia ou outros recursos. Note que no caso das defesas digitais, foram
incluídas as propriedades de segurança da informação mínimas que elas
devem implementar (por exemplo confidencialidade, integridade e
autenticidade).

Perceba que essa é uma tarefa que requer método e sistemática mas que
pode ser feita de forma incremental, isto é, aos poucos, e começar a
partir de um brainstorm ou chuva de ideias e passar por uma análise
crítica posterior.

Com nosso modelo de ameaças pronto, teremos um conhecimento maior dos
**vetores** de ataque prováveis contra nós, isto é, as portas de entradas de
ameaças.  A esse conjunto de vetores de ataque damos o nome de **superfície de
ataque**.

## Resumo

Hora do resumo! Ao pensar numa atividade ou ferramenta, pensamos em três eixos:

1. Funcionamento: entendimento é o primeiro passo.
2. Ameaças: entendendo, conseguimos avaliar as ameaças possíveis.
3. Defesas: conhecendo as ameaças, podemos pensar em defesas.

Todas as atividades relevantes de uma pessoa ou organização podem ser organizadas
numa lista, incluindo as possíveis e prováveis ameaças àquelas atividades juntamente
com as defesas a serem adotadas.

Essa lista é chamada aqui de Checklist de Segurança ou Modelo de Ameaças.

## Atividades

1. Esboce um modelo de ameaça básico. Liste suas atividades e prováveis ameaças.
   Depois organize-as em ordem de acordo com as defesas mais importantes que
   você pretende adotar.

2. Com o seu modelo de ameaças, você conseguiria planejar um cronograma para
   adotar algumas práticas de segurança? Tente ser razoável e honesto(a) com você
   mesmo(a) e escolher um calendário factível!

## Referências

* [Plano de Autodefesa](https://plano.autodefesa.org).
