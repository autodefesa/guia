# Rastros Digitais

## Identidade

Além da sua senha, a questão da identidade também é importante. Não é
necessário usar sempre seu nome real. Muito pelo contrário, é muito
interessante adotar um ou mais pseudônimos, como o fez [Fernando
Pessoa](http://pt.wikipedia.org/wiki/Fernando_Pessoa), que inclusive
reservou um estilo literário por heterônimo.

Você não precisa ser como Pessoa e criar muitos nomes e personalidades
diferentes: o simples fato de você não usar seu nome ou seu nome
completo já é grande progresso.

## Apagamento de arquivos

Especial atenção também deve ser tomada na hora de apagar arquivos do
seu computador. Se os arquivos contiverem informações que você não quer
que de modo algum caiam nas mãos de terceiros, não adianta simplesmente
apága-los da forma tradicional (usando os programas normalmente
utilizados pelo seu sistema operacional). Isso porque:

* Em geral, quando você solicita a um programa para apagar um arquivo,
   ele apenas remove a referência do mesmo no "índice" do sistema de
   arquivos, mas os dados podem ou não continuarem dentro do disco
   rígido (veja por exemplo o [truque do
   serrote](http://lists.indymedia.org/pipermail/imc-tech/2005-October/1022-me.html)
   do xmux.
* Mesmo que você tenha certeza que seu programa não só removeu a
   referência ao arquivo no índice do sistema de arquivos mas também
   sobrescreveu todos as informações do arquivo, ainda é possível
   recuperar os dados através de uma [análise magnética do
   disco](http://wipe.sourceforge.net/secure_del.html).

Uma ferramenta do GNU/Linux que tenta evitar ambos os problemas é o
[wipe](http://wipe.sourceforge.net/). Um método complementar que
funciona em algumas mídias é conhecido como [ATA SECURE
ERASE](http://tinyapps.org/docs/wipe_drives_hdparm.html).

## Identificação de fotografias

Ao publicar fotografias, tenha o cuidado de

1. Substituir o rosto das pessoas por operações irreversíveis (como por
   exemplo pintá-las nalgum programa de edição). Evite utilizar efeitos
   (como por exemplo espiralar regiões da imagem) porque muitas vezes
   eles podem ser desfeitos e revelar os rostos.
2. Remover os dados [Exif](http://en.wikipedia.org/wiki/Exif) (por
   exemplo com o programa
   [jhead](http://www.sentex.net/~mwandel/jhead/)).

## Bancos de dados de redes sociais

Redes sociais, como o famigerado Ferrabook ou as tradicionais correntes de
emails são uma forma recente de se monitorar a associação entre pessoas.
Pariticipar de uma rede social cuja infra-estrutura não tenha
comprometimento com a preservação da privacidade dos usuários e usuárias
é colaborar para a [mineração de
dados](http://pt.wikipedia.org/wiki/Data_mining) não-solicitada.
Cuidado!

## Tempestade eletromagnética

A chamada "tempestade eletromagnética" se refere à radiação emitida
pelos equipamentos eletromagnéticos em funcionamento. Não é
exclusividade de um transmissor emitir ondas eletromagnéticas moduladas:
seu computador (principalmente seu monitor) faz isso o tempo todo.

Com aparelhos apropriados e a uma curta distância, é possível
reconstruir a imagem que alguém está observando num monitor sem estar
olhando diretamente para ele.

Este tópico está aqui de modo apenas ilustrativo, já que as técnicas
utilizadas atualmente são dignas de filmes de espionagem e os
equipamentos ainda são caros.

Como demostração, foi desenvolvido o software de GNU/Linux [Tempest for
Eliza](http://www.erikyyy.de/tempest/), que permite qualquer pessoa
transmitir uma mp3 em AM utilizando um monitor ligado num computador.

As contras medidas para esse tipo de ataque são:

* Compre um computador blindado :P (muito caro).
* Evite utilizar computadores em locais públicos ou de fácil acesso
  se você está realmente desconfiado/a de que exista alguém interessado
  em suas informações.

## Sobre

Texto originalmente publicado na [Documentação do CMI
Brasil](http://docs.indymedia.org/view/Local/CmiBrasilSeguranca)
