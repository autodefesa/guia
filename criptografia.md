# Criptografia Básica

## Funcionamento

Este não é um guia sobre criptografia, mas daremos os conceitos mais básicos
para que você consiga usá-la na vida prática sabendo em linhas gerais o que
está -- ou pelo menos deveria estar -- acontecendo na sua comunicação
criptografada.

A criptografia é o método de codificar dados e metadados para que a informação
possua um ou mais critérios de segurança como confidencialidade, integridade e
autenticididade.

Em sua aplicação mais básica, a criptografia é a técnica de codificar mensagens
de tal modo que apenas quem possuir o segredo de como decodificá-las pode
acessar seu conteúdo original.

A criptografia ainda pode ser utilizada para checar a autenticidade de uma mensagem,
ou seja, checar a autoria da mensagem, e ainda para verificar se a mensagem não
foi adulterada em seu trânsito.

Essas e outras propriedades da segurança da informação podem ser obtidas juntas
ou separadas dependendo do sistema criptográfico em uso.

A criptografia é baseada num processo onde a mensagem original é combinada com
uma chave de codificação e uma operação criptográfica para produzir um texto codificado:

    mensagem original -> operação cifrar, chave -> mensagem codificada

Para obter a mensagem original a partir da mensagem criptográfica, é preciso
ter acesso a uma chave que permitae realizar uma operação no caminho inverso:

    mensagem codificada -> operação decifrar, chave -> mensagem original

Como estamos lidando com criptografia em sistemas digitais, tanto a mensagem
original quanto a codificada e as chaves utilizadas são apenas números.

A mensagem codificada pode então trafegar por qualquer meio de comunicação,
pois suas propriedades de segurança da informação estarão operando. Ou seja,
essa criptografia, desde que bem implementada, oferece **confidencialidade**
à comunicação.

Por exemplo, se a mensagem codificada tiver a propriedade da confidencialidade,
ela só poderá ser lida por quem tiver condições de decifrá-la, isto é, por quem
tiver a chave de criptografia correta.

Existem dois tipos de chaves:

1. Chaves simétricas.
2. Chave assimétricas.

Veremos nos dois próximos capítulos como opera a criptografia com cada
uma delas.

## Chaves simétricas

Na criptografia baseada em chaves simétricas, ambas as partes de uma comunicação
compartilham a mesma chave criptográfica e esta é usada tanto para cifrar quanto
para decifrar mensagens.

Assim, é crucial para o funcionamento seguro da comunicação que a chave seja
mantida em segredo. Revelar a chave significa revelar a comunicação e até permitir
que terceiros possam usar a chave para enviar mensagens.

A criptografia de chave simétrica é bastante utilizada, porém na prática ela oferece
um problema: como compartilhar a chave entre as partes envolvidas?

No caso de duas pessoas se encontrarem pessoalmente para combinarem uma chave simétrica,
não há problema. Ou se a chave for transmitida de uma para outra num canal de comunicação
que ambas considerem seguros por terem efetivamente controle físico sobre ele.

Também não há problema no caso da criptografia usada para criptografar conteúdo
armazenado em discos, pois a chave não precisa ser transmitida.

Mas o que acontece se quisermos compartilhar a mesma chave com uma pessoa distante
e usando a internet como meio? Uma interceptação na comunicação poderia facilmente
capturar a chave durante sua transmissão, comprometendo toda a criptografia posterior.

E não poderíamos passar a chave usando uma comunicação criptografada, pois o outro
lado precisaria da própria chave para poder decifrá-la!

Uma das formas de contornar parcialmente esse problema é a utilização da
criptografia de chave assimétrica.

## Chaves assimétricas

Já a criptografia assimétrica é baseada em pares de chaves:

1. Uma chave pública, que pode (mas não precisa) ser divulgada amplamente.

2. Uma chave privada, que deve ser mantida em sigilo.

Cada parte envolvida na comunicação precisa de um par de chaves.
Suponha uma comunicação entre Fulana e Beltrana:

1. Fulana possui uma chave privada que chamaremos de Fulana Privada.
   E uma chave pública que chamaremos de Fulana Pública.

2. O mesmo ocorre com Beltrana: ela tem uma chave Beltrana Pública
   e uma Beltrana Privada.

A comunicação entra ambas funciona da seguinte maneira:

1. Fulana e Beltrana trocam suas chaves públicas. Isso quer dizer
   que Fulana recebe uma cópia de Beltrana Pública. Beltrana, por
   sua vez, recebe uma cópia de Fulana Pública.

2. Fulana pode criar uma mensagem para Beltrana usando o
   seguinte caminho

        mensagem original -> operação cifrar, chave Beltrana Pública -> mensagem codificada

Após receber a mensagem condificada, Beltrana pode decodificá-la
usando o esquema

    mensagem codificada -> operação decifrar, chave Beltrana Privada -> mensagem original

Se Beltrana quiser responder com outra mensagem, ela deve seguir o seguinte
caminho:

    mensagem original -> operação cifrar, chave Fulana Pública -> mensagem codificada

E Fulana poderá decifrá-la usando

    mensagem codificada -> operação decifrar, chave Fulana Privada -> mensagem original

Na prática, com a criptografia de chaves assimétricas, só esses caminhos
são viáveis para codificar e decodificar mensagens.

Com o uso de computadores para a realização das operações, a criptografia
se torna prática e eficiente.

## Identificação

A criptografia de chaves assimétricas resolve o problema da transmissão da
chave num meio desprotegido, já que a captura da chave pública por oponentes
não gera perigo adicional para a comunicação criptografada.

Mas essa transmissão de chaves não resolve um problema adicional: como garantir
que a chave não foi interceptada e substituída por outra, falsa?

E se alguém trocar a chave pública de uma pessoa por uma falsa e começar a
agir como se fosse essa pessoa?

Ou seja: como fazer a relação entre uma pessoa, instituição ou sistema e uma chave
pública? No nosso contexto de criptografia básica, podemos chamar esse processo
de **identificação** da chave pública: a quem ela pertence? Quem tem o controle
sobre ela?

A transmissão da chave pública não resolve todo o problema da segurança
da comunicação. Para confirmar se uma chave pública pertence a alguém,
precisamos ter alguma forma adicional para checá-la.

A maneira mais imediata é uma confirmação da chave pública ao vivo com
ambas as partes da comunicação certificando as duas chaves públicas, isto é,
uma chave pública por parte envolvida.

Ou então elas podem eleger um canal de comunicação que elas controlem ou
usar alguma outra forma de identificação: por exemplo, se as pessoas se conhecem
e concordarem com a relativa segurança do método, podem usar uma chamada telefônica
e ditarem um para o outro o conteúdo de suas chaves públicas.

Isso pode funcionar bem se um já conhece a voz do outro ou da outra e se
assumirmos que não existe um interceptador capaz de simular vozes convincentes.

O importante aqui é cada parte concordar com o processo de identificação.

Mas existe outro problema: muitos pares de chave que oferecem alguma segurança
tem um tamanho grande, o que complica muito a confirmação do tamanho da chave.

Para resolver isso utiliza-se a chamada **impressão digital** de uma chave pública,
como veremos no próximo capítulo.

## Impressão digital

Digamos que a impressão digital de uma chave seja sua versão resumida. É
possível que duas chaves públicas possuam a mesma impressão digital, mas isso é
muito difícil de acontecer. Sendo menor que a chave original mas ainda assim
praticamente única, é mais fácil utilizar uma impressão digital.

Impressões digitais existem tanto para criptografia simétrica quanto
assimétrica.

A impressão digital é uma sequência de números expressados tipicamente usando
o **sistema hexadecimal**, que utiliza os números de 0 a 9 e os algarismos de
A a F:

    268F 448F CCD7 AF34 183E  52D8 9BDE 1A08 9E98 BC16

Muitos softwares de criptografia possuem uma função de identificação de chaves,
ajudando tanto no processo de identificação quanto informando ao usuário se
determinadas chaves dos seus contatos já foram identificadas.

## Assinatura digital

Já vimos que podemos trocar chaves e fazer a identificação entre uma chave
e uma pessoa ou entidade. Queremos também ter um processo para checar se
uma mensagem foi enviada por determinada pessoa, entidade ou sistema.

Como podemos fazer essa verificação? Como podemos adicionar **autenticidade**
à nossa comunicação?

O próximo passo na criptografia é o uso das assinaturas digitais. Aqui
trataremos apenas do caso da criptografia assimétrica, onde uma assinatura
digital pode ser criada pelo seguinte processo:

    mensagem original -> operação assinar, chave privada -> assinatura da mensagem original

Ou, alternativamente:

    mensagem codificada -> operação assinar, chave privada -> assinatura da mensagem codificada

Ou seja, para a criação de uma assinatura deve ser utilizada a chave privada.

É possível também apenas assinar uma mensagem, sem criptografá-la, isto é,
sem codificar seu conteúdo:

    mensagem -> operação assinar, chave privada -> assinatura

A mensagem -- codificada ou não -- pode assim ser transmitida num meio qualquer
juntamente com a assinatura e, na outra ponta, ser verificada.

Para checar a assinatura, a seguinte operação é usada:

    mensagem, assinatura -> operação verificar, chave pública -> confirmação ou não-confirmação

Voltando ao nosso exemplo de Fulana e Beltrana, Fulana pode assinar uma mensagem
usando

    mensagem -> operação assinar, chave Fulana Privada -> assinatura

Em seguida, Fulana envia a mensagem e a assinatura para Beltrana, que verifica
a procedência da mensagem usando

    mensagem, assinatura -> operação verificar, chave Fulana Pública -> confirmação ou não

Ou seja, para criar a assinatura, Fulana usa sua chave privada, enquanto que
Beltrana pode checar a assinatura usando a chave pública de Fulana!

De quebra, a checagem de assinatura ainda oferece a propriedade da **integridade**
à nossa comunicação, já que mensagens que não puderem ter sua assinatura verificada
podem conter erros de transmissão ou terem sido adulteradas em trânsito.

Se a mensagem estiver apenas assinada, então uma interceptação poderá acessar
seu conteúdo, mas não conseguirá adulterar a mensagem sem que ocorra uma
falha na checagem da assinatura.

Em várias aplicações apenas a assinatura digital é utilizada, por exemplo para
permitir que um comunicado ou uma versão de um software publicamente distribuídos
possam ser verificados.

Mas, para proteger o conteúdo da mensagem, recomenda-se utilizar tanto a criptografia
do conteúdo quanto uma assinatura digital.

Tanto a mensagem original quanto a mensagem codificada podem ser assinadas e
essa é uma escolha que depende da implementação específica de criptografia que
está sendo usada. Em caso de possibilidade de escolha, assine e então
criptografe.

## Certificação: a rede de confiança

É possível até **assinar uma chave pública**:

1. Suponha que Fulana e Beltrana troquem chaves públicas e se identifiquem
   mutuamente, ou seja, atestem de algum modo que as chaves públicas realmente
   pertençam a uma e à outra.

2. Agora, imagine que Beltrana viaje para longe e encontre com Sicrana.
   Beltrana e Sicrana também trocam chaves públicas e realizam a identificação
   das chaves.

3. Mas Sicrana também quer ter alguma forma de identificar se a
   cópia da chave de Fulana, com quem ela nunca encontrou, também é válida.

4. Se Sicrana confia em Beltrana enquanto capaz de identificar Fulana
   corretamente, então Sicrana pode simplesmente consultar Beltrana a
   respeito dessa identificação. Noutras palavras, Beltrana pode
   **certificar** Sicrana sobre a chave de Fulana.

5. Uma forma de certificar uma chave pública é criar uma
   assinatura nessa chave.

A certificação usando assinatura funciona de modo análogo à assinatura
de uma mensagem, com a diferença que a mensagem é a própria chave pública.

Assim, Beltrana pode criar uma assinatura da chave de Fulana, deixá-la
disponível publicamente ou então enviá-la para Sicrana.

Sicrana, por sua vez, pode checar essa assinatura e, caso a assinatura
seja válida e Sicrana confie na capacidade de Beltrana de realizar a
identificação correta de Fulana, ela pode também criar uma assinatura
sobre a chave de Fulana.

A certificação cria um esquema de cadeia ou rede de confiança que permite
que pessoas, instituições e sistemas distantes identifiquem-se uns aos
outros.

Note que a confiança aqui não implica em confiança irrestrita, mas apenas
uma atestação restrita de que determinada chave pública é considerada mesmo
como pertencente a determinada pessoa.

Esse conceito é muito útil na implementação prática da segurança da
informação e é utilizado diariamente, como você verá a seguir no guia.

## Sigilo futuro

O último conceito sobre criptografia deste guia é chamado de **Sigilo Futuro**
e é muito importante para a proteção a longo prazo de uma comunicação.

Suponha que Fulana e Beltrana trocaram chaves públicas, se identificaram mutuamente
e usam criptografia para a comunicação diária.

Suponha que isso ocorra por muitos anos. E que por muitos anos um espião esteja
gravando toda essa comunicação.

Sem ter pelo menos uma das chaves privadas -- a de Fulana ou Beltrana -- o espião
não conseguirá fazer nada além disso: gravar as mensagens criptografadas.

Mas suponha agora que, num belo dia, o espião consiga roubar uma das chaves
privadas. A partir deste momento o espião pode decifrar retrospectivamente
todas as mensagens trocadas entre ambas as partes que sejam decifráveis com
a chave roubada. Se ambas as chaves forem obtidas, o sigilo de toda a comunicação
foi pro espaço.

E agora, o que fazer?

Bom, uma solução simples seria fazer com que Fulana e Beltrana destruíssem
periodicamente seus pares de chaves depois de substituí-las por pares novos.
Elas poderiam, antes de destruir as chaves antigas, usá-las para identificarem
as novas, num processo conhecido como **rolagem de chaves**.

Assim, se o espião capturasse uma chave privada, ele só teria acesso a um pedaço
menor da comunicação trocada.

Com isso, Fulana e Beltrana obtém uma propriedade da comunicação criptografada
chamada de **sigilo futuro**, que significa a proteção da maior parte da
comunicação no caso de perdas futuras de material criptográfico.

O sigilo futuro pode ser obtido manualmente com a rolagem periódica de chaves,
mas também pode ser feito automaticamente. Alguns sistemas criptográficos possuem
modos de operação com sigilo futuro.

## Ataques

Do que falamos até agora, podemos extrair diversos ataques a sistemas criptográficos
e incluir mais alguns outros:

1. O sistema criptográfico pode ter falhas conceituais: neste caso, o próprio princípio
   ou a matemática do sistema possui falhas que podem ser exploradas para quebrar
   a segurança da informação.

2. O sistema criptográfico pode ter falhas na implementação: aqui, é o hardware ou o
   software criptográfico que apresenta defeitos que podem ser usados para
   quebrar a segurança.

3. O sistema criptográfico pode ser mal utilizado, quando o uso incorreto de um sistema
   ocasiona em quebras na segurança. Isso depende muito de onde e como um sistema
   é usado, do nível de conhecimento de quem o utiliza e assim por diante.

4. Roubo de material criptográfico, isto é, roubo de chaves privadas. Isso pode
   ser usado para forjar mensagens e assinaturas e também para decifrar mensagens.

Alguns ataques podem levar a outros: falhas conceituais, de implementação ou
de uso podem levar ao roubo do material criptográfico.

É importante saber que a criptografia não resolve tudo. Ela não é o remédio para
todos os problemas de insegurança na comunicação digital. Mas ela é uma medida
importante e necessária.

A criptografia também não é infalível. Ela é baseada no conhecimento matemático
e na capacidade técnica de um dado momento da história. Nada garante que, num dado
momento, não seja descoberta uma forma de detonar um sistema.

Isso é o equivalente a dizer que é possível num dado momento a descoberta de falhas
conceituais ou de implementação no sistema criptográfico.

É possível até que alguém já saiba fazer isso, mas que prefira manter esse
conhecimento em segredo por razões óbvias: se a falha se torna pública, ela
pode ser corrigida ou o sistema ser substituído.

Mas, nesses casos, não é apenas a sua criptografia que vai por água abaixo, mas
a criptografia utilizada por milhões de pessoas, por empresas e instituições.

Até onde sabemos, o conhecimento e a capacidade dos atores mais poderosos hoje
no mundo não é capaz de quebrar uma criptografia forte e bem implementada. Ao invés
disso, esses atacantes tem investido na **trapaça** e em métodos de invasão que
contornem a criptografia explorando outras vulnerabilidades nos sistemas.

## Defesas

O que é uma criptografia forte e bem implementada? Como podemos avaliar isso?

Para responder essa pergunta, seria necessário entrar nos detalhes de como
a criptografia funciona, o que deixamos para as nossas referências do guia.

Se não temos condições, pelo menos no momento, de entender mais como a
criptografia funciona, podemos ao menos confiar na opinião da comunidade de
segurança sobre quais padrões de criptografia são recomendáveis.

O mesmo acontece com as implementações, isto é, os softwares que utilizam
de determinados processos criptográficos.

Isso significa que não devemos usar um software que se diz seguro apenas
por sua propaganda, porque o site dele é bonito ou porque ele possui um
cadeado no seu logotipo. Existem centenas de softwares que se propõem como
alternativas viáveis para comunicação segura.

Então tome cuidado e use apenas softwares que você pode avaliar a segurança por
conta própria ou confie na opinião da comunidade de pesquisa.

Além desta importante observação, seguem outras dicas:

1. Proteja as suas chaves privadas! Você pode usar até a criptografia
   de armazenamento do seu dispositivo para isso, como veremos adiante.

2. Mantenha seus softwares de criptografia sempre atualizados. Softwares
   desatualizados podem conter falhas conhecidas e portanto fáceis de
   serem exploradas por qualquer pessoa.

   Muitos sistemas possuem procedimentos automáticos de atualização
   que já dão conta disso. Outros precisam que a atualização seja
   feita manualmente de tempos em tempos. Existem, ainda, fontes de
   notícia de softwares ou de segurança que informam sobre vulnerabilidades
   encontradas e atualizações disponíveis.

Daremos dicas mais específicas quando abordarmos os softwares de criptografia
recomendados neste curso.

## Resumo

* Chaves simétricas e assimétricas.
* Cifrar e decifrar mensagens: fornecem **confidencialidade**.
* Assinaturas digitais: fornecem **autenticidade** e **integridade**.
* Identificação de chaves com pessoas, entidades ou sistemas: fornecem **autenticidade**.
* A identificação feita por terceiros é chamada neste guia de **certificação**.
* Use apenas soluções de criptografia forte que sejam consagradas pela comunidade de segurança.

## Atividades

1. Você conseguiria dizer quais meios de comunicação que você utiliza
   que estão com criptografia disponível e ativada? Você conseguiria
   classificar a qualidade da criptografia? Faça uma pesquisa rápida!

## Referências

* [Abre-te Sésamo: as senhas da nossa vida digital | Oficina Antivigilância](https://antivigilancia.org/pt/2015/06/abre-te-sesamo-as-senhas-da-nossa-vida-digital-2/).
* [Response to XKCD - Passwords](https://www.coolacid.net/articles/39-latest/239-response-to-xkcd-passwords).
