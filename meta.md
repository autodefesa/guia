# Meta

Manual em desenvolvimento por [Autodefesa](https://autodefesa.org).

## Licençiamento

Não havendo menção contrária, todo o conteúdo é disponível de acordo com

* [Creative Commons — Attribution-ShareAlike 3.0 Unported — CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/).
* [Definition of Free Cultural Works](http://freedomdefined.org/Definition).
* Logo: [Karate Girl](https://openclipart.org/detail/102751/karate-girl).

## Baixando o repositório

URL pública, somente leitura:

    git clone --recursive https://0xacab.org/autodefesa/guia

## Contribuindo

Este wiki utiliza o [padrão de documentação da Autodefesa](https://plano.autodefesa.org/meta.html).

## Em construção

Este guia sempre estará incompleto e sempre estará em construção!
