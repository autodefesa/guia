# Referências

```{toctree}
:maxdepth: 1
:maxdepth: 1
:glob:
referencias/*
```

Estas são referências complementares a este guia, o que não significa que
avaliamos, concordamos, ou recomendamos necessariamente o seus conteúdos.

## Guias práticos

### Em português

* [PRATO DO DIA: a refeição dos cuidados digitais](https://pratododia.org/).
* [A Criptografia Funciona - Como Proteger Sua Privacidade na Era da Vigilância em Massa](https://we.riseup.net/deriva/a-criptografia-funciona-como-proteger+260170).
* [Guia de Protestos](https://protestos.org).
* [Security-in-a-box](https://securityinabox.org/pt)
* [PRISM Break - Português](https://prism-break.org/pt/)
* [Segurança da Informação](https://ativismo.org.br/wp-content/uploads/2015/07/SI_Guia1_1.pdf).
* [Dicas de privacidade](https://www.gpopai.usp.br/wiki/index.php/Dicas_de_privacidade)
* [Cultura de Segurança](http://pt.protopia.at/wiki/Cultura_de_Seguran%C3%A7a)
* [Internet Segura](http://internetsegura.br/) do CGI.br.
* [Guia prática de estratégias e táticas para a segurança digital feminista](http://feminismo.org.br/guia/guia-pratica-seguranca-cfemea.pdf).
* [Segurança Holística](https://we.riseup.net/assets/481193/Tactical+tech+Seguranca+Holistica+com+prefacio.pdf).
* [Data Detox Kit](https://datadetoxkit.org/pt/home).
* [Tem boi na linha? » Guia prático de combate à vigilância na internet](https://web.archive.org/web/20180902092412/https://temboinalinha.org/).
* [Oficinas e Ferramentas | Oficina Antivigilância](https://web.archive.org/web/20190629152018/https://antivigilancia.org/pt/oficinas-e-ferramentas-beta/).
* [Sem segurança não existe pauta - Agência
  Pública](https://apublica.org/2020/10/sem-seguranc%CC%A7a-na%CC%83o-existe-pauta/):
  cartilha de segurança no campo e digital consolida alguns protocolos adotados
  ao longo dos anos pela Agência Pública.

### Em espanhol

* [Quema tu móvil](https://quematumovil.pimienta.org).
* [Cibermujeres](https://cyber-women.com/es/): currícula de seguridad digital
  con enfoque holístico y perspectiva de género que tiene el fin de brindar
  experiencias de aprendizaje para defensoras de derechos humanos que trabajan
  en entornos de alto riesgo..
* [Protege.la](https://protege.la/): espacio abierto para compartir recursos
  sobre seguridad y privacidad digital.
* [Registrando Incidentes de Seguridad Digital como Práctica de Mitigación del Riesgo](https://sursiendo.org/blog/2020/10/registro-y-analisis-de-incidentes-de-seguridad-digital/).
* [#SeguridadDigital](https://segudigital.org/).
* [¡Que Mercurio retrógrado no afecte tus comunicaciones digitales!](https://fortuna.segudigital.org/es).

### Em inglês

* [Organisational Security Wiki](https://orgsec.community/display/OS).
* [Freedom of the Press Foundation - Guides & Training ](https://freedom.press/training/).
* [Holistic security](https://holistic-security.tacticaltech.org/) ([download links](https://holistic-security.tacticaltech.org/downloads.html)).
* [Surveillance Self-Defense | Tips, Tools and How-tos for Safer Online Communications](https://ssd.eff.org/).
* [Email Self-Defense - a guide to fighting surveillance with GnuPG encryption](https://emailselfdefense.fsf.org/en/).
* [Anonymity/Security: A practical guide to computers for anarchists](http://zinelibrary.info/anonymity-security-practical-guide-computers-anarchists).
* [Quick Guide to Alternatives](https://alternatives.tacticaltech.org).
* [Me and my Shadow](https://myshadow.org/).
* [Information Security for Journalists | tcij.org](http://www.tcij.org/resources/handbooks/infosec).
* [Cybersecurity Policy for Human Rights Defenders - Publication | Global Partners Digital](http://www.gp-digital.org/publication/travel-guide-to-the-digital-world-cybersecurity-policy-for-human-rights-defenders/).
* [Complete manual - Gender and Tech Ressources](https://gendersec.tacticaltech.org/wiki/index.php/Complete_manual).
* [Security in Context](https://secresearch.tacticaltech.org/).
* [ActivistSecurity collective](http://www.activistsecurity.org)
* [Digital Security and Privacy for Human Rights Defenders](https://equalit.ie/esecman).
* [Rebel-Alliance-Tech-Manual: Introduction to electronic security for activists and dissidents](https://github.com/rebel-tech/Rebel-Alliance-Tech-Manual).
* [Security Tips Every Signal User Should Know](https://theintercept.com/2016/07/02/security-tips-every-signal-user-should-know/)
* [Guia para furar a censura](http://www.boingboing.net/censorroute.html) (dicas do Boing Boing)
* [Security Culture for Activists](http://www.ruckus.org/downloads/RuckusSecurityCultureForActivists.pdf) da [Ruckus Society](http://ruckus.org/).
* [Integrated security | Integrated security - the Manual](http://www.integratedsecuritymanual.org/).
* [Security Culture - A Comprehensive Guide for Activists in Australia](http://www.inventati.org/securityau).
* [OPSEC Resources](https://grugq.github.io/resources/).
* [Privacy Tools](https://privacytools.io).
* [Practical Privacy and Security](https://security.sflc.in/).
* [Security Planner](https://securityplanner.org/).
* [Digital Security Readiness Assessment Tool](https://0xacab.org/iecology/security-checklists).
* [LevelUp](https://level-up.cc/).
* [Holistic Security](https://holistic-security.tacticaltech.org/).
* [Security Education Companion (SEC)](https://sec.eff.org/).
* [Helpdesk - Reporters Without Borders](https://helpdesk.rsf.org/).
* [Cyberwomen](https://cyber-women.com/): digital security curriculum with a
  holistic and gender perspective, aimed at offering trainers with tools to
  provide in-person learning experiences to human rights defenders and
  journalists working in high-risk environments.
* [Counter-surveillance resource center](https://www.csrc.link/).
* [GJS - Hostile Environment Training & Support](https://www.gjs-security.com/).
* [EDRi’s privacy for kids booklet: Your guide to the Digital Defenders](https://edri.org/our-work/privacy-for-kids-digital-defenders/).
* [Tangible Internet Freedom](https://gitlab.torproject.org/raya/tangible-internet-freedom).
* [Web Privacy Rocks!](https://webprivacy.rocks/): collection of documents about privacy on the Web and developing for the privacy-friendly web.
* [PRISM Break](https://prism-break.org/en/) (english version).
* [Safer Storytellers: Strengthening safety and security resources for visual storytellers and journalists](https://saferstorytellers.org/).
* [Training Resoures - The Tor Project](https://community.torproject.org/training/resources/).
* [Digital Security Guides - The Tor Project](https://community.torproject.org/training/digital-security-guides/).
* [Training Checklist - The Tor Project](https://community.torproject.org/training/checklist/).
* [Training Risks - The Tor Project](https://community.torproject.org/training/risks/).
* [Earth Defenders Toolkit](https://www.earthdefenderstoolkit.com/kit-de-ferramentas/).
* [Toolkit for Psychosocial Providers Serving Digital Rights Defenders](https://www.digitalrights.community/blog/new-toolkit-for-psychosocial-providers-serving-digital-rights-defenders).

### Noutros idiomas

* [Guide d’autodéfense numérique](https://guide.boum.org).

### Sistemas

* [Segurança digital para sites - 1.0 - Actantes](https://actantes.org.br/wp-content/uploads/2017/05/Seguran%C3%A7a-de-Sites-Apostila-V.1.0.pdf).
* [Cibersegurança – Recursos Web para GLAM (galerias, bibliotecas, arquivos e museus)](https://webmuseu.org/recursos/category/ciberseguranca/).
* [Securing Debian Manual](http://www.debian.org/doc/manuals/securing-debian-howto/).
* [Verifying authenticity of Debian CDs](http://www.debian.org/CD/verify) ([How to tell if the key is safe](http://wiki.debian.org/SecureApt#How_to_tell_if_the_key_is_safe)).

### Análises, discussões e aprofundamentos

* [Warren and Brandeis, "The Right to Privacy"](https://groups.csail.mit.edu/mac/classes/6.805/articles/privacy/Privacy_brand_warr2.html).
* [Why I Wrote PGP](http://www.philzimmermann.com/EN/essays/WhyIWrotePGP.html).
* [The Economics of Mass Surveillance and the Questionable Value of Anonymous Communication](http://freehaven.net/anonbib/cache/danezis:weis2006.pdf).
* [O que todo revolucionário deve saber sobre a repressão (1929)](http://www.midiaindependente.org/pt/blue/2007/07/389198.shtml).
* [Cryptography I - Stanford University | Coursera](https://www.coursera.org/learn/crypto).
* [Salta Montes - traduções para o português no tema da segurança digital e guerra psicológica ](https://saltamontes.noblogs.org/).
* Glossário [draft-dkg-hrpc-glossary-00 - Human Rights Protocol Considerations Glossary](https://tools.ietf.org/html/draft-dkg-hrpc-glossary-00).
