# Mensageria

## Funcionamento

Neste curso falaremos de mensageria instantânea e não instantânea.

Falaremos de comunicação por envio de texto, imagens, áudio e vídeo e também de
chamadas de voz.

Em termos de segurança, tudo isso pode ser tratado conjuntamente, já que o
princípio de funcionamento da mensageria é baseado naquilo que já foi dito ao
longo do curso sobre transmissão de mensagens em meios digitais.

No entanto, na prática existem muitas aplicações, muitos padrões e muitos
protocolos de comunicação. Existem as defasagens de segurança comuns a todos
eles e também problemas específicos de cada plataforma.

Nos dois próximos capítulos trataremos de ataques e defesas na mensageria.

## Ataques

Nós já mencionamos neste curso diversos ataques à segurança da informação:
interceptação de comunicação, invasão de sistemas, roubo de material
criptográfico, etc.

A maioria deles também se aplica à mensageria. Aqui vamos nos concentrar
nos ataques específicos ou que devemos prestar atenção especial no caso
da mensageria.

1. A aplicação de mensageria é bem estabelecida? Existem inúmeros aplicativos
   para comunicação instantânea, muitos deles afirmando inclusive serem seguros
   quando não são. O primeiro fator de insegurança pode ser um aplicativo que
   em si é inseguro.

2. No caso do aplicativo oferecer criptografia, ela é de ponto a ponto?
   Ou o conteúdo das mensagens pode ser acessado pelo serviço de mensageria?

   Pode ser importante observar também se a criptografia de ponta a ponta oferece
   negação plausível e a possibilidade de identificar chaves a usuários.

   No geral, a criptografia do aplicativo é bem implementada?

   A criptografia opera também nos metadados?

   Nem sempre é fácil responder essas perguntas e por isso é importante
   ficar de olho nos aplicativos recomendados pela comunidade de segurança.

3. Como é feito o login na aplicação?

   Aplicativos que funcionam no computador em geral possuem autenticação
   com senha. Mas, no caso dos comunicadores de celular a criação de contas
   envolve a checagem com número de telefone como identificador global
   de usuários.

   Isso tem vários problemas. O número de telefone não é um dado anônimo e nem
   propriedade do usuário, mas sim da companhia telefônica. Além disso, se
   mal implementada essa confirmação pode ser burlada por atacantes para
   roubar sua conta ou espionarem a sua comunicação.

4. Onde ficam armazenadas as mensagens?

   É importante saber se as mensagens ainda não entregues ficam armazenadas no
   servidor sem criptografia. E, se depois de entregues, ficam armazenadas no
   dispositivo do usuário também sem criptografia.

## Defesas

Para defender sua segurança na mensageria, vou recomendar algumas tecnologias
e aplicativos que estão disponíveis em 2016.

Consulte sempre documentações atualizadas e confiáveis para novidades.

1. Para mensageria instantânia no smartphone, utilize o aplicativo Signal. Ele
   não é perfeito, mas é o melhor dentre todas as aplicações de mensagem
   instantânea para telefones.

   Benefícios do Signal incluem:

   * Faz chamadas por voz criptografadas usando o padrão ZRTP.
   * Criptografia ponto-a-ponto.
   * Sigilo futuro.
   * Armazenamento criptografado das suas mensagens no seu telefone caso
     você configure uma senha de bloqueio.
   * Permite que você faça a identificação de contatos com chaves criptográficas,
     processo conhecido como identificação criptográfica.
   * Possui um cliente de desktop que opera pareado ao smartphone.

   Limitações do Signal:

   * Apesar do cliente do Signal ser em código aberto, a parte do servidor
     não está inteiramente disponível.
   * O Signal utiliza a infraestrutura de mensageria do Google, o que não é
     muito bom em termos de privacidade e autonomia. Existe uma versão do
     Signal chamada de LibreSignal que não tem essa limitação, mas ela ainda
     não é muito estabelecida e talvez deixe de ser mantida.

2. Para mensageria instantânea no smartphone ou no computador, Comuncação
   Off-The-Record, ou OTR, que além de criptografia ponta-a-ponta suporta
   **negação plausível**.

   Existem vários softwares livres que implementam o protocolo OTR. Consulte
   as referências para detalhes.

3. Para comunicação por áudio e vídeo no seu computador, utilize o Jitsi
   diretamente no seu navegador.

4. Se você usa email e quer proteger sua comunicação, procure um provedor que
   respeite sua privacidade e suporte a tecnologia STARTTLS, que trocando em
   miúdos é o equivalente ao HTTPS no contexto do email

   Para proteger o conteúdo da mensagem ainda mais, utilize o padrão OpenPGP.

Nas referências você encontra links de manuais e guias de instalação
de softwares de mensageria.

## Email

O email é um sistema de comunicação muito popular que remonta às
primeiras redes computacionais. Quando o sistema de email foi criado,
não existia SPAM, vírus de email ou pessoas querendo interceptar suas
mensagens. Por isso, os protocolos de comunicação de email não foram
preparados para esses tipos de ataques. Em outras palavras, a
insegurança do sistema de email já conhece pela arquitetura do próprio
protocolo.

A falha do protocolo de email já começa com a problemática da
privacidade, que é descrita [nesta
seção](https://docs.indymedia.org/view/Sysadmin/GnuPGpt#Criptografia_e_correio_eletr_nic)
do [Manual de
Criptografia](https://docs.indymedia.org/view/Sysadmin/GnuPGpt).

Além desse problema de terceiros poderem ler suas mensagens, o correio
eletrônico ainda é uma porta para o recebimento de vírus. Para evitar
esse tipo de inconveniente, use programas de email e um sistema
operacional que sejam o mais imune possível.

Outro problema do sistema de email é a possibilidade de, apenas
observando uma mensagem, descobrir por onde ela foi enviada. Isso não é
exatamente uma vulnerabilidade, novamente é uma questão de design do
sistema de email, onde as mensagens são etiquetadas por cada servidor
por onde ela passou. Assim, se você receber um email de uma pessoa, em
geral você pode olhar os cabeçalhos da mensagem e descobrir qual
servidor a pessoa usou para enviar a mensagem! Essa informação é o chute
inicial para que você descubra qual é o provedor que a pessoa usa e
dependendo de quem você for é até possível descobrir de que local físico
o email foi enviado.

Qual a solução para esses problemas com email?

* Use sempre [Criptografia e conexão
  segura](https://docs.indymedia.org/view/Sysadmin/GnuPGpt#4)
* Verifique se o seu serviço de email oferece conexão segura e tem uma boa
  política de privacidade
* Evite ler emails de computadores públicos
* Use um [sistema operacional
  seguro](https://docs.indymedia.org/view/Sysadmin/TutorialLinux) e um
  [programa para ler emails que também seja
  seguro](http://br.mozdev.org/thunderbird/), caso contrário não abra anexos
  que contenham códigos que possam ser executados pelo seu sistema
* Se você acessa seu email via web, não esqueça de se desconectar dele após o
  uso

## Listas de discussão

Participar de [listas de
discussão](https://docs.indymedia.org/view/Local/CmiBrasilListasDiscussao)
também requer medidas básicas de segurança:

* Sempre que você entrar numa lista de discussão, verifique se os arquivos da
  lista ficam disponíveis publicamente na internet.

* A lista de discussão, apesar de ter arquivos fechados, pode ser pública, ou
  seja, qualquer pessoa pode se inscrever e a partir disso ter acesso aos
  arquivos. Mesmo que ela seja fecha e com inscrição restrita, ainda pode haver
  vazamento dos arquivos.

* Não divulgue dados pessoais seus ou de terceiros em listas de discussão.
  Quando necessário, apenas faça isso em mensagens privadas.

## Referências

* [Autodefesa no E-mail * um guia para combater a vigilância encriptando com GnuPG](https://emailselfdefense.fsf.org/pt-br/).
