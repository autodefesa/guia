# Índice

* Acesso via [Onion Service](http://tdtf5fdpfjgpxci4pqrtr5cvmsytqu25c2kdbflllz37k5glz6bexcyd.onion) ([sobre](https://community.torproject.org/onion-services/)) | [PDF](_static/autodefesa-digital.pdf) | [EPUB](_static/autodefesa-digital.epub) | [Vídeos](https://vimeo.com/rhatto)

Este é um guia para **qualquer pessoa e grupo** que tenha interesse em saber por
conta própria como se defender de ameaças do uso de dispositivos de comunicação
na era da vigilância automática.

Não importa quem você seja. Não importa o que você pensa. Não importa o que
você faz. Não importa o quanto você tem. Não importa se você governa ou é
governado. A vigilância digital te afeta.

A vigilância digital pode ser realizada por qualquer ator hostil. Ele pode
ocupar um cargo público, pode estar infiltrado numa empresa ou ser uma
"pessoa comum". Ele pode até ser um agente duplo que te vende segurança mas
que você não percebe por não ter senso crítico e autoconsciência.

**Saiba aqui como se defender sem cair na paranóia.**

```{toctree}
:maxdepth: 1
introducao.md
basico.md
avancado.md
limites.md
checklist.md
comunicacao.md
criptografia.md
senhas.md
computadores.md
telefones.md
internet.md
mensageria.md
backups.md
rastros.md
oficinas.md
referencias.md
meta.md
```
